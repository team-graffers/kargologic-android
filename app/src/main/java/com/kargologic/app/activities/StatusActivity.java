package com.kargologic.app.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kargologic.app.Fragments.CompletedFragment;
import com.kargologic.app.Fragments.InprogressFragment;
import com.kargologic.app.Fragments.UpcomingFragment;
import com.kargologic.app.R;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.classes.MyBroadcastReceiverNotification;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class StatusActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager vp;
    PagerAdapter adapter;
    Toolbar toolbar;
    ImageView notification;
    CircleImageView profileImg;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;
    public static View viewNotifyDot;

 //   Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //     getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.status_tab);

        /*dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_progress_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);*/

        preferences = getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.toolbar_searchbar);

        profileImg = (CircleImageView) toolbar.findViewById(R.id.profile_icon);

        if (preferences.getString(GlobalClass.imgUser, null) != null){
            Log.d("img_url_server", preferences.getString(GlobalClass.imgUser, null));
        }

        profileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(StatusActivity.this, ProfileActivity.class);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        notification = findViewById(R.id.notification_icon);
        viewNotifyDot = findViewById(R.id.view_notify_dot);

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewNotifyDot.setVisibility(View.GONE);
                GlobalClass.isNotify = false;
                startActivity(new Intent(StatusActivity.this, NotificationActivity.class));
            }
        });

        init();

        if (preferences.getString(GlobalClass.imgUser, null) != null){

            profileImg.setImageBitmap(null);
            Picasso.with(this).load(preferences.getString(GlobalClass.imgUser, null)).fit().placeholder(R.drawable.user_img_default).error(R.drawable.user_img_default).into(profileImg);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (GlobalClass.isNotify){
            viewNotifyDot.setVisibility(View.VISIBLE);
        }

        context = StatusActivity.this;

        Intent intent2 = new Intent(context, MyBroadcastReceiverNotification.class);
        PendingIntent pendingIntent2 = PendingIntent.getBroadcast(context, 123, intent2, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager mgr2 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mgr2.cancel(pendingIntent2);
        pendingIntent2.cancel();

        Intent intent = new Intent(context, MyBroadcastReceiverNotification.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 123, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 10000, pendingIntent);
    }

    @Override
    protected void onStop() {
        super.onStop();

        Intent intent2 = new Intent(context, MyBroadcastReceiverNotification.class);
        PendingIntent pendingIntent2 = PendingIntent.getBroadcast(context, 123, intent2, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager mgr2 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mgr2.cancel(pendingIntent2);
        pendingIntent2.cancel();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.truck_yellow).setText("In-Progress");
        tabLayout.getTabAt(1).setIcon(R.drawable.upcoming_black).setText("Upcoming");
        tabLayout.getTabAt(2).setIcon(R.drawable.complete_black).setText("Completed");
   //     tabLayout.setTabTextColors(getResources().getColor(R.color.blue), getResources().getColor(R.color.design_default_color_primary));
    }


    private void setupViewPager(ViewPager viewPager) {

        adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new InprogressFragment());
 //       adapter.addFragment(new BlankFragment());
        adapter.addFragment(new UpcomingFragment());
        adapter.addFragment(new CompletedFragment());

    //    adapter.addFragment(new BlankFragment());
    //    adapter.addFragment(new BlankFragment());
        viewPager.setAdapter(adapter);
    }

    class PagerAdapter extends FragmentPagerAdapter {

        private Map<Integer, String> fragmentTags;
        FragmentManager fragmentManager;

        private final List<Fragment> mFragmentList = new ArrayList<>();

        public PagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
            this.fragmentManager = supportFragmentManager;
            fragmentTags = new HashMap<>();
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {

            Object obj = super.instantiateItem(container, position);

            if (obj instanceof Fragment){
                Fragment f = (Fragment) obj;
                String tag = f.getTag();
                fragmentTags.put(position, tag);
            }
            return obj;
        }

        public Fragment getFragment(int position){
            String tag = fragmentTags.get(position);
            if (tag == null){
                return null;
            }

            return fragmentManager.findFragmentByTag(tag);
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);
    }


    private void init() {

        vp = (ViewPager) findViewById(R.id.pager);
        setupViewPager(vp);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(vp);
        setupTabIcons();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition() == 0) {
                    tabLayout.getTabAt(0).setIcon(R.drawable.truck_yellow).setText("In-Progress");
                    tabLayout.getTabAt(1).setIcon(R.drawable.upcoming_black).setText("Upcoming");
                    tabLayout.getTabAt(2).setIcon(R.drawable.complete_black).setText("Completed");
                }else if (tab.getPosition() == 1) {
                    tabLayout.getTabAt(0).setIcon(R.drawable.truck_black).setText("In-Progress");
                    tabLayout.getTabAt(1).setIcon(R.drawable.upcoming_yellow).setText("Upcoming");
                    tabLayout.getTabAt(2).setIcon(R.drawable.complete_black).setText("Completed");
                }else if (tab.getPosition() == 2) {
                    tabLayout.getTabAt(0).setIcon(R.drawable.truck_black).setText("In-Progress");
                    tabLayout.getTabAt(1).setIcon(R.drawable.upcoming_black).setText("Upcoming");
                    tabLayout.getTabAt(2).setIcon(R.drawable.complete_yellow).setText("Completed");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                Fragment fragment = ((PagerAdapter) vp.getAdapter()).getFragment(i);
                if (i >= 0 && fragment != null){
                    fragment.onResume();
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }
}
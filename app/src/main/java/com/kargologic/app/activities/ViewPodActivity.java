package com.kargologic.app.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.kargologic.app.R;
import com.kargologic.app.classes.GlobalClass;

import java.io.InputStream;

public class ViewPodActivity extends AppCompatActivity {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private Dialog dialog;
    TextView textView;
    ImageButton backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pod);

        preferences = getSharedPreferences(GlobalClass.preferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_progress_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        textView = (TextView) dialog.findViewById(R.id.please_wait);
        textView.setVisibility(View.VISIBLE);
        dialog.setCancelable(false);
        dialog.show();

        ImageView imageView = (ImageView) findViewById(R.id.img_vw_view_pod);
        backBtn = (ImageButton) findViewById(R.id.back_btn_pod);

        if (preferences.getString(GlobalClass.podImage, null) != null) {
            new DownloadImageFromInternet(imageView)
                    .execute(preferences.getString(GlobalClass.podImage, null));
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    private class DownloadImageFromInternet extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public DownloadImageFromInternet(ImageView imageView) {
            this.imageView = imageView;
//            Toast.makeText(getApplicationContext(), "Please wait....", Toast.LENGTH_SHORT).show();
        }

        protected Bitmap doInBackground(String... urls) {
            String imageURL = urls[0];
            Bitmap bimage = null;
            try {
                InputStream in = new java.net.URL(imageURL).openStream();
                bimage = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                Log.e("Error Message", e.getMessage());
                e.printStackTrace();
            }
            return bimage;
        }

        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
            textView.setVisibility(View.GONE);
            dialog.dismiss();
        }
    }
}

package com.kargologic.app.activities;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kargologic.app.classes.CurrentLocation;
import com.kargologic.app.classes.DashBoardDataCls;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.classes.InternetGpsCheck;
import com.kargologic.app.classes.MyBroadcastReceiver;
import com.kargologic.app.classes.PermissionManager;
import com.kargologic.app.pojoClasses.SignInCls.SignInClss;
import com.kargologic.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class SignInActivity extends AppCompatActivity {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    TextView signup, forgotpassword;
    Button signin;
    TextInputEditText phone, pass;
    RequestQueue queue;
    Dialog dialog;
    Context context;

    private PermissionManager permissionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        permissionManager = new PermissionManager() {
        };
        permissionManager.checkAndRequestPermissions(this);

        setContentView(R.layout.signin);

        context = SignInActivity.this;

        if (new InternetGpsCheck(context).isGpsEnabled()) {
            new CurrentLocation(context).liveLocation();
            new CurrentLocation(context).lattLong();
        } else {
            Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }

        preferences = getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        if (preferences.getString(GlobalClass.toKen, null) != null) {

            //        new DashBoardDataCls().userregister(this);
            GlobalClass.boolDashBoard = true;

            Intent intent2 = new Intent(context, MyBroadcastReceiver.class);
            PendingIntent pendingIntent2 = PendingIntent.getBroadcast(context, 123, intent2, PendingIntent.FLAG_ONE_SHOT);
            AlarmManager mgr2 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            mgr2.cancel(pendingIntent2);
            pendingIntent2.cancel();

            Intent intent = new Intent(context, MyBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 123, intent, PendingIntent.FLAG_ONE_SHOT);
            AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, pendingIntent);

            startActivity(new Intent(SignInActivity.this, StatusActivity.class));
        } else {

            queue = Volley.newRequestQueue(this);

            phone = findViewById(R.id.phone);
            pass = findViewById(R.id.password);

            dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_progress_layout);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setCancelable(false);

            signup = findViewById(R.id.tv_signup);
            String text1 = "<font color=#333333>Don't have an account ?</font> " +
                    "<font color=#dfa302> Sign Up </font>";
            signup.setText(Html.fromHtml(text1));

            signup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
                }
            });

            signin = findViewById(R.id.signin_button);
            signin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (new InternetGpsCheck(context).isNetworkConnected()) {

                        if (phone.getText().toString().trim().length() > 0) {
                            if (pass.getText().toString().trim().length() > 0) {

                                dialog.show();

                                String url = GlobalClass.login_URL;
                                //              url = "http://13.54.44.169:8000/driv/driv_login/";
                                JSONObject jsonObj = new JSONObject();

                                try {
                                    jsonObj.put("phone", phone.getText().toString().trim());
                                    jsonObj.put("password", pass.getText().toString().trim());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Log.d("login_json", jsonObj.toString());

                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {

                                        Log.d("response_signin", response.toString());

                                        try {

                                            SignInClss sign = new Gson().fromJson(response.toString(), SignInClss.class);

                                            String token = response.getString("token");
                                            String name = response.getString("name");
                                            String phoneNo = response.getString("phone");
                                            String email = response.getString("email");

                                            String image = response.getString("image");
                                            String carrierId = response.getString("carr_id");
                                            String carrierCompId = response.getString("carr_com_id");
                                            int vehicleId = response.getInt("cate_id");

                                            editor.putString(GlobalClass.toKen, token);
                                            editor.putString(GlobalClass.nameUser, name);
                                            editor.putString(GlobalClass.contactNoUser, phoneNo);
                                            editor.putString(GlobalClass.emailUser, email);
                                            editor.putString(GlobalClass.imgUser, image);
                                            editor.putString(GlobalClass.carrierId, carrierId);
                                            editor.putString(GlobalClass.carrierCompId, carrierCompId);

                                            editor.putString(GlobalClass.truckType, sign.getCategName());
                                            editor.putString(GlobalClass.truckRego, sign.getTruckRegNum());
                                            editor.putString(GlobalClass.licenseNo, sign.getLicenseNum());
                                            editor.putInt(GlobalClass.vehicleId, vehicleId);

                                            editor.commit();

                                            Log.d("response_token", token);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        new DashBoardDataCls().userregister(SignInActivity.this);
                                        dialog.dismiss();

                                        Intent intent2 = new Intent(context, MyBroadcastReceiver.class);
                                        PendingIntent pendingIntent2 = PendingIntent.getBroadcast(context, 123, intent2, PendingIntent.FLAG_ONE_SHOT);
                                        AlarmManager mgr2 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                                        mgr2.cancel(pendingIntent2);
                                        pendingIntent2.cancel();

                                        Intent intent = new Intent(context, MyBroadcastReceiver.class);
                                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 123, intent, PendingIntent.FLAG_ONE_SHOT);
                                        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, pendingIntent);

                                        startActivity(new Intent(SignInActivity.this, StatusActivity.class));
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        errorResponse(error);
                                    }
                                }) {
                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        HashMap<String, String> headers = new HashMap<>();
                                        headers.put("Content-Type", "application/json");
                                        return headers;
                                    }
                                };

                                RequestQueue queue = Volley.newRequestQueue(context);
                                int timeOut = 15000;
                                DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                                request.setRetryPolicy(retryPolicy);
                                queue.add(request);
                            } else {
                                pass.setError("Enter valid password.");
                            }
                        } else {
                            phone.setError("Enter valid mobile number.");
                        }
                    } else {
                        Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            forgotpassword = findViewById(R.id.forgotpassword_tv);
            forgotpassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(SignInActivity.this, ForgotPasswordActivity.class));

                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        permissionManager.checkResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);
    }


    public void errorResponse(VolleyError error) {

        dialog.dismiss();

        Log.d("response_volley_err", error.toString());
        //     Log.d("response_volley_code", error.toString());
        error.printStackTrace();

        /*if (error instanceof TimeoutError){

        }

        if (error instanceof NetworkError){
            Toast.makeText(context, "Please Check Your Internet Connection.", Toast.LENGTH_LONG).show();
        }*/

        if (error.networkResponse != null) {
            Log.d("response_volley_err2", String.valueOf(error.networkResponse.statusCode));
            try {
                String responseBody = new String(error.networkResponse.data, "utf-8");
                JSONObject data = new JSONObject(responseBody);
            /*JSONArray errors = data.getJSONArray("errors");
            JSONObject jsonMessage = errors.getJSONObject(0);*/
                String message = data.getString("detail");

                if (message.equals("Invalid token.")) {
                    editor.remove(GlobalClass.toKen);
                    editor.commit();
                    context.startActivity(new Intent(context, SignInActivity.class));
                }
                Log.d("response_e_upcoming6", message);
            } catch (JSONException e) {
            } catch (UnsupportedEncodingException errorr) {
            }
        } else {
//            Toast.makeText(this.context, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

// Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_volley_err3", obj.toString());
                String msg = obj.getString("message");
                Toast.makeText(context, "" + msg, Toast.LENGTH_LONG).show();
            } catch (UnsupportedEncodingException e1) {
// Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
// returned data is not JSONObject?
                e2.printStackTrace();
            }
        } else {
            //           Toast.makeText(this.context, "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }
    }
}
package com.kargologic.app.activities;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.classes.InternetGpsCheck;
import com.kargologic.app.classes.MyBroadcastReceiver;
import com.kargologic.app.R;
import com.kargologic.app.pojoClasses.profile.ProfileCls;
import com.kargologic.app.pojoClasses.vehicleList.VehicleList;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Toolbar toolbar;
    ImageButton btnEditProfileImg;
    LinearLayout backBtn;
    CircleImageView profileimage;
    private static final int SELECT_PICTURE = 1;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    TextInputEditText phoneNo, email, license, vehicleRego;
    TextView password, vehicleType, nameDriver;
    Spinner vehicleTypeSpin;
    Button saveChanges;
    HashMap<String, String> headers;

    RequestQueue queue;
    String profileDataNumEmailPass;
    private ImageButton logoutBtn;
    Dialog dialog;
    WindowManager.LayoutParams lWindowParams;
    ImageView bulletNew, bulletReNew, bulletReNewCnfm;
    TextInputEditText newProfileDataCnfm, newProfileData, currentProfileData;
    TextView hintLayout;
    static ImageButton noChanges;
    static Button confirmChanges;

    List<String> listSpin;
    List<Integer> listPos;
    int idVehicleType;
    String imageString, imageNameWithExt;
    VehicleList list;
    private Context context;
//    private TextView versionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //       getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.profile);

        context = ProfileActivity.this;

        imageString = "";
        imageNameWithExt = "";
        initialize();

        license.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                saveChanges.setVisibility(View.VISIBLE);
            }
        });
        vehicleRego.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                saveChanges.setVisibility(View.VISIBLE);
            }
        });
    }

    private void initialize() {

        preferences = getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        queue = Volley.newRequestQueue(this);

        headers = new HashMap<>();
        headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));

        profileimage = (CircleImageView) findViewById(R.id.profile_image);

        phoneNo = (TextInputEditText) findViewById(R.id.profile_phone);
        email = (TextInputEditText) findViewById(R.id.email);
        password = (TextView) findViewById(R.id.update_password);

        nameDriver = (TextView) findViewById(R.id.driver_name);

        license = (TextInputEditText) findViewById(R.id.license);
        vehicleType = (TextView) findViewById(R.id.vehicle_type);
        vehicleTypeSpin = (Spinner) findViewById(R.id.vehicle_type_spinner);

        vehicleRego = (TextInputEditText) findViewById(R.id.vehicle_rego);

        if (preferences.getString(GlobalClass.licenseNo, null) != null) {
            license.setText(preferences.getString(GlobalClass.licenseNo, null));
        }
        if (preferences.getString(GlobalClass.truckType, null) != null) {
            vehicleType.setText(preferences.getString(GlobalClass.truckType, null));
        }
        if (preferences.getString(GlobalClass.truckRego, null) != null) {
            vehicleRego.setText(preferences.getString(GlobalClass.truckRego, null));
        }

        saveChanges = (Button) findViewById(R.id.save_changes);
        saveChanges.setVisibility(View.GONE);

        toolbar = findViewById(R.id.profie_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.toolbar_profile);

        backBtn = (LinearLayout) findViewById(R.id.back_profile);
        logoutBtn = (ImageButton) findViewById(R.id.logout_btn);
        btnEditProfileImg = (ImageButton) findViewById(R.id.edit_profile);

        if (preferences.getString(GlobalClass.imgUser, null) != null) {
            Log.d("img_url_serverPro", preferences.getString(GlobalClass.imgUser, null));
        }
        if (preferences.getString(GlobalClass.imgUser, null) != null) {
            Picasso.with(this).load(preferences.getString(GlobalClass.imgUser, null)).fit().placeholder(R.drawable.user_img_default).error(R.drawable.user_img_default).into(profileimage);
        } else {
            profileimage.setImageResource(R.drawable.user_img_default);
        }




        if (preferences.getString(GlobalClass.contactNoUser, null) != null)
            phoneNo.setText(preferences.getString(GlobalClass.contactNoUser, null));
        if (preferences.getString(GlobalClass.emailUser, null) != null)
            email.setText(preferences.getString(GlobalClass.emailUser, null));
        if (preferences.getString(GlobalClass.nameUser, null) != null)
            nameDriver.setText(new GlobalClass().convert(preferences.getString(GlobalClass.nameUser, null)));

        vehicleTypeSpin.setOnItemSelectedListener(this);

  //      versionName = (TextView) findViewById(R.id.version_name);

        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int versionNumber = pinfo.versionCode;
  //          versionName.setText("Version "+pinfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        createSpinnerList();

        onClickEvents();
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        if (preferences.getString(GlobalClass.imgUser, null) != null) {
            Picasso.with(this).load(preferences.getString(GlobalClass.imgUser, null)).fit().placeholder(R.drawable.user_img_default).error(R.drawable.user_img_default).into(profileimage);
        } else {
            profileimage.setImageResource(R.drawable.user_img_default);
        }
    }*/

    int idPosition = -1;
    private void createSpinnerList() {
        String url = GlobalClass.truck_Cat_List;
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

   //             Log.d("response_list", response.toString());
                list = new Gson().fromJson(response.toString(), VehicleList.class);

                listSpin = new ArrayList<>();
                listPos = new ArrayList<>();

                for (int i = 0; i<list.getData().size(); i++){
                    listSpin.add(list.getData().get(i).getCategName());
                    if (!list.getData().get(i).getIsLast()){
                        listPos.add(i);
                    }

                    if (preferences.getInt(GlobalClass.vehicleId, -1) == list.getData().get(i).getId()){
                        idPosition = i;
                    }
                }

                createSpinner();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response_list", error.toString());
            }
        });

        queue.add(request);
    }

    boolean b = true;
    private void createSpinner() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, listSpin){
            @Override
            public boolean isEnabled(int position) {

                int pos = position;
                boolean bool = true;
                for (int i=0; i<listPos.size(); i++){
                    if (pos == listPos.get(i)){
                        bool = false;
                    }
                }

                if (bool) {
                    return true;
                }else {return false;}
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;

                boolean bool = true;
                for (int i=0; i<listPos.size(); i++){
                    if (position == listPos.get(i)){
                        bool = false;
                    }
                }

                if (bool) {
                    tv.setTextColor(Color.BLACK);
                }else {tv.setTextColor(Color.parseColor("#9C9C9C"));}
                return view;
            }
        };


        adapter.setDropDownViewResource(R.layout.spinner_item);

        vehicleTypeSpin.setAdapter(adapter);

        int i, x = 0;
        for (i=0; i<listPos.size(); i++){
            if (x == listPos.get(i)){
                x++;
            }else {break;}
        }

        if (idPosition == -1){
            vehicleTypeSpin.setSelection(x);
        }else { vehicleTypeSpin.setSelection(idPosition);}
        int pos = vehicleTypeSpin.getSelectedItemPosition();
        idVehicleType = list.getData().get(pos).getId();

        vehicleType.setText(vehicleTypeSpin.getSelectedItem().toString());
    }

    private void onClickEvents() {

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnEditProfileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutMethod();
            }
        });
        saveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChangesMthd();
            }
        });
        phoneNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneNumberUpdate();
            }
        });
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateEmail();
            }
        });

        password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new InternetGpsCheck(context).isNetworkConnected()) {
                    updatePswd();
                }else {
                    Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(ProfileActivity.this, StatusActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void updatePswd() {

        dialogInitialize();
        dialog.show();// I was told to call show first I am not sure if this it to cause layout to happen so that we can override width?
        dialog.getWindow().setAttributes(lWindowParams);

        currentProfileData.setHint("Enter current password");
        newProfileData.setHint("Enter new password");
        newProfileDataCnfm.setHint("Confirm password");

        bulletReNewCnfm.setVisibility(View.VISIBLE);
        newProfileDataCnfm.setVisibility(View.VISIBLE);

        hintLayout.setText("UPDATE PASSWORD");

        bulletNew.setBackgroundResource(R.drawable.password);
        bulletReNew.setBackgroundResource(R.drawable.password);
        bulletReNewCnfm.setBackgroundResource(R.drawable.password);


        confirmChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentProfileData.getText().toString().length() > 0 && newProfileData.getText().toString().trim().length() > 0 && newProfileData.getText().toString().trim().equals(newProfileDataCnfm.getText().toString().trim())) {

                    String url = GlobalClass.changePswd;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("old_pass", currentProfileData.getText().toString().trim());
                        jsonObject.put("new_pass", newProfileData.getText().toString().trim());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    final Dialog dialog2 = new Dialog(ProfileActivity.this);
                    dialog2.setContentView(R.layout.dialog_progress_layout);
                    dialog2.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialog2.setCancelable(false);
                    dialog2.show();

                    RequestQueue  queue = Volley.newRequestQueue(ProfileActivity.this);
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getString("status").equals("success")){

                                    Toast.makeText(ProfileActivity.this, "Password updated successfully", Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }else {
                                    Toast.makeText(ProfileActivity.this, "You have entered wrong password!!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            dialog2.dismiss();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dialog2.dismiss();
                        }
                    }){
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            return headers;
                        }
                    };

                    int timeOut = 15000;
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    request.setRetryPolicy(retryPolicy);
                    queue.add(request);
                } else {
                    newProfileData.setError("Enter valid password");
                }
            }
        });
    }

    private void dialogInitialize() {

        dialog = new Dialog(ProfileActivity.this);
        dialog.setContentView(R.layout.dialog_update_profile_layout);

        lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(dialog.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.FILL_PARENT; // this is where the magic happens
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setCancelable(false);

        noChanges = (ImageButton) dialog.findViewById(R.id.no_changes);
        hintLayout = (TextView) dialog.findViewById(R.id.hint_layout);
        confirmChanges = (Button) dialog.findViewById(R.id.confirm_changes);

        bulletNew = (ImageView) dialog.findViewById(R.id.icon_profile_current);
        bulletReNew = (ImageView) dialog.findViewById(R.id.icon_profile_new);
        bulletReNewCnfm = (ImageView) dialog.findViewById(R.id.icon_profile_new_cnfm);

        currentProfileData = (TextInputEditText) dialog.findViewById(R.id.current_profile);
        newProfileData = (TextInputEditText) dialog.findViewById(R.id.new_profile);
        newProfileDataCnfm = (TextInputEditText) dialog.findViewById(R.id.new_profile_cnfm);


        confirmChanges.setText("Continue");


        noChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void updateEmail() {

        dialogInitialize();

        dialog.show();// I was told to call show first I am not sure if this it to cause layout to happen so that we can override width?
        dialog.getWindow().setAttributes(lWindowParams);

        bulletReNewCnfm.setVisibility(View.GONE);
        newProfileDataCnfm.setVisibility(View.GONE);

        currentProfileData.setHint("Enter new email address");
        newProfileData.setHint("Confirm email address");
        hintLayout.setText("UPDATE EMAIL ADDRESS");

        currentProfileData.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        newProfileData.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        bulletNew.setBackgroundResource(R.drawable.mail);
        bulletReNew.setBackgroundResource(R.drawable.mail);

        confirmChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentProfileData.getText().toString().trim().length() > 0 && newProfileData.getText().toString().trim().equals(currentProfileData.getText().toString().trim())) {

                    if (preferences.getString(GlobalClass.emailUser, null) != null && preferences.getString(GlobalClass.emailUser, null).equals(newProfileData.getText().toString().trim())) {

                        dialog.dismiss();
                    } else {
                        profileDataNumEmailPass = newProfileData.getText().toString().trim();

                        if (validate(newProfileData.getText().toString().trim())) {

                            if (new InternetGpsCheck(context).isNetworkConnected()) {
                                checkEmailIsUnique(newProfileData.getText().toString().trim(), dialog);
                            }else {
                                Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                            }
  //                          checkEmailIsUnique(newProfileData.getText().toString().trim(), dialog);
                        } else {
                            dialog.dismiss();
                            newProfileData.setError("Enter valid email address");
                        }
                    }
                } else {
                    newProfileData.setError("Email address does not match");
                }
            }
        });
    }

    private void phoneNumberUpdate() {

        dialogInitialize();
        dialog.show();// I was told to call show first I am not sure if this it to cause layout to happen so that we can override width?
        dialog.getWindow().setAttributes(lWindowParams);

        bulletReNewCnfm.setVisibility(View.GONE);
        newProfileDataCnfm.setVisibility(View.GONE);

        currentProfileData.setHint("Enter new mobile number");
        newProfileData.setHint("Confirm mobile number");

        hintLayout.setText("UPDATE MOBILE NUMBER");

        currentProfileData.setInputType(InputType.TYPE_CLASS_NUMBER);
        newProfileData.setInputType(InputType.TYPE_CLASS_NUMBER);

        currentProfileData.setMaxLines(1);
        currentProfileData.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        newProfileData.setMaxLines(1);
        newProfileData.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});

        bulletNew.setBackgroundResource(R.drawable.call);
        bulletReNew.setBackgroundResource(R.drawable.call);


        confirmChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentProfileData.getText().toString().trim().length() == 10 && currentProfileData.getText().toString().trim().equals(newProfileData.getText().toString().trim())) {

                    if (preferences.getString(GlobalClass.contactNoUser, null) != null && preferences.getString(GlobalClass.contactNoUser, null).equals(newProfileData.getText().toString().trim())) {

                        dialog.dismiss();
                    } else {
                        profileDataNumEmailPass = newProfileData.getText().toString().trim();

                        if (new InternetGpsCheck(context).isNetworkConnected()) {
                            checkMobileIsUnique(newProfileData.getText().toString().trim(), dialog);
                        }else {
                            Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        }
   //                     checkMobileIsUnique(newProfileData.getText().toString().trim(), dialog);
                    }
                } else {
                    currentProfileData.setError("Enter valid mobile number");
                }
            }
        });
    }

    private void saveChangesMthd() {

        if (phoneNo.getText().toString().trim().length() > 0) {
            if (email.getText().toString().trim().length() > 0 && validate(email.getText().toString().trim())) {
                if (license.getText().toString().trim().length() > 0) {
                    if (vehicleTypeSpin.getSelectedItem().toString().trim().length() > 0) {
                        if (vehicleRego.getText().toString().trim().length() > 0) {

                            if (new InternetGpsCheck(context).isNetworkConnected()) {
                                saveChangedMethod();
                            }else {
                                Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                            }
    //                        saveChangedMethod();
                        } else { vehicleRego.setError("Enter vehicle rego"); }
                    } else { vehicleType.setError("Enter vehicle type"); }
                } else { license.setError("Enter license number"); }
            } else { email.setError("Enter valid email id"); }
        } else { phoneNo.setError("Enter valid mobile number"); }
    }

    private void logoutMethod() {

        editor.clear();
        editor.commit();

        Intent intent3 = new Intent(context, MyBroadcastReceiver.class);
        PendingIntent pendingIntent3 = PendingIntent.getBroadcast(context, 123, intent3, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager mgr2 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr2.cancel(pendingIntent3);
        pendingIntent3.cancel();

        Intent intent = new Intent(ProfileActivity.this, SignInActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void checkEmailIsUnique(final String emailUnique, final Dialog dialog) {

        final Dialog dialog2 = new Dialog(this);
        dialog2.setContentView(R.layout.dialog_progress_layout);
        dialog2.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog2.setCancelable(false);
        dialog2.show();

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("email", emailUnique);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = GlobalClass.uniqueEmailURL;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialog2.dismiss();

                try {
                    String msgResponse = response.getString("message");

                    if (msgResponse.equals("email does not exist")) {
                        email.setText(profileDataNumEmailPass);
                        saveChanges.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(ProfileActivity.this, "This email already exist", Toast.LENGTH_SHORT).show();
                    }
                    Log.d("msgResponse", msgResponse);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialog2.dismiss();
                errorResponse(ProfileActivity.this, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        queue.add(request);
    }

    private void checkMobileIsUnique(String mob, final Dialog dialog) {

        final Dialog dialog2 = new Dialog(this);
        dialog2.setContentView(R.layout.dialog_progress_layout);
        dialog2.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog2.setCancelable(false);
        dialog2.show();

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("number", mob);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = GlobalClass.uniqueMobileURL;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialog2.dismiss();
                try {
                    String msgResponse = response.getString("message");

                    if (msgResponse.equals("exist")) {
                        Toast.makeText(ProfileActivity.this, "This number already exist.", Toast.LENGTH_SHORT).show();
                    } else {
                        phoneNo.setText(profileDataNumEmailPass);
                        saveChanges.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialog2.dismiss();
                errorResponse(ProfileActivity.this, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        queue.add(request);
    }

    private boolean validate(String emailUnique) {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String emailTxt = emailUnique;

        if (emailTxt.matches(emailPattern) && emailTxt.length() > 0) {
            return true;
        } else {
            return false;
        }
    }

    private void saveChangedMethod() {

        final Dialog dialog2 = new Dialog(this);
        dialog2.setContentView(R.layout.dialog_progress_layout);
        dialog2.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog2.setCancelable(false);
        dialog2.show();

        JSONObject params = new JSONObject();

        try {
            params.put("new_phone", phoneNo.getText().toString().trim());
            params.put("new_email", email.getText().toString().trim());
            params.put("license_num", license.getText().toString());
            params.put("categ_id", idVehicleType);
            params.put("truck_reg_num", vehicleRego.getText().toString());
            params.put("profile_image", imageString);
            params.put("img_name", imageNameWithExt);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("jhhjhdjhdjsdhjasd", params.toString());

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = GlobalClass.updateProfileURL;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // response
                        Log.d("jhhjhdjhdjsdhjasd", response.toString());
                        dialog2.dismiss();

                        ProfileCls profileCls = new Gson().fromJson(response.toString(), ProfileCls.class);

                        String[] image = profileCls.getData().getImage().split("\\/");

                        String imgUrl = image[0]+"/";
                        for (int i=1; i<image.length; i++){
                            if (image[i].equals("")){

                            }else {
                                imgUrl += "/" + image[i];
                            }
                        }
                        Log.d("img_url_server_pro", imgUrl);
                        /*String nameWithProperSpacing = image.replaceAll("\\/+", "/");
                        Log.d("jhhjhdjhdjsdhjasd", nameWithProperSpacing);*/
                        editor.putString(GlobalClass.imgUser, imgUrl);

                        editor.putString(GlobalClass.contactNoUser, profileCls.getData().getPhone());
                        editor.putString(GlobalClass.emailUser, profileCls.getData().getEmail());

                        int idVehicle = list.getData().get(vehicleTypeSpin.getSelectedItemPosition()).getId();
                        editor.putInt(GlobalClass.vehicleId, idVehicle);
                        editor.commit();

                        editor.putString(GlobalClass.nameUser, profileCls.getData().getName());

                        editor.putString(GlobalClass.truckRego, profileCls.getData().getTruckRegNum());
                        editor.putString(GlobalClass.licenseNo, profileCls.getData().getLicenseNum());

                        editor.commit();

                        saveChanges.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog2.dismiss();
                        errorResponse(ProfileActivity.this, error);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    private void selectImage() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        /*if(resultCode == Activity.RESULT_OK && data != null){
            String realPath;
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11)
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());

                // SDK > 19 (Android 4.4)
            else
                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());


            setTextViews(Build.VERSION.SDK_INT, data.getData().getPath(),realPath);
        }*/
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && null != data) {
            Bitmap bitmap = null;
            if (data != null) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    //                 bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);

      //              profileimage.setImageBitmap(bitmap);

                    String imgUriPath = fileType(context, data.getData());
                    String[] str = data.getData().getLastPathSegment().split(":");

                    imageNameWithExt = str[str.length-1]+"."+imgUriPath;

                    getResizedBitmap(bitmap, 1100);

         //           String wholeID = DocumentsContract.getDocumentId(data.getData());

                    Log.d("img_uri", data.getData().getPath());
                    Log.d("wholeID", data.getData().toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String fileType(Context context, Uri uri){

        String fileTypeee = "";

        try{
            ContentResolver contentResolver = context.getContentResolver();
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            fileTypeee = mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
        }catch (Exception ex){

        }
        return fileTypeee;
    }

    private void setTextViews(int sdk, String uriPath,String realPath){

        String[] realP = realPath.split("/");
        imageNameWithExt = realP[realP.length-1];

        Uri uriFromPath = Uri.fromFile(new File(realPath));

        // you have two ways to display selected image

        // ( 1 ) imageView.setImageURI(uriFromPath);

        // ( 2 ) imageView.setImageBitmap(bitmap);
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uriFromPath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        getResizedBitmap(bitmap, 1100);
 //       profileimage.setImageBitmap(bitmap);

        Log.d("HMKCODE", "Build.VERSION.SDK_INT:"+sdk);
        Log.d("HMKCODE", "URI Path:"+uriPath);
        Log.d("HMKCODE", "Real Path: "+realPath);
    }

    public Bitmap getResizedBitmap22(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public void getResizedBitmap(Bitmap image, int maxSize) {

        if (image != null) {

 //           Toast.makeText(context, "Image selected....", Toast.LENGTH_SHORT).show();

            int width = image.getWidth();
            int height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap.createScaledBitmap(image, width, height, true).compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

            profileimage.setImageBitmap(image);

            saveChanges.setVisibility(View.VISIBLE);

            /* return Bitmap.createScaledBitmap(image, width, height, true);*/

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        vehicleType.setText(vehicleTypeSpin.getSelectedItem().toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void errorResponse(Context context, VolleyError error){

        this.context = context;
        Log.d("response_volley_err", error.toString());

        if (error instanceof NoConnectionError){
   //         Toast.makeText(context, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }

        if (error.networkResponse != null) {
            Log.d("response_volley_err2", String.valueOf(error.networkResponse.statusCode));
        }else{
   //         Toast.makeText(context, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

// Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_volley_err3", obj.toString());
            } catch (UnsupportedEncodingException e1) {
// Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
// returned data is not JSONObject?
                e2.printStackTrace();
            }
        }else {
            //           Toast.makeText(this.context, "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }

        new GlobalClass().inValidToken(ProfileActivity.this, error);
    }
}
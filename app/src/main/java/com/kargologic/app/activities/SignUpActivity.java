package com.kargologic.app.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.kargologic.app.R;


public class SignUpActivity extends AppCompatActivity {


    TextView signin,terms;



        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);



            signin=findViewById(R.id.tv_signIn);
            String text1 = "<font color=#1f2124>Have an account already ?</font> "+
                    "<font color=#ffd360> Sign In </font>";
            signin.setText(Html.fromHtml(text1));

            signin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(SignUpActivity.this,SignInActivity.class));


                }
            });




            terms=findViewById(R.id.terms);
            String text = "<font color=#333333>By signing you are agree to our</font> "+
                    "<font color=#3e8fff> T&C </font>"+"<font color=#333333> and </font>"+"<font color=#3e8fff>Privacy Policy</font>";
            terms.setText(Html.fromHtml(text));


            terms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(SignUpActivity.this, PrivacyPolicyActivity.class));


                }
            });

        }
}

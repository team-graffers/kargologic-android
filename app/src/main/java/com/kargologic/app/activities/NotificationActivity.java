package com.kargologic.app.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kargologic.app.Adapter.NotificationListAdapter;
import com.kargologic.app.R;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.pojoClasses.notification.Datum;
import com.kargologic.app.pojoClasses.notification.NotificationList;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class NotificationActivity extends AppCompatActivity {

    Toolbar toolbar;
    LinearLayout back;
    RecyclerView rv;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;
    List<Datum> list;
    private Dialog dialog;
    private TextView noNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
  //      getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.notification);

        toolbar = findViewById(R.id.notification_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.toolbar_notification);

        context = NotificationActivity.this;

        preferences = getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        back=(LinearLayout) findViewById(R.id.back_notify);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rv = (RecyclerView) findViewById(R.id.recyclerview);
        noNotification = (TextView) findViewById(R.id.no_order_in_list_txt);

        setNotificationListData();
    }

    private void setNotificationListData() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_progress_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, GlobalClass.notification, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();

                Log.d("resp_notify_s", response.toString());

                NotificationList notificationList = new Gson().fromJson(response.toString(), NotificationList.class);

                list = new ArrayList<>();


                if (notificationList.getData().size() > 0) {

                    Datum datum;
                    for (int i = 0; i < notificationList.getData().size(); i++) {

                        int ipos = new Random().nextInt(5);


                        for (int x = 0; x < i; x++) {
                            if (notificationList.getData().get(i).getOrderID() == list.get(x).getOrderID()) {
                                ipos = list.get(x).getColorLogo();
                                break;
                            }
                        }

                        datum = new Datum();

                        datum.setActive(notificationList.getData().get(i).getActive());
                        datum.setColorLogo(ipos);
                        datum.setComments(notificationList.getData().get(i).getComments());
                        datum.setCreatedByCarrier(notificationList.getData().get(i).getCreatedByCarrier());
                        datum.setCreatedByCustomer(notificationList.getData().get(i).getCreatedByCustomer());
                        datum.setCreatedByDriver(notificationList.getData().get(i).getCreatedByDriver());
                        datum.setCreatedForCarrier(notificationList.getData().get(i).getCreatedForCarrier());
                        datum.setCreatedForCustomer(notificationList.getData().get(i).getCreatedForCustomer());
                        datum.setCreatedForDriver(notificationList.getData().get(i).getCreatedForDriver());
                        datum.setDateTimeOfCreate(notificationList.getData().get(i).getDateTimeOfCreate());
                        datum.setId(notificationList.getData().get(i).getId());
                        datum.setOrderID(notificationList.getData().get(i).getOrderID());
                        datum.setRead(notificationList.getData().get(i).getRead());

                        list.add(datum);
                    }

                    NotificationListAdapter adapter = new NotificationListAdapter(context, list);

                    rv.setHasFixedSize(true);
                    rv.setLayoutManager(new LinearLayoutManager(context));
                    rv.setAdapter(adapter);
                    rv.setItemAnimator(new DefaultItemAnimator());

                    rv.setVisibility(View.VISIBLE);
                    noNotification.setVisibility(View.GONE);
                }else {
                    noNotification.setText("No notifications to display");
                    rv.setVisibility(View.GONE);
                    noNotification.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.d("resp_notify_e", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }
}

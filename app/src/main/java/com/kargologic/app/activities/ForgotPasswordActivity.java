package com.kargologic.app.activities;

import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kargologic.app.R;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.classes.InternetGpsCheck;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity {

    TextInputEditText emailForgot;
    Button btnForgotPswd;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);

        emailForgot = (TextInputEditText) findViewById(R.id.email_forgot_pswd);
        btnForgotPswd = (Button) findViewById(R.id.email_send_forgot_pswd);

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_progress_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        btnForgotPswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emailForgot.getText().toString().trim().length()>0 && validEmail(emailForgot.getText().toString().trim())){

                    if (new InternetGpsCheck(ForgotPasswordActivity.this).isNetworkConnected()) {
                        sendResetPswdLink(emailForgot.getText().toString().trim());
                    }else {
                        Toast.makeText(ForgotPasswordActivity.this, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
  //                  sendResetPswdLink(emailForgot.getText().toString().trim());
                }else {emailForgot.setError("Enter valid email address");}
            }
        });
    }

    private void sendResetPswdLink(String email) {

        String url = GlobalClass.forgotPswd;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();

                try {
                    final String msg = response.getString("message");
                    final String scs = response.getString("data");

                    final Dialog dialog22 = new Dialog(ForgotPasswordActivity.this);
                    WindowManager.LayoutParams lWindowParams;

                    dialog22.setContentView(R.layout.dialog_forgot_pswd);

                    lWindowParams = new WindowManager.LayoutParams();
                    lWindowParams.copyFrom(dialog22.getWindow().getAttributes());
                    lWindowParams.width = WindowManager.LayoutParams.FILL_PARENT; // this is where the magic happens
                    lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog22.setCancelable(false);

                    dialog22.show();// I was told to call show first I am not sure if this it to cause layout to happen so that we can override width?
                    dialog22.getWindow().setAttributes(lWindowParams);

                    TextView msgTxt = (TextView) dialog22.findViewById(R.id.msg_forgot_pswd);
                    Button btnOK = (Button) dialog22.findViewById(R.id.ok_forgot);

                    msgTxt.setText(scs+".");

                    btnOK.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog22.dismiss();
                            if (msg.equals("success")){
                                startActivity(new Intent(ForgotPasswordActivity.this, SignInActivity.class));
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
            }
        });

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    private boolean validEmail(String trim) {
        String email = trim;

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (email.matches(emailPattern) && email.length() > 0) {
            return true;
        }
        return false;
    }
}

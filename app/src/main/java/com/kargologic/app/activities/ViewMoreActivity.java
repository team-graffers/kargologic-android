package com.kargologic.app.activities;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kargologic.app.Adapter.AdapterChat;
import com.kargologic.app.Adapter.AttachmentListAdapter;
import com.kargologic.app.Adapter.ViewMoreDetailAdapter;
import com.kargologic.app.classes.ApiCallCls;
import com.kargologic.app.classes.CurrentLocation;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.classes.InternetGpsCheck;
import com.kargologic.app.classes.LatLngCls;
import com.kargologic.app.pojoClasses.ViewMore.Assets;
import com.kargologic.app.pojoClasses.ViewMore.Attachment;
import com.kargologic.app.pojoClasses.ViewMore.DataData;
import com.kargologic.app.pojoClasses.ViewMore.Delivery;
import com.kargologic.app.pojoClasses.ViewMore.ExtraDetail;
import com.kargologic.app.pojoClasses.ViewMore.Pickup;
import com.kargologic.app.pojoClasses.ViewMore.ViewMoreClass;
import com.kargologic.app.pojoClasses.eta.EtaTime;
import com.kargologic.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewMoreActivity extends AppCompatActivity {

    TextView logoTxt, nameCustomer, addressPickUp, addressDelivery, datePickUp, dateDelivery, additionalNote;
    TextView vehicleType, regNumber;
    public static TextView statusOrder;
    public static ImageView statusDotColor, statusTruck;
    ImageButton btnCall, btnChat, btnChatSend, btnCloseChat;
    //    Button viewPODBtn;
    RecyclerView recyclrVwOrderStatus, recyclrVwAdditionAtt, recyclrVwChat;
    RelativeLayout chatLayout;
    EditText chatTxt;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    List<LatLngCls> listLatLng;
    List<LatLngCls> listEtaTime;
    LatLngCls latLngCls;

    Toolbar toolbar;
    LinearLayout arrowBack;
    public static Dialog dialog;
    private TextView jobNumber, noComments, noAdditionalAttachment;
    private boolean boolAddNotes = true;
    int PICK_IMAGE_REQUEST = 111;
    Bitmap bitmap = null;

    ArrayList<DataData> datalist;
    ViewMoreDetailAdapter adapter;
    private DataData itemDataData;
    private RecyclerView chatRecyclrVw;
    private Uri imageUri;
    private static Context context;
    private Handler mHandler;
    private int iii;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_more);

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_progress_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        context = ViewMoreActivity.this;

        if (new InternetGpsCheck(context).isGpsEnabled()) {
            new CurrentLocation(context).lattLong();
            new CurrentLocation(context).liveLocation();
        } else {
            Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }

        initialize();
    }

    private void initialize() {

        logoTxt = (TextView) findViewById(R.id.logo_textview);
        nameCustomer = (TextView) findViewById(R.id.name_textview);
        statusOrder = (TextView) findViewById(R.id.status_order);
        addressPickUp = (TextView) findViewById(R.id.pickup_address);
        addressDelivery = (TextView) findViewById(R.id.deliver_address);
        datePickUp = (TextView) findViewById(R.id.pickup_date);
        dateDelivery = (TextView) findViewById(R.id.deliver_date);
        additionalNote = (TextView) findViewById(R.id.additional_note);
        noComments = (TextView) findViewById(R.id.no_comments);
        noAdditionalAttachment = (TextView) findViewById(R.id.no_additional_attachment);
        vehicleType = (TextView) findViewById(R.id.vehicle_type);
        regNumber = (TextView) findViewById(R.id.registration_number);

        statusDotColor = (ImageView) findViewById(R.id.status_dot_color);
        statusTruck = (ImageView) findViewById(R.id.status_truck);

        btnCall = (ImageButton) findViewById(R.id.call_btn);
        btnChat = (ImageButton) findViewById(R.id.chat_btn);
        btnChatSend = (ImageButton) findViewById(R.id.chat_send_btn);
        btnCloseChat = (ImageButton) findViewById(R.id.close_chat);

        /*     viewPODBtn = (Button) findViewById(R.id.view_pod_btn);*/

        recyclrVwOrderStatus = (RecyclerView) findViewById(R.id.order_status_inprogress);
        recyclrVwAdditionAtt = (RecyclerView) findViewById(R.id.additional_attachment_inprogress);
        recyclrVwChat = (RecyclerView) findViewById(R.id.chat_recyclr_vw);

        chatLayout = (RelativeLayout) findViewById(R.id.chat_layout_show_hide);
        chatTxt = (EditText) findViewById(R.id.chat_txt);

        preferences = getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        toolbar = findViewById(R.id.inprogress_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.toolbar_viewprogress);

        arrowBack = (LinearLayout) findViewById(R.id.arrow_back);
        jobNumber = (TextView) findViewById(R.id.job_number);

        btnClick();
    }

    private void btnClick() {

        arrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatLayout.setVisibility(View.VISIBLE);
            }
        });

        btnChatSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chatTxt.getText().toString().trim().length() > 0) {
                    chatSendMethod();
                }
            }
        });

        btnCloseChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatLayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (chatLayout.getVisibility() == View.VISIBLE) {
            chatLayout.setVisibility(View.GONE);
        } else {
            finish();
            //     startActivity(new Intent(ViewMoreActivity.this, StatusActivity.class));
        }
    }

    Boolean bool = true;

    private void chatSendMethod() {

        final HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("comment", chatTxt.getText().toString().trim());
            jsonObject.put("order_id", preferences.getString(GlobalClass.orderId, null));
            jsonObject.put("date_time_of_create", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(Calendar.getInstance().getTime()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = GlobalClass.sendComments;

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                bool = false;
                chatTxt.setText("");
                pickUpDropData();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                volleyErrorResponse(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    @Override
    public void onStart() {
        super.onStart();

        capturePOD = false;

        if (new InternetGpsCheck(context).isGpsEnabled()) {
            new CurrentLocation(context).lattLong();
            new CurrentLocation(context).lattLong();
        } else {
            Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }

        if (new InternetGpsCheck(context).isNetworkConnected()) {
            iii = 0;
            pickUpDropData();
        } else {
            Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void pickUpDropData() {

        if (!dialog.isShowing()) {
            dialog.show();
        }

        final HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));

        String url = GlobalClass.base_Url + "driv/driver_job_detail/?order_id=" + preferences.getString(GlobalClass.orderId, null);

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                datalist = new ArrayList<>();
                listLatLng = new ArrayList<>();
                listEtaTime = new ArrayList<>();
                adapter = new ViewMoreDetailAdapter(ViewMoreActivity.this, datalist, preferences.getString(GlobalClass.orderId, null), listEtaTime);

                recyclrVwOrderStatus.setHasFixedSize(true);
                recyclrVwOrderStatus.setLayoutManager(new LinearLayoutManager(ViewMoreActivity.this));
                recyclrVwOrderStatus.setAdapter(adapter);
                recyclrVwOrderStatus.setItemAnimator(new DefaultItemAnimator());

                Log.d("response_view_more", response.toString());
                dialog.dismiss();

                final ViewMoreClass dashboardClass = new Gson().fromJson(response.toString(), ViewMoreClass.class);

                if (dashboardClass.getMain().size() > 0) {

        //            Log.d("no_attach", dashboardClass.getMain().get(0).getExtraDetail().getAdditionalNote());
                    String attachMent = dashboardClass.getMain().get(0).getExtraDetail().getAdditionalNote();
                    if (attachMent != null && !attachMent.equals("")) {
                        additionalNote.setText("   " + dashboardClass.getMain().get(0).getExtraDetail().getAdditionalNote());
                    } else {
                        additionalNote.setText("No additional note available");
                    }
                    if (dashboardClass.getAttachment().size() > 0) {
                        noAdditionalAttachment.setVisibility(View.GONE);
                        recyclrVwAdditionAtt.setVisibility(View.VISIBLE);
                        setDataAdditionalRecyclrVw(dashboardClass.getAttachment());
                    } else {
                        noAdditionalAttachment.setVisibility(View.VISIBLE);
                        recyclrVwAdditionAtt.setVisibility(View.GONE);
                    }

                    if (dashboardClass.getOrderComment().size() > 0) {

                        noComments.setVisibility(View.GONE);
                        AdapterChat adapterChat = new AdapterChat(ViewMoreActivity.this, dashboardClass.getOrderComment());
                        chatRecyclrVw = (RecyclerView) findViewById(R.id.chat_recyclr_vw);
                        chatRecyclrVw.setHasFixedSize(true);
                        chatRecyclrVw.setLayoutManager(new LinearLayoutManager(ViewMoreActivity.this));
                        chatRecyclrVw.setAdapter(adapterChat);
                    } else {
                        noComments.setVisibility(View.VISIBLE);
                    }

                    String jobNum = dashboardClass.getMain().get(0).getExtraDetail().getJobNo();

                    editor.putString(GlobalClass.jobNo, jobNum);
                    editor.commit();

                    jobNumber.setText("#" + jobNum);

                    callBtnEvent(btnCall, dashboardClass.getMain().get(0).getExtraDetail().getCarrierCompanyNumber());

                    setLogoTxtandDimens(dashboardClass.getMain().get(0).getExtraDetail().getCustomerCompanyName());

                    setPickUpData(dashboardClass.getMain().get(0).getPickup(), dashboardClass.getMain().get(0).getExtraDetail().getOrderId());
                    setDeliveryData(dashboardClass.getMain().get(0).getDelivery(), dashboardClass.getMain().get(0).getExtraDetail().getOrderId());

                    setEnRouteTime(dashboardClass.getMain().get(0).getOrderEnroute());

                    sortDataList(dashboardClass);
                    setPickUpDeliveryAddressDate(datalist);
                    setStatusOfItem();
                    setStatusandStatusDotColor(dashboardClass.getMain().get(0).getExtraDetail());

                    mHandler = new Handler();
                    mHandler.postDelayed(m_Runnable, 60000);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                volleyErrorResponse(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    private final Runnable m_Runnable = new Runnable() {
        @Override
        public void run() {
            setStatusOfItem();
            iii += 100;
            mHandler.postDelayed(m_Runnable, 60000);
        }
    };

    private void prepareEtaTimeList(int i) {

        if (new InternetGpsCheck(context).isGpsEnabled()) {


            String url = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&waypoint0=geo!" + GlobalClass.latt + "," + GlobalClass.lngg + "&waypoint1=geo!" + datalist.get(i).getLatt() + "," + datalist.get(i).getLngg() + "&mode=fastest;truck;traffic:disabled";
            etaTimeMethod(url, i);
        }
    }

    private void setStatusandStatusDotColor(ExtraDetail extraDetail) {

        if (extraDetail.getVehicleType() != null) {
            vehicleType.setText(extraDetail.getVehicleType());
        }
        if (extraDetail.getVehicleNumber() != null) {
            regNumber.setText(extraDetail.getVehicleNumber());
        }

        if (extraDetail.getStatus().equals("pending")) {

            statusDotColor.setImageResource(R.drawable.red_status_dot);
            statusOrder.setText("Pending");
            statusTruck.setImageResource(R.drawable.upcoming);
            //           viewPODBtn.setVisibility(View.GONE);
        } else if (extraDetail.getStatus().equals("in process")) {

            statusDotColor.setImageResource(R.drawable.status_dot);
            statusOrder.setText("In-progress");
            statusTruck.setImageResource(R.drawable.inprogress);
            //           viewPODBtn.setVisibility(View.GONE);
        } else if (extraDetail.getStatus().equals("delivered")) {

            statusDotColor.setImageResource(R.drawable.green_status_dot);
            statusOrder.setText("Completed");
            statusTruck.setImageResource(R.drawable.compelete);
//            viewPODBtn.setVisibility(View.GONE);
        }
    }


    static long etaTime;

    private void setStatusOfItem() {

        etaTime = -1;
        boolean bool = true;
        String status;

        for (int i = 0; i < datalist.size(); i++) {

            String pickUpDropID = datalist.get(i).getIdPickUpDrop();
            String pickUpDropSt = datalist.get(i).getPickUpDrop();
            String actualTime = datalist.get(i).getActualDateTime();

            if (datalist.get(i).getActualDateTime() != null) {

                /*if (datalist.get(0).getStatus().equals("1")) {
                    break;
                }*/
            } else /*if (bool) */{

                /*bool = false;*/
                /*if (i>0) {
                    prepareEtaTimeList(i);
                }else {*/
                status = "1";

                itemDataData = new DataData();

                itemDataData.setDatetime(datalist.get(i).getDatetime());
                itemDataData.setStatus(status);
                itemDataData.setActualDateTime(actualTime);
                itemDataData.setPickUpDrop(pickUpDropSt);
                itemDataData.setIdPickUpDrop(pickUpDropID);
                itemDataData.setAddress(datalist.get(i).getAddress());
                itemDataData.setQty(datalist.get(i).getQty());
                itemDataData.setSenderReceiver(datalist.get(i).getSenderReceiver());
                itemDataData.setAssetsList(datalist.get(i).getAssetsList());

                if (datalist.get(i).getLatt() != null) {
                    Log.d("aaaaaassda", "" + datalist.get(i).getPickUpDrop() + "  " + datalist.get(i).getLatt() + "  " + datalist.get(i).getLngg());
                    itemDataData.setLatt(datalist.get(i).getLatt());
                    itemDataData.setLngg(datalist.get(i).getLngg());
                } else {
                    itemDataData.setLatt(String.valueOf(GlobalClass.latt));
                    itemDataData.setLngg(String.valueOf(GlobalClass.lngg));
                }
                itemDataData.setEtaTime(etaTime);
                if (datalist.get(i).getPickUpDrop().equals("delivery")) {
                    itemDataData.setPodImg(datalist.get(i).getPodImg());
                }

                datalist.set(i, itemDataData);
                adapter.notifyDataSetChanged();

                if (i > 0) {
                    prepareEtaTimeList(i);
                }

                break;
                //  }
            }
        }
    }

    private void sortDataList(ViewMoreClass dashboardClass) {

        for (int i = 1; i < datalist.size() - 1; i++) {
            for (int j = i + 1; j < datalist.size(); j++) {

                if (datalist.get(i).getDatetime().compareTo(datalist.get(j).getDatetime()) > 0) {

                    itemDataData = new DataData();

                    String idid = datalist.get(i).getIdPickUpDrop() + "   " + datalist.get(i).getLatt();
                    itemDataData = datalist.get(i);
                    datalist.set(i, datalist.get(j));
                    datalist.set(j, itemDataData);

                    latLngCls = new LatLngCls();

                    latLngCls = listEtaTime.get(i);
                    listEtaTime.set(i, listEtaTime.get(j));
                    listEtaTime.set(j, latLngCls);
                }
            }
        }

        adapter.notifyDataSetChanged();

        /*for (int i = 1; i < datalist.size(); i++) {
            Log.d("assets_size", "" + datalist.get(i).getSenderReceiver());
            Log.d("aaaaaas", "" + datalist.get(i).getPickUpDrop() + "  " + datalist.get(i).getLatt() + "  " + datalist.get(i).getLngg());
        }*/

        LatLngCls latLngCls;
        GlobalClass.listLatLng = new ArrayList<>();

        for (int i = 1; i < datalist.size(); i++) {

            String idddd = datalist.get(i).getIdPickUpDrop();
            Log.d("pick_drop_iid", idddd);

            latLngCls = new LatLngCls();
            latLngCls.setLat(Double.parseDouble(datalist.get(i).getLatt()));
            latLngCls.setLng(Double.parseDouble(datalist.get(i).getLngg()));

            GlobalClass.listLatLng.add(latLngCls);
        }
    }


    public void etaCal(List<LatLngCls> listLatLng, String idPickUpDrop, String pickUpDrop) {

        String url = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&waypoint0=geo!" + listLatLng.get(0).getLat() + "," + listLatLng.get(0).getLng();

        int xh = 0;
        for (int ijk = 1; ijk < listLatLng.size(); ijk++) {

            ++xh;
            url += "&waypoint" + xh + "=geo!" + listLatLng.get(ijk).getLat() + "," + listLatLng.get(ijk).getLng();
        }

        url += "&mode=fastest;truck;traffic:disabled";
        //      etaTimeMethod(url, idPickUpDrop, pickUpDrop);
    }

    long time;

    private void etaTimeMethod(String url, final int i) {

        /*String url = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&waypoint0=geo!" + listLatLng.get(ijk - 1).getLat() + "," + listLatLng.get(ijk - 1).getLng() + "&waypoint1=geo!" + listLatLng.get(ijk).getLat() + "," + listLatLng.get(ijk).getLng() + "&mode=fastest;truck;traffic:disabled";*/
        Log.d("url_string", url);

        RequestQueue queue = Volley.newRequestQueue(ViewMoreActivity.this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                EtaTime etaTime = new Gson().fromJson(response.toString(), EtaTime.class);

                time = etaTime.getResponse().getRoute().get(0).getSummary().getTrafficTime();

                latLngCls = new LatLngCls();
                latLngCls.setEtaTime(time + iii);
                listEtaTime.set(i, latLngCls);
                adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String pickUpDropID = datalist.get(i).getIdPickUpDrop();
                String pickUpDropSt = datalist.get(i).getPickUpDrop();
                String actualTime = datalist.get(i).getActualDateTime();
                String status = "1";

                itemDataData = new DataData();

                itemDataData.setDatetime(datalist.get(i).getDatetime());
                itemDataData.setStatus(status);
                itemDataData.setActualDateTime(actualTime);
                itemDataData.setPickUpDrop(pickUpDropSt);
                itemDataData.setIdPickUpDrop(pickUpDropID);
                itemDataData.setAddress(datalist.get(i).getAddress());
                itemDataData.setQty(datalist.get(i).getQty());
                itemDataData.setSenderReceiver(datalist.get(i).getSenderReceiver());
                itemDataData.setAssetsList(datalist.get(i).getAssetsList());

                if (datalist.get(i).getLatt() != null) {
                    Log.d("aaaaaassda", "" + datalist.get(i).getPickUpDrop() + "  " + datalist.get(i).getLatt() + "  " + datalist.get(i).getLngg());
                    itemDataData.setLatt(datalist.get(i).getLatt());
                    itemDataData.setLngg(datalist.get(i).getLngg());
                }/*else {
                    itemDataData.setLatt(String.valueOf(GlobalClass.latt));
                    itemDataData.setLngg(String.valueOf(GlobalClass.lngg));
                }*/
                itemDataData.setEtaTime(0);
                if (datalist.get(i).getPickUpDrop().equals("delivery")) {
                    itemDataData.setPodImg(datalist.get(i).getPodImg());
                }

                datalist.set(i, itemDataData);
                adapter.notifyDataSetChanged();
                Log.d("fkjfdasfkdf", error.toString() /*+ "  " + error.networkResponse.statusCode*/);
            }
        });

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    private void setPickUpData(List<Pickup> pickup, Integer orderId) {

        String currentDateAndTime, pickUpDrop, status, actualDateTime, idPickUpDrop;
        List<Assets> assetsList;
        Assets assets;

        for (int i = 0; i < pickup.size(); i++) {

            currentDateAndTime = pickup.get(i).getPickupDateTimeTo();
            pickUpDrop = "pickup";

            if (pickup.get(i).getActualPickupDateTime() != null) {
                status = "2";
                actualDateTime = String.valueOf(pickup.get(i).getActualPickupDateTime());
            } else {

                status = "0";
                actualDateTime = null;
            }

            idPickUpDrop = pickup.get(i).getId().toString();

            itemDataData = new DataData();

            assetsList = new ArrayList<>();
            for (int k = 0; k < pickup.get(i).getAssetList().size(); k++) {
                assets = new Assets();
                assets.setAssetId(pickup.get(i).getAssetList().get(k).getAssetId());
                assets.setRegisterationNo(pickup.get(i).getAssetList().get(k).getRegisterationNo());
                assetsList.add(assets);
            }

            itemDataData.setDatetime(currentDateAndTime);
            itemDataData.setStatus(status);
            itemDataData.setActualDateTime(actualDateTime);
            itemDataData.setPickUpDrop(pickUpDrop);
            itemDataData.setIdPickUpDrop(idPickUpDrop);
            itemDataData.setAddress(pickup.get(i).getAddress());
            itemDataData.setQty(pickup.get(i).getAssignedQuantity());
            itemDataData.setLatt(pickup.get(i).getLatitude());
            itemDataData.setLngg(pickup.get(i).getLongitude());
            itemDataData.setOrderId(String.valueOf(orderId));
            itemDataData.setEtaTime(-1);

            if (pickup.get(i).getSender() != null && pickup.get(i).getSender().getName() != null) {
                itemDataData.setSenderReceiver(pickup.get(i).getSender().getName());
            }
            itemDataData.setAssetsList(assetsList);

            datalist.add(itemDataData);

            latLngCls = new LatLngCls();
            latLngCls.setPickUpDrop(datalist.get(i).getPickUpDrop());
            latLngCls.setIdPickDrop(datalist.get(i).getIdPickUpDrop());
            latLngCls.setEtaTime(-1);
            listEtaTime.add(latLngCls);
        }
    }

    private void setDeliveryData(List<Delivery> delivery, Integer orderId) {

        String currentDateAndTime, pickUpDrop, status, actualDateTime, idPickUpDrop;
        List<Assets> assetsList;
        Assets assets;

        int sizeDelivery = delivery.size();
        for (int i = 0; i < sizeDelivery; i++) {

            currentDateAndTime = delivery.get(i).getDeliveryDateTimeTo();
            pickUpDrop = "delivery";

            if (delivery.get(i).getActualDeliveryDateTime() != null) {
                status = "2";
                actualDateTime = String.valueOf(delivery.get(i).getActualDeliveryDateTime());
            } else {

                status = "0";
                actualDateTime = null;
            }

            idPickUpDrop = delivery.get(i).getId().toString();

            assetsList = new ArrayList<>();
            int sizeAssetList = delivery.get(i).getAssetList().size();
            for (int k = 0; k < sizeAssetList; k++) {
                assets = new Assets();
                assets.setAssetId(delivery.get(i).getAssetList().get(k).getAssetId());
                assets.setRegisterationNo(delivery.get(i).getAssetList().get(k).getRegisterationNo());
                assetsList.add(assets);
            }

            itemDataData = new DataData();

            itemDataData.setDatetime(currentDateAndTime);
            itemDataData.setStatus(status);
            itemDataData.setActualDateTime(actualDateTime);
            itemDataData.setPickUpDrop(pickUpDrop);
            itemDataData.setIdPickUpDrop(idPickUpDrop);
            itemDataData.setAddress(delivery.get(i).getAddress());
            itemDataData.setQty(delivery.get(i).getAssignedQuantity());
            itemDataData.setPodImg(String.valueOf(delivery.get(i).getPodImage()));
            itemDataData.setLatt(delivery.get(i).getLatitude());
            itemDataData.setLngg(delivery.get(i).getLongitude());
            itemDataData.setOrderId(String.valueOf(orderId));
            itemDataData.setEtaTime(-1);

            if (delivery.get(i).getReceiver() != null && delivery.get(i).getReceiver().getName() != null) {
                String receiverName = delivery.get(i).getReceiver().getName();
                itemDataData.setSenderReceiver(receiverName);
            }
            itemDataData.setAssetsList(assetsList);

            datalist.add(itemDataData);

            latLngCls = new LatLngCls();
            latLngCls.setPickUpDrop(datalist.get(i).getPickUpDrop());
            latLngCls.setIdPickDrop(datalist.get(i).getIdPickUpDrop());
            latLngCls.setEtaTime(-1);
            listEtaTime.add(latLngCls);
        }
    }

    private void setEnRouteTime(Object orderEnroute) {

        String currentDateAndTime, pickUpDrop, status, actualDateTime, idPickUpDrop;

        if (datalist.size() > 0) {

            String strstrstr = String.valueOf(orderEnroute);

            if (orderEnroute != null) {
                currentDateAndTime = strstrstr;
                status = "2";
                actualDateTime = currentDateAndTime;
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                currentDateAndTime = sdf.format(Calendar.getInstance().getTime());

                status = "1";
                actualDateTime = null;
            }
            pickUpDrop = "en-route";
            idPickUpDrop = "";

            itemDataData = new DataData();

            itemDataData.setDatetime(currentDateAndTime);
            itemDataData.setStatus(status);
            itemDataData.setActualDateTime(actualDateTime);
            itemDataData.setPickUpDrop(pickUpDrop);
            itemDataData.setIdPickUpDrop(idPickUpDrop);
            itemDataData.setAssetsList(new ArrayList<Assets>());
            itemDataData.setLatt(String.valueOf(GlobalClass.latt));
            itemDataData.setLngg(String.valueOf(GlobalClass.lngg));
            itemDataData.setEtaTime(0);

            datalist.add(0, itemDataData);
            latLngCls = new LatLngCls();
            latLngCls.setPickUpDrop(pickUpDrop);
            latLngCls.setIdPickDrop(idPickUpDrop);
            latLngCls.setEtaTime(-1);
            listEtaTime.add(latLngCls);
        }
    }

    private void setPickUpDeliveryAddressDate(ArrayList<DataData> datalist) {

        addressPickUp.setText(datalist.get(1).getAddress());

        String[] datePick = datalist.get(1).getDatetime().split("T");
        try {
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(datePick[0]);

            String ss = new SimpleDateFormat("dd/MM/yy").format(d);
            datePickUp.setText(ss);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        addressDelivery.setText(datalist.get(datalist.size() - 1).getAddress());

        String[] dateDeliverydd = datalist.get(datalist.size() - 1).getDatetime().split("T");

        try {
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(dateDeliverydd[0]);

            String ss = new SimpleDateFormat("dd/MM/yy").format(d);
            dateDelivery.setText(ss);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setLogoTxtandDimens(String customerCompanyName) {

        String text = customerCompanyName;

        if (!(text.toLowerCase().equals("na"))) {

            String nameWithProperSpacing = text.replaceAll("\\s+", " ");

            String[] txtArray = nameWithProperSpacing.trim().split(" ");

            String logoTxtt;
            if (txtArray.length > 2) {
                logoTxtt = String.valueOf(txtArray[0].charAt(0)) + String.valueOf(txtArray[1].charAt(0));
            } else {
                logoTxtt = String.valueOf(txtArray[0].charAt(0)) + String.valueOf(txtArray[0].charAt(1));
            }

            String first = logoTxtt.toUpperCase();

            logoTxt.setText(first);
            nameCustomer.setText(text);
        }
        /*carried.setText(dashboardClass.getMain().get(0).getExtraDetail().getLoadCarried());
        carriedType.setText(dashboardClass.getMain().get(0).getExtraDetail().getLoadType());
        noOfUnits.setText(dashboardClass.getMain().get(0).getExtraDetail().getUnits());
        weightItem.setText(dashboardClass.getMain().get(0).getExtraDetail().getLoadWeight());

        String dimens1 = dashboardClass.getMain().get(0).getExtraDetail().getLengthLoadDimen();
        String dimens2 = dashboardClass.getMain().get(0).getExtraDetail().getWidthLoadDimen();
        String dimens3 = dashboardClass.getMain().get(0).getExtraDetail().getHeightLoadDimen();
        String dimens4 = dashboardClass.getMain().get(0).getExtraDetail().getMeasureUnit();
        String dimens = dimens1 + "*" + dimens2 + "*" + dimens3 + " " + dimens4;
        dimensionItem.setText(dimens);*/
    }

    private void callBtnEvent(ImageButton btnCall, final String customerCompanyNumber) {
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contactNumber = customerCompanyNumber;

                if (contactNumber != null && contactNumber != "null" && contactNumber != "") {

                    Uri u = Uri.parse("tel:" + contactNumber);

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + contactNumber));

                    startActivity(callIntent);
                } else {
                    Toast.makeText(ViewMoreActivity.this, "Contact number not available", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setDataAdditionalRecyclrVw(List<Attachment> attachment) {

        AttachmentListAdapter adapter1 = new AttachmentListAdapter(ViewMoreActivity.this, attachment);

        recyclrVwAdditionAtt.setHasFixedSize(true);
        recyclrVwAdditionAtt.setLayoutManager(new LinearLayoutManager(ViewMoreActivity.this));
        recyclrVwAdditionAtt.setAdapter(adapter1);
        recyclrVwAdditionAtt.setItemAnimator(new DefaultItemAnimator());
    }


    int position, listSize;
    String idPickUpDrop;
    boolean capturePOD;

    @Override
    protected void onRestart() {
        super.onRestart();
        if (capturePOD){
            capturePOD = false;
            dialog.show();
        }
    }

    public void captureImg(int position, String idPickUpDrop, int listSize) {

        capturePOD = true;

        this.position = position;
        this.listSize = listSize;
        this.idPickUpDrop = idPickUpDrop;

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New_Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From_Your_Camera");

        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private File getFile() {
        File img_file = null;
        try {
            File folder = new File("sdcard/camera");
            if (!folder.exists()) {
                folder.mkdir();
            }
            img_file = new File(folder, "imagess.jpg");
        } catch (Exception ex) {
        }
        return img_file;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                getResizedBitmap(bitmap, 1100);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap.createScaledBitmap(image, width, height, true).compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        String str = imageString;
        capturePodDelivery(str);
    }


    private void capturePodDelivery(String imageString) {

        if (!dialog.isShowing()) {
            dialog.show();
        }
        Calendar calendar = Calendar.getInstance();
        Date d = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        final String dateTimeActual = simpleDateFormat.format(d);

        final JSONObject jsonObject = new JSONObject();
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObject.put("drop_id", idPickUpDrop);
            jsonObject.put("actual_delivery_date_time", dateTimeActual);

            jsonObj.put("update_data", jsonObject);

            jsonObj.put("order_id", preferences.getString(GlobalClass.orderId, null));
            jsonObj.put("carr_id", preferences.getString(GlobalClass.carrierId, null));

            jsonObj.put("driver_name", preferences.getString(GlobalClass.nameUser, null));
            jsonObj.put("order_job", preferences.getString(GlobalClass.jobNo, null));
            jsonObj.put("pod_img", imageString);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiCallCls apiCallCls = new ApiCallCls() {
            @Override
            public Response.Listener<JSONObject> responseJsonReqSuccess() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (position == listSize) {

                            String url = GlobalClass.lastDeliveryStatusChangeTime + "?en_route_or_pod=pod";

                            Log.d("response_capture_pod", response.toString());
                            final JSONObject jsonObj = new JSONObject();

                            try {
                                jsonObj.put("order_id", preferences.getString(GlobalClass.orderId, null));
                                jsonObj.put("carr_id", preferences.getString(GlobalClass.carrierId, null));
                                jsonObj.put("en_route_time", dateTimeActual);
                                jsonObj.put("final_delivery_date_time", dateTimeActual);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            RequestQueue queue = Volley.newRequestQueue(ViewMoreActivity.this);
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    Log.d("response_last_delivery", response.toString());
                                    GlobalClass.boolDashBoard = false;
                                    /*startActivity(new Intent(ViewMoreActivity.this, ViewMoreActivity.class));*/
                                    onStart();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    dialog.dismiss();
                                    volleyErrorResponse(error);
                                }
                            }) {
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {

                                    HashMap<String, String> headers = new HashMap<>();
                                    headers.put("Content-Type", "application/json");
                                    headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));

                                    return headers;
                                }
                            };

                            queue.add(request);
                        } else {
                            onStart();
                        }
                    }
                };
            }
        };
        String url = GlobalClass.actualDeliveryTime + "?order_id=" + preferences.getString(GlobalClass.orderId, null) + "&carr_id=" + preferences.getString(GlobalClass.carrierId, null);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));
        apiCallCls.jsonPostMethod(this, url, jsonObj, dialog, headers);
    }

    private void updateList() {

        Calendar calendar = Calendar.getInstance();
        Date d = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        final String dateTimeActual = simpleDateFormat.format(d);

        itemDataData = new DataData();

        itemDataData.setDatetime(ViewMoreDetailAdapter.list.get(position).getDatetime());
        itemDataData.setStatus("2");
        itemDataData.setActualDateTime(dateTimeActual);
        itemDataData.setPickUpDrop(ViewMoreDetailAdapter.list.get(position).getPickUpDrop());
        itemDataData.setIdPickUpDrop(ViewMoreDetailAdapter.list.get(position).getIdPickUpDrop());
        itemDataData.setAddress(ViewMoreDetailAdapter.list.get(position).getAddress());
        itemDataData.setQty(ViewMoreDetailAdapter.list.get(position).getQty());

        ViewMoreDetailAdapter.list.set(position, itemDataData);
        if (position < ViewMoreDetailAdapter.list.size() - 1) {

            DataData itemDataData22 = new DataData();

            itemDataData22.setDatetime(ViewMoreDetailAdapter.list.get(position + 1).getDatetime());
            itemDataData22.setStatus("1");
            itemDataData22.setActualDateTime(ViewMoreDetailAdapter.list.get(position + 1).getActualDateTime());
            itemDataData22.setPickUpDrop(ViewMoreDetailAdapter.list.get(position + 1).getPickUpDrop());
            itemDataData22.setIdPickUpDrop(ViewMoreDetailAdapter.list.get(position + 1).getIdPickUpDrop());
            itemDataData22.setAddress(ViewMoreDetailAdapter.list.get(position + 1).getAddress());
            itemDataData22.setQty(ViewMoreDetailAdapter.list.get(position + 1).getQty());

            ViewMoreDetailAdapter.list.set(position + 1, itemDataData22);
        }
        adapter.notifyDataSetChanged();
    }

    public void volleyErrorResponse(VolleyError error) {

        Log.d("response_volley_err", error.toString());

        if (error.networkResponse != null) {
            Log.d("response_volley_err2", String.valueOf(error.networkResponse.statusCode));
        } else {
            Toast.makeText(this, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

// Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_volley_err3", obj.toString());
            } catch (UnsupportedEncodingException e1) {
// Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
// returned data is not JSONObject?
                e2.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }

        new GlobalClass().inValidToken(ViewMoreActivity.this, error);
    }




    //           etaCal(listLatLng, datalist.get(i).getIdPickUpDrop(), datalist.get(i).getPickUpDrop());

            /*LatLngCls latLngCls;

            latLngCls = new LatLngCls();
            latLngCls.setIdPickDrop("0");
            latLngCls.setLat(GlobalClass.latt);
            latLngCls.setLng(GlobalClass.lngg);
            listLatLng.add(latLngCls);

            for (int i = 1; i < datalist.size(); i++) {
                latLngCls = new LatLngCls();
                if (!(datalist.get(i).getActualDateTime() != null)) {

                    double ltd = Double.parseDouble(datalist.get(i).getLatt());
                    double lgd = Double.parseDouble(datalist.get(i).getLngg());
                    latLngCls.setLat(ltd);
                    latLngCls.setLng(lgd);
                    latLngCls.setIdPickDrop(datalist.get(i).getIdPickUpDrop());
                    latLngCls.setPickUpDrop(datalist.get(i).getPickUpDrop());
                    listLatLng.add(latLngCls);

                    etaCal(listLatLng, datalist.get(i).getIdPickUpDrop(), datalist.get(i).getPickUpDrop());
                }
            }*/



            /*addNotesShowHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boolAddNotes){
                    addNotesShowHide.setVisibility(View.VISIBLE);
                    boolAddNotes = false;
                }else {
                    addNotesShowHide.setVisibility(View.GONE);
                    boolAddNotes = true;
                }
            }
        });*/




     /*String pickUpDropID = datalist.get(i).getIdPickUpDrop();
                String pickUpDropSt = datalist.get(i).getPickUpDrop();
                String actualTime = datalist.get(i).getActualDateTime();
                String status = "1";

                itemDataData = new DataData();

                itemDataData.setDatetime(datalist.get(i).getDatetime());
                itemDataData.setStatus(status);
                itemDataData.setActualDateTime(actualTime);
                itemDataData.setPickUpDrop(pickUpDropSt);
                itemDataData.setIdPickUpDrop(pickUpDropID);
                itemDataData.setAddress(datalist.get(i).getAddress());
                itemDataData.setQty(datalist.get(i).getQty());
                itemDataData.setSenderReceiver(datalist.get(i).getSenderReceiver());
                itemDataData.setAssetsList(datalist.get(i).getAssetsList());

                if (datalist.get(i).getLatt() != null) {
                    Log.d("aaaaaassda", "" + datalist.get(i).getPickUpDrop() + "  " + datalist.get(i).getLatt() + "  " + datalist.get(i).getLngg());
                    itemDataData.setLatt(datalist.get(i).getLatt());
                    itemDataData.setLngg(datalist.get(i).getLngg());
                }*//*else {
                    itemDataData.setLatt(String.valueOf(GlobalClass.latt));
                    itemDataData.setLngg(String.valueOf(GlobalClass.lngg));
                }
                itemDataData.setEtaTime(time);
                if (datalist.get(i).getPickUpDrop().equals("delivery")) {
                    itemDataData.setPodImg(datalist.get(i).getPodImg());
                }

                datalist.set(i, itemDataData);*/


    /*for (int sa = 0; sa < listEtaTime.size(); sa++) {

                    if (listEtaTime.get(sa).getIdPickDrop().equals(idPickUpDrop) && listEtaTime.get(sa).getPickUpDrop().equals(pickUpDrop)) {


                        latLngCls = new LatLngCls();
                        latLngCls.setIdPickDrop(listEtaTime.get(sa).getIdPickDrop());
                        latLngCls.setPickUpDrop(listEtaTime.get(sa).getPickUpDrop());
                        latLngCls.setEtaTime(time);
                        listEtaTime.set(sa, latLngCls);
                        adapter.notifyDataSetChanged();

                        break;
                    }
                }*/

    /*    Log.d("fkjfdasfkdf", "" + ijk + "   " + time + "   " + response.toString());*/
}

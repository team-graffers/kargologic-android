package com.kargologic.app.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kargologic.app.R;


public class PrivacyPolicyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_policy);
    }
}

package com.kargologic.app.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.pojoClasses.ViewMore.OrderComment;
import com.kargologic.app.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//
// Created by Sandy on 04-Jun-19 at 7:56 PM.
//
public class AdapterChat extends RecyclerView.Adapter<AdapterChat.RecyclrVwHolder>{

    Context context;
    LayoutInflater inflater;
    List<OrderComment> list;

    public AdapterChat(Context context, List<OrderComment> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclrVwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.item_chat, parent, false);
        return new RecyclrVwHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclrVwHolder holder, int position) {

        if (list.get(position).getCreatedByCustomerID() != null && list.get(position).getCreatedByCustomerID().getName() != null){
            holder.name.setText(new GlobalClass().convert(list.get(position).getCreatedByCustomerID().getName()+":"));

        }else if (list.get(position).getCreatedByCarrierID() != null && list.get(position).getCreatedByCarrierID().getName() != null){
            holder.name.setText(new GlobalClass().convert(list.get(position).getCreatedByCarrierID().getName()+":"));

        }else if (list.get(position).getCreatedByDriverID() != null && list.get(position).getCreatedByDriverID().getName() != null){
            holder.name.setText(new GlobalClass().convert(list.get(position).getCreatedByDriverID().getName()+":"));
        }
        holder.chatTxt.setText(list.get(position).getComment());

        if(list.get(position).getDate_timeOfCreate() != null && list.get(position).getDate_timeOfCreate().length()>0){
            String timeAndDate = String.valueOf(list.get(position).getDate_timeOfCreate());

            String dateTime[] = timeAndDate.split("T");

            final String listdate = dateTime[0];
            String[] listtime = dateTime[1].split("\\.");
            try {
                Date d = new SimpleDateFormat("yyyy-MM-dd").parse(listdate);
                String dd = new SimpleDateFormat("dd/MM/yy").format(d);

                Date dt = new SimpleDateFormat("HH:mm:ss").parse(listtime[0]);
                String tt = new SimpleDateFormat("hh:mm aa").format(dt);
                holder.timeDate.setText("["+dd+" "+tt+"] ");
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
 //       holder.timeDate.setText("");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclrVwHolder extends RecyclerView.ViewHolder {

        TextView name, chatTxt, timeDate;
        public RecyclrVwHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.chat_name);
            chatTxt = (TextView) itemView.findViewById(R.id.chat_txt);
            timeDate = (TextView) itemView.findViewById(R.id.chat_time_date);
        }
    }
}

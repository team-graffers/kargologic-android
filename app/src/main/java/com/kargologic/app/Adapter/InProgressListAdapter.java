package com.kargologic.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kargologic.app.activities.ViewMoreActivity;
import com.kargologic.app.classes.DataListInProgressFrag;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.drawable_vector_color_change.VectorChildFinder;
import com.kargologic.app.drawable_vector_color_change.VectorDrawableCompat;
import com.kargologic.app.pojoClasses.EtaTimeCls;
import com.kargologic.app.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class InProgressListAdapter extends RecyclerView.Adapter<InProgressListAdapter.ViewHolder> {

    List<DataListInProgressFrag> list;
    List<EtaTimeCls> listEtaRoute;
    List<EtaTimeCls> listEtaNext;
    private Context context;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    //ArrayList<String> list22;

    public InProgressListAdapter(Context context, List<DataListInProgressFrag> list, List<EtaTimeCls> listEtaRoute, List<EtaTimeCls> listEtaNext) {
        this.list = list;
        this.listEtaRoute = listEtaRoute;
        this.listEtaNext = listEtaNext;
        this.context = context;

        preferences = context.getSharedPreferences(GlobalClass.preferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        // this.list22 = list22;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.list_data_inprogress, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


        VectorChildFinder vector = new VectorChildFinder(context, R.drawable.ic_bulleticon_svg, holder.imageView);

        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");

            /*if (listColor.get(position) == 0){
                path1.setFillColor(context.getResources().getColor(R.color.a1));
            }else if (listColor.get(position) == 1){
                path1.setFillColor(context.getResources().getColor(R.color.a2));
            }else if (listColor.get(position) == 2){
                path1.setFillColor(context.getResources().getColor(R.color.a3));
            }else if (listColor.get(position) == 3){
                path1.setFillColor(context.getResources().getColor(R.color.a4));
            }else if (listColor.get(position) == 4){
                path1.setFillColor(context.getResources().getColor(R.color.a5));
            }*/
        path1.setFillColor(context.getResources().getColor(new GlobalClass().bulletBackColor[list.get(position).getColorLogo()]));

        holder.logotext.setTextColor(context.getResources().getColor(new GlobalClass().bulletFontColor[list.get(position).getColorLogo()]));

        holder.name.setText(list.get(position).getCompanyName());
        holder.jobno.setText(list.get(position).getJobNumber());

        String text = holder.name.getText().toString();

        String nameWithProperSpacing = text.replaceAll("\\s+", " ");

        String[] txtArray = nameWithProperSpacing.trim().split(" ");

        String logoTxt;
        if (txtArray.length > 2) {

            logoTxt = String.valueOf(txtArray[0].charAt(0)) + String.valueOf(txtArray[1].charAt(0));
        } else {
            logoTxt = String.valueOf(txtArray[0].charAt(0)) + String.valueOf(txtArray[0].charAt(1));
        }

        String first = logoTxt.toUpperCase();

        holder.logotext.setText(first);

        holder.pickupaddress.setText(list.get(position).getPickUpAddress());

        String jsonString = String.valueOf(list.get(position).getPickUpDate());

        try {
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(jsonString);

            String ss = new SimpleDateFormat("dd/MM/yy").format(d);
            holder.pickupdate.setText(ss);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.delieveraddress.setText(list.get(position).getDeliveryAddress());

        String jsonString1 = String.valueOf(list.get(position).getDeliveryDate());

        try {
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(jsonString1);

            String ss = new SimpleDateFormat("dd/MM/yy").format(d);
            holder.delieverdate.setText(ss);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.viewmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                detailActivity(position);
                /*if (new InternetGpsCheck(context).isGpsEnabled()) {
                    new CurrentLocation(context).lattLong();
                    new CurrentLocation(context).liveLocation();
                    detailActivity(position);
                }else {
                    Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
                }*/
            }
        });
        holder.viewMoreLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                detailActivity(position);
                /*if (new InternetGpsCheck(context).isGpsEnabled()) {
                    new CurrentLocation(context).lattLong();
                    new CurrentLocation(context).liveLocation();
                    detailActivity(position);
                }else {
                    Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        holder.callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String contactNumber = list.get(position).getCompanyContact();

                if (contactNumber != null && contactNumber != "null" && contactNumber != "") {

                    Uri u = Uri.parse("tel:" + contactNumber);

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + contactNumber));

                    context.startActivity(callIntent);
                } else {
                    Toast.makeText(context, "Contact number not available", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.estimate.setVisibility(View.GONE);
        holder.estimated.setVisibility(View.GONE);
        String etaMsg = "<font color=#F52F2F>ETA can't be calculated</font>";
        if (listEtaRoute.get(position).getTimeEta() == 0) {
            holder.estimatedelievertime.setText(Html.fromHtml(etaMsg));
        } else {

            int days = (int) listEtaRoute.get(position).getTimeEta() / (24 * 3600);
            long hours = listEtaRoute.get(position).getTimeEta() % (24 * 3600);
            int minuts = (int) (hours % 3600);
            int seconds = minuts % 60;

            int hhh = (int) (hours / 3600);
            int mmm = minuts / 60;
            holder.estimatedelievertime.setText("" + days + "D " + hhh + "H " + mmm + "M");
        }

        if (listEtaNext.get(position).getTimeEta() == 0) {
            holder.estimatepickptime.setText(Html.fromHtml(etaMsg));
        }else{
            int days = (int) listEtaNext.get(position).getTimeEta() / (24 * 3600);
            long hours = listEtaNext.get(position).getTimeEta() % (24 * 3600);
            int minuts = (int) (hours % 3600);
            int seconds = minuts % 60;

            int hhh = (int) (hours / 3600);
            int mmm = minuts / 60;
            holder.estimatepickptime.setText("" + days + "D " + hhh + "H " + mmm + "M");
        }

        if (list.get(position).getActualTime() != null && list.get(position).getActualTime() != "null") {
            holder.estimatepickptime.setText("Completed");
            holder.estimate.setVisibility(View.GONE);
        }

        Log.d("eta_abc", "job:" + list.get(position).getJobNumber() + "  next:" + listEtaNext.get(position) + "  total:" + listEtaRoute.get(position));

        //       Log.d("eta_time", "Adapter");
    }

    private void detailActivity(final int position) {

        editor.putString(GlobalClass.orderId, list.get(position).getOrderId());
        editor.commit();
        context.startActivity(new Intent(context, ViewMoreActivity.class));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView logotext, name, jobno, pickupaddress, delieveraddress, pickupdate, delieverdate, estimatepickptime, estimatedelievertime, estimate, estimated;
        Button viewmore;
        ImageView imageView;
        Button callBtn;
        LinearLayout viewMoreLayout;

        public ViewHolder(View v) {
            super(v);

            logotext = v.findViewById(R.id.logo_textview);
            imageView = (ImageView) v.findViewById(R.id.logo_textview_back);
            name = v.findViewById(R.id.name_textview);
            jobno = v.findViewById(R.id.job_no);
            pickupaddress = v.findViewById(R.id.pickup_address);
            delieveraddress = v.findViewById(R.id.deliever_address);
            pickupdate = v.findViewById(R.id.pickup_date);
            delieverdate = v.findViewById(R.id.deliever_date);
            estimatedelievertime = v.findViewById(R.id.estimate_delievertime);
            estimatepickptime = v.findViewById(R.id.estimate_pickuptime);
            viewmore = v.findViewById(R.id.viewmore);
            callBtn = (Button) v.findViewById(R.id.call_btn);
            viewMoreLayout = (LinearLayout) v.findViewById(R.id.view_more_layout);

            estimate = (TextView) v.findViewById(R.id.estimate);
            estimated = (TextView) v.findViewById(R.id.estimated);
        }
    }
}

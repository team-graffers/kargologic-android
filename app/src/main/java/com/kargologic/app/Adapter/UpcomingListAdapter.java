package com.kargologic.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kargologic.app.activities.ViewMoreActivity;
import com.kargologic.app.classes.CurrentLocation;
import com.kargologic.app.classes.DataListInProgressFrag;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.classes.InternetGpsCheck;
import com.kargologic.app.drawable_vector_color_change.VectorChildFinder;
import com.kargologic.app.drawable_vector_color_change.VectorDrawableCompat;
import com.kargologic.app.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class UpcomingListAdapter extends RecyclerView.Adapter<UpcomingListAdapter.ViewHolder> {

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    private List<DataListInProgressFrag> list;
    private Context context;

    public UpcomingListAdapter(Context context, List<DataListInProgressFrag> list) {
        this.list = list;
        this.context = context;

        preferences = context.getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        if (new InternetGpsCheck(context).isGpsEnabled()) {
            new CurrentLocation(context).lattLong();
            new CurrentLocation(context).liveLocation();
        }else {
            Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_data_upcoming, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


        VectorChildFinder vector = new VectorChildFinder(context, R.drawable.ic_bulleticon_svg, holder.imageView);

        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");

        path1.setFillColor(context.getResources().getColor(new GlobalClass().bulletBackColor[list.get(position).getColorLogo()]));

        holder.logotext.setTextColor(context.getResources().getColor(new GlobalClass().bulletFontColor[list.get(position).getColorLogo()]));

        holder.name.setText(list.get(position).getCompanyName());
        holder.jobno.setText(list.get(position).getJobNumber());


        holder.pickupaddress.setText(list.get(position).getPickUpAddress());

        String jsonString = String.valueOf(list.get(position).getPickUpDate());

        String date[] = jsonString.split("T");

        try {
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(date[0]);

            String ss = new SimpleDateFormat("dd/MM/yy").format(d);
            holder.pickupdate.setText(ss);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.delieveraddress.setText(list.get(position).getDeliveryAddress());

        String jsonString1 = String.valueOf(list.get(position).getDeliveryDate());

        String ddate[] = jsonString1.split("T");
        try {
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(ddate[0]);

            String ss = new SimpleDateFormat("dd/MM/yy").format(d);
            holder.delieverdate.setText(ss);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String text = holder.name.getText().toString();
        String nameWithProperSpacing = text.replaceAll("\\s+", " ");

        String[] txtArray = nameWithProperSpacing.trim().split(" ");

        String logoTxt;
        if (txtArray.length > 2) {
            logoTxt = txtArray[0].charAt(0) + String.valueOf(txtArray[1].charAt(0));
        } else {
            logoTxt = txtArray[0].charAt(0) + String.valueOf(txtArray[0].charAt(1));
        }

        String first = logoTxt.toUpperCase();
        holder.logotext.setText(first);

        holder.upcomingviewmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailActivity(position);
            }
        });
        holder.viewMoreLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailActivity(position);
            }
        });

        holder.callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String contactNumber = list.get(position).getCompanyContact();

                if (contactNumber != null && contactNumber != "null" && contactNumber != "") {

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + contactNumber));
                    context.startActivity(callIntent);
                } else {
                    Toast.makeText(context, "Contact number not available", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void detailActivity(final int position) {

        editor.putString(GlobalClass.orderId, list.get(position).getOrderId());
        editor.commit();

        context.startActivity(new Intent(context, ViewMoreActivity.class));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imageView;
        TextView logotext, name, jobno, pickupaddress, delieveraddress, pickupdate, delieverdate;
        Button upcomingviewmore;
        Button callBtn;
        LinearLayout viewMoreLayout;

        public ViewHolder(View v) {
            super(v);

            logotext = v.findViewById(R.id.logo_textview);
            imageView = (ImageView) v.findViewById(R.id.logo_textview_back);
            name = v.findViewById(R.id.name_textview);
            jobno = v.findViewById(R.id.job_no);
            pickupaddress = v.findViewById(R.id.pickup_address);
            delieveraddress = v.findViewById(R.id.deliever_address);
            pickupdate = v.findViewById(R.id.pickup_date);
            delieverdate = v.findViewById(R.id.deliever_date);
            upcomingviewmore = v.findViewById(R.id.upcoming_viewmore);
            callBtn = (Button) v.findViewById(R.id.call_btn);
            viewMoreLayout = (LinearLayout) v.findViewById(R.id.view_more_layout);
        }
    }
}

package com.kargologic.app.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kargologic.app.pojoClasses.ViewMore.Assets;
import com.kargologic.app.R;

import java.util.List;

/**
 * Created by Sandy on 06/08/2019 at 03:51 PM.
 */
public class AssetsAdapter extends RecyclerView.Adapter<AssetsAdapter.VwHolder> {

    Context context;
    List<Assets> assetsList;
    LayoutInflater inflater;

    public AssetsAdapter(Context context, List<Assets> assetsList) {
        this.context = context;
        this.assetsList = assetsList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_asset_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, int position) {
        holder.assetId.setText(""+assetsList.get(position).getAssetId());
        holder.assetType.setText(assetsList.get(position).getRegisterationNo());
    }

    @Override
    public int getItemCount() {
        return assetsList.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {
        TextView assetId, assetType;
        public VwHolder(@NonNull View itemView) {
            super(itemView);
            assetId = (TextView) itemView.findViewById(R.id.asset_id);
            assetType = (TextView) itemView.findViewById(R.id.asset_type);
        }
    }
}

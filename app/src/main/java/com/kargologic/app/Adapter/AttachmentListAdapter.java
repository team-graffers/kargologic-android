package com.kargologic.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kargologic.app.pojoClasses.ViewMore.Attachment;
import com.kargologic.app.R;

import java.util.List;

public class AttachmentListAdapter extends RecyclerView.Adapter<AttachmentListAdapter.ViewHolder> {

    Context context;
    private List<Attachment> list22;

    public AttachmentListAdapter(Context context, List<Attachment> list22) {
        this.list22 = list22;
        this.context = context;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_data_attachments, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


       holder.ordersheet.setText(list22.get(position).getAttachmentName());

       holder.downloadicon.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if (list22.get(position).getPhysicalPath() != null) {
                   Uri uri = Uri.parse(list22.get(position).getPhysicalPath()); // missing 'http://' will cause crashed
                   Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                   context.startActivity(intent);
               }
           }
       });
    }


    @Override
    public int getItemCount() {
        return list22.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView ordersheet, sheetsize;
        ImageView attachicon,downloadicon;

        public ViewHolder(View v) {
            super(v);

            ordersheet = v.findViewById(R.id.ordersheet);
            sheetsize = v.findViewById(R.id.ordersheet_size);
            attachicon=v.findViewById(R.id.logo_attachment);
            downloadicon=v.findViewById(R.id.download_icon);
        }
    }
}

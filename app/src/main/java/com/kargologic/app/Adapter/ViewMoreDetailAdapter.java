package com.kargologic.app.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kargologic.app.activities.ViewMoreActivity;
import com.kargologic.app.activities.ViewPodActivity;
import com.kargologic.app.classes.ApiCallCls;
import com.kargologic.app.classes.CurrentLocation;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.classes.InternetGpsCheck;
import com.kargologic.app.classes.LatLngCls;
import com.kargologic.app.pojoClasses.ViewMore.Assets;
import com.kargologic.app.pojoClasses.ViewMore.DataData;
import com.kargologic.app.pojoClasses.eta.EtaTime;
import com.kargologic.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.json.JSONObject.NULL;

public class ViewMoreDetailAdapter extends RecyclerView.Adapter<ViewMoreDetailAdapter.ViewHolder> {

//    private final Dialog dialog;
    public static List<DataData> list;
    Context context;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    List<LatLngCls> listEtaTime;
    int pos;
    ViewMoreActivity activity;

    public ViewMoreDetailAdapter(Context context, List<DataData> list, String string, List<LatLngCls> listEtaTime) {

        this.list = list;
        this.listEtaTime = listEtaTime;
        this.context = context;

        activity = (ViewMoreActivity) context;

        preferences = context.getSharedPreferences(GlobalClass.preferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();

        /*dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progress_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);*/

        if (new InternetGpsCheck(context).isGpsEnabled()) {
            new CurrentLocation(context).lattLong();
            new CurrentLocation(context).lattLong();
        } else {
            Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.item_list_data_viewmore, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        if (position == list.size() - 1) {
            holder.dashPickupComplete.setVisibility(View.GONE);
        }

        holder.tickPickup.setVisibility(View.VISIBLE);
        holder.qty.setVisibility(View.VISIBLE);
        holder.senderReceiver.setVisibility(View.VISIBLE);
        int i = position;
        String address = list.get(position).getAddress();
        holder.address.setText(address);

        String etaTimeStr = "";
        long time = 0;

        if (position > 0) {

            time = listEtaTime.get(position).getEtaTime();

            if (time == -1) {
                if (list.get(position).getActualDateTime() != null) {
                    etaTimeStr = "";
                } else {
                    if (new InternetGpsCheck(context).isGpsEnabled()) {
                        etaTimeStr = "ETA";
                    }else {
                        /*etaTimeStr = "<font color=#F52F2F>ETA can't be calculated</font>" ;*/
                        etaTimeStr = "ETA can't be calculated" ;
                    }
                }
            } else if (time == 0){
                /*etaTimeStr = "<font color=#F52F2F>ETA can't be calculated</font>" ;*/
                etaTimeStr = "ETA can't be calculated" ;
            }else {
                int days = (int) time / (24 * 3600);
                long hours = time % (24 * 3600);
                int minuts = (int) (hours % 3600);
                int seconds = minuts % 60;

                int hhh = (int) (hours / 3600);
                int mmm = minuts / 60;
                etaTimeStr = "" + days + "D " + hhh + "H " + mmm + "M";
            }
        }

        if (position > 0 && list.get(position).getQty() != null) {
            holder.qty.setText("Qty: " + list.get(position).getQty());
        }

        String jsonString;
        String jsonStringOn;

        int sizeAsset = list.get(position).getAssetsList().size();
        if (sizeAsset != 0) {
            holder.assetCounter.setText("Assets: " + sizeAsset);
            holder.assetsLayout.setVisibility(View.VISIBLE);
        } else {
            holder.assetsLayout.setVisibility(View.GONE);
        }

        holder.assetCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assetsList(list.get(position).getAssetsList());
            }
        });

        if (list.get(position).getActualDateTime() != null && list.get(position).getActualDateTime() != "null" && list.get(position).getActualDateTime().length() > 0) {

            jsonStringOn = list.get(position).getActualDateTime();
            String dateTime[] = jsonStringOn.split("T");

            final String listdate = dateTime[0];
            String listtime = dateTime[1];
            try {
                Date d = new SimpleDateFormat("yyyy-MM-dd").parse(listdate);
                String ss = new SimpleDateFormat("dd/MM/yy").format(d);
                holder.dateOn.setText("ON: " + ss);

                Date dd = new SimpleDateFormat("HH:mm:ss'Z'").parse(listtime);
                String st = new SimpleDateFormat("hh:mm aa").format(dd);
                holder.timeOn.setText(st);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.dateOn.setVisibility(View.VISIBLE);
            holder.timeOn.setVisibility(View.VISIBLE);
        } else {
            holder.dateOn.setVisibility(View.GONE);
            holder.timeOn.setVisibility(View.GONE);
        }
        jsonString = String.valueOf(list.get(i).getDatetime());

        String dateTime[] = jsonString.split("T");

        final String listdate = dateTime[0];
        String listtime = dateTime[1];
        try {
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(listdate);
            String ss = new SimpleDateFormat("dd/MM/yy").format(d);
            holder.date.setText(ss);

            Date dd = new SimpleDateFormat("HH:mm:ss'Z'").parse(listtime);
            String st = new SimpleDateFormat("hh:mm aa").format(dd);
            holder.time.setText(st);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String pickndrop = list.get(position).getPickUpDrop();

        holder.capturePod.setVisibility(View.GONE);
        holder.btnViewPodImg.setVisibility(View.GONE);

        if (pickndrop.equals("en-route")) {

            holder.dateOn.setVisibility(View.GONE);
            holder.timeOn.setVisibility(View.GONE);

            holder.textViewPickup.setText("En-route to Pick-Up");
            holder.markComplete.setText("En-route");

            holder.etaBlue.setVisibility(View.GONE);

            holder.qty.setVisibility(View.GONE);
            holder.senderReceiver.setVisibility(View.GONE);
            holder.directionBtn.setVisibility(View.GONE);

            if (list.get(position).getStatus().equals("2")) {

                holder.markComplete.setVisibility(View.GONE);
                holder.pickupTruck.setVisibility(View.INVISIBLE);
                holder.progressCircle.setBackgroundResource(R.drawable.completed_progressbar);
                holder.dashPickupComplete.setTextColor(context.getResources().getColor(R.color.dot_complete));
                holder.dashPickupComplete.setVisibility(View.VISIBLE);
            } else if (list.get(position).getStatus().equals("1")) {

                holder.tickPickup.setVisibility(View.GONE);
                holder.progressCircle.setBackgroundResource(R.drawable.active_progressbar);
                holder.dashPickupComplete.setTextColor(context.getResources().getColor(R.color.dot_incomplete));
                holder.dashPickupComplete.setVisibility(View.VISIBLE);
            }
        } else if (pickndrop.equals("pickup")) {

            if (list.get(position).getSenderReceiver() != null && list.get(position).getSenderReceiver() != "null") {

                String sstr = "Sender: " + list.get(position).getSenderReceiver();
                holder.senderReceiver.setText(sstr);
            }else {
                holder.senderReceiver.setText("Sender: ");
            }

            holder.textViewPickup.setText("Pick-Up");
            holder.markComplete.setText("Mark as Complete");

            if (list.get(position).getStatus().equals("2")) {

                holder.markComplete.setVisibility(View.GONE);
                holder.etaBlue.setVisibility(View.GONE);
                holder.pickupTruck.setVisibility(View.INVISIBLE);
                holder.directionBtn.setVisibility(View.GONE);
                holder.progressCircle.setBackgroundResource(R.drawable.completed_progressbar);
                holder.dashPickupComplete.setTextColor(context.getResources().getColor(R.color.dot_complete));
                holder.dashPickupComplete.setVisibility(View.VISIBLE);

            } else if (list.get(position).getStatus().equals("1")) {

                holder.etaBlue.setText(Html.fromHtml(etaTimeStr));
                holder.tickPickup.setVisibility(View.GONE);
                holder.etaBlue.setBackgroundResource(R.drawable.eta_blue);
                holder.markComplete.setVisibility(View.VISIBLE);
                //              holder.directionBtn.setVisibility(View.VISIBLE);
                holder.progressCircle.setBackgroundResource(R.drawable.active_progressbar);

                if (new InternetGpsCheck(context).isGpsEnabled()) {
                    new CurrentLocation(context).lattLong();
                    new CurrentLocation(context).lattLong();
                } else {
                    Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
                }

                holder.dashPickupComplete.setTextColor(context.getResources().getColor(R.color.dot_incomplete));
                holder.dashPickupComplete.setVisibility(View.VISIBLE);
   //             calculateEtaTime(position, list.get(position).getLatt(), list.get(position).getLngg());

            } else if (list.get(position).getStatus().equals("0")) {

                holder.etaBlue.setText("ETA");
                holder.markComplete.setVisibility(View.GONE);
                holder.tickPickup.setVisibility(View.GONE);
                holder.etaBlue.setBackgroundResource(R.drawable.eta_gray);
                holder.pickupTruck.setVisibility(View.INVISIBLE);
                holder.directionBtn.setVisibility(View.GONE);
                holder.textViewPickup.setTextColor(Color.parseColor("#737373"));
                holder.time.setTextColor(Color.parseColor("#737373"));
                holder.date.setTextColor(Color.parseColor("#737373"));
                holder.dashPickupComplete.setTextColor(context.getResources().getColor(R.color.dot_incomplete));
                holder.dashPickupComplete.setVisibility(View.VISIBLE);
                holder.progressCircle.setBackgroundResource(R.drawable.not_active_progressbar);
            }

        } else if (pickndrop.equals("delivery")) {

            if (list.get(position).getSenderReceiver() != null && list.get(position).getSenderReceiver() != "null") {
                String sstr = "Receiver: " + list.get(position).getSenderReceiver();
                holder.senderReceiver.setText(sstr);
            }else {
                holder.senderReceiver.setText("Receiver: ");
            }

            holder.textViewPickup.setText("Delivery");
            holder.markComplete.setText("Capture POD");
            holder.tickPickup.setVisibility(View.GONE);

            if (list.get(position).getStatus().equals("2")) {

                String pickUpDropID = list.get(position).getIdPickUpDrop();

                holder.btnViewPodImg.setVisibility(View.VISIBLE);
                holder.tickPickup.setVisibility(View.VISIBLE);

                holder.etaBlue.setVisibility(View.GONE);
                holder.pickupTruck.setVisibility(View.INVISIBLE);
                holder.directionBtn.setVisibility(View.GONE);
                holder.markComplete.setVisibility(View.GONE);
                holder.progressCircle.setBackgroundResource(R.drawable.completed_progressbar);
                holder.dashPickupComplete.setTextColor(context.getResources().getColor(R.color.dot_complete));
                holder.dashPickupComplete.setVisibility(View.VISIBLE);

            } else if (list.get(position).getStatus().equals("1")) {

                holder.etaBlue.setText(Html.fromHtml(etaTimeStr));
                holder.etaBlue.setBackgroundResource(R.drawable.eta_blue);
                holder.tickPickup.setVisibility(View.GONE);
                holder.markComplete.setVisibility(View.VISIBLE);
                //               holder.directionBtn.setVisibility(View.VISIBLE);
                holder.progressCircle.setBackgroundResource(R.drawable.active_progressbar);

                if (new InternetGpsCheck(context).isGpsEnabled()) {
                    new CurrentLocation(context).lattLong();
                    new CurrentLocation(context).lattLong();
                } else {
                    Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
                }

                holder.dashPickupComplete.setTextColor(context.getResources().getColor(R.color.dot_incomplete));
                holder.dashPickupComplete.setVisibility(View.VISIBLE);
     //           calculateEtaTime(position, list.get(position).getLatt(), list.get(position).getLngg());

            } else if (list.get(position).getStatus().equals("0")) {

                holder.tickPickup.setVisibility(View.GONE);
                holder.etaBlue.setBackgroundResource(R.drawable.eta_gray);
                holder.etaBlue.setText("ETA");
                holder.pickupTruck.setVisibility(View.INVISIBLE);
                holder.directionBtn.setVisibility(View.GONE);
                holder.markComplete.setVisibility(View.GONE);

                holder.textViewPickup.setTextColor(Color.parseColor("#737373"));
                holder.time.setTextColor(Color.parseColor("#737373"));
                holder.date.setTextColor(Color.parseColor("#737373"));
                holder.dashPickupComplete.setTextColor(context.getResources().getColor(R.color.dot_incomplete));
                holder.dashPickupComplete.setVisibility(View.VISIBLE);
                holder.progressCircle.setBackgroundResource(R.drawable.not_active_progressbar);
            }
        }

        holder.markComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.markComplete.getText().equals("En-route")) {
                    ViewMoreActivity.dialog.show();
                    pos = position;
                    enRouteStatusChngd();
                } else if (holder.markComplete.getText().equals("Mark as Complete")) {
                    ViewMoreActivity.dialog.show();
                    pos = position;
                    markAsCompletePickUp(position);
                } else if (holder.markComplete.getText().equals("Capture POD")) {

                    ViewMoreActivity activity = (ViewMoreActivity) context;
                    activity.captureImg(position, list.get(position).getIdPickUpDrop(), list.size() - 1);
                }
            }
        });

        holder.btnViewPodImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (list.get(position).getPodImg() != null) {
                    editor.putString(GlobalClass.podImage, list.get(position).getPodImg());
                    editor.commit();
                    context.startActivity(new Intent(context, ViewPodActivity.class));
                } else {
                    Toast.makeText(context, "POD not available", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.directionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directionNavigate();
            }
        });

        if (list.get(0).getActualDateTime() != null) {
        } else {
            holder.etaBlue.setText("ETA");
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView dashPickupComplete, markComplete, textViewPickup, date, time, dateOn, timeOn, address;
        TextView qty, senderReceiver, assetCounter;
        TextView etaBlue;
        ImageView progressCircle;
        ImageView tickPickup, pickupTruck;
        Button capturePod, directionBtn;
        ImageButton btnViewPodImg;
        LinearLayout assetsLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            progressCircle = itemView.findViewById(R.id.enroute_circle);
            textViewPickup = itemView.findViewById(R.id.textview_pickup);
            dashPickupComplete = itemView.findViewById(R.id.dash_pickup_complete);
            address = (TextView) itemView.findViewById(R.id.address);
            qty = (TextView) itemView.findViewById(R.id.qty);
            senderReceiver = (TextView) itemView.findViewById(R.id.sender_receiver);
            assetCounter = (TextView) itemView.findViewById(R.id.asset_counter);

            assetsLayout = (LinearLayout) itemView.findViewById(R.id.assets_layout);

            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            dateOn = itemView.findViewById(R.id.date_on);
            timeOn = itemView.findViewById(R.id.time_on);

            markComplete = itemView.findViewById(R.id.mark_as_completed);
            capturePod = itemView.findViewById(R.id.capturePOD);
            tickPickup = itemView.findViewById(R.id.tick_pick);
            btnViewPodImg = (ImageButton) itemView.findViewById(R.id.view_pod_img_btn);

            etaBlue = itemView.findViewById(R.id.eta_blue);

            pickupTruck = itemView.findViewById(R.id.pickuptruck);
            directionBtn = (Button) itemView.findViewById(R.id.direction_btn);
        }
    }


    private void assetsList(List<Assets> assetsList) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_assets_list);
        dialog.setCancelable(false);
        dialog.show();
        WindowManager.LayoutParams lWindowParams;
        lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(dialog.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.FILL_PARENT; // this is where the magic happens
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lWindowParams);
        ImageButton close = (ImageButton) dialog.findViewById(R.id.close_asset_dialog);

        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.asset_recycler_vw);

        AssetsAdapter assetsAdapter = new AssetsAdapter(context, assetsList);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(assetsAdapter);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void directionNavigate() {
        if (new InternetGpsCheck(context).isGpsEnabled()) {
            new CurrentLocation(context).lattLong();
            new CurrentLocation(context).liveLocation();
        }
    }

    private void markAsCompletePickUp(final int position) {

        Calendar calendar = Calendar.getInstance();
        Date d = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        final String dateTimeActual = simpleDateFormat.format(d);

        /*SimpleDateFormat simpleDateFormat22 = new SimpleDateFormat("dd MMM yyyy hh:mm aa");
        final String dateTimeActual22 = simpleDateFormat22.format(d);*/

        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObject.put("pick_id", list.get(position).getIdPickUpDrop());
            jsonObject.put("actual_pickup_date_time", dateTimeActual);

            jsonObj.put("update_data", jsonObject);

            jsonObj.put("order_id", preferences.getString(GlobalClass.orderId, null));
            jsonObj.put("carr_id", preferences.getString(GlobalClass.carrierId, null));

            jsonObj.put("driver_name", preferences.getString(GlobalClass.nameUser, null));

            String jobNumber = preferences.getString(GlobalClass.jobNo, null);
            jsonObj.put("order_job", jobNumber);

            Log.d("response_json_obj", jsonObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiCallCls apiCallCls = new ApiCallCls() {
            @Override
            public Response.Listener<JSONObject> responseJsonReqSuccess() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (position == list.size() - 1) {
                            callFinalStatusChange(dateTimeActual);
                        } else {
                            activity.onStart();
                        }
                    }
                };
            }
        };
        String url = GlobalClass.actualPickUpTime/*+"?order_id="+preferences.getString(GlobalClass.orderId, null)+"&carr_id="+preferences.getString(GlobalClass.carrierId, null)*/;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));
        apiCallCls.jsonPostMethod(context, url, jsonObj, ViewMoreActivity.dialog, headers);
    }

    private void callFinalStatusChange(final String dateTimeActual) {

        String url = GlobalClass.lastDeliveryStatusChangeTime + "?en_route_or_pod=pod";

        final JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("order_id", preferences.getString(GlobalClass.orderId, null));
            jsonObj.put("carr_id", preferences.getString(GlobalClass.carrierId, null));
            jsonObj.put("en_route_time", dateTimeActual);
            jsonObj.put("final_delivery_date_time", dateTimeActual);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("response_last_delivery", response.toString());
                GlobalClass.boolDashBoard = false;

                ViewMoreActivity.statusDotColor.setImageResource(R.drawable.green_status_dot);
                ViewMoreActivity.statusOrder.setText("Completed");
                ViewMoreActivity.statusTruck.setImageResource(R.drawable.compelete);


  //              updateList(dateTimeActual);

                activity.onStart();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ViewMoreActivity.dialog.dismiss();
                volleyErrorResponse(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));

                return headers;
            }
        };

        queue.add(request);
    }

    private void enRouteStatusChngd() {

        JSONObject jsonObject = new JSONObject();

        Calendar calendar = Calendar.getInstance();
        Date d = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        final String dateTimeActual = simpleDateFormat.format(d);

        try {
            jsonObject.put("order_id", preferences.getString(GlobalClass.orderId, null));
            jsonObject.put("carr_id", preferences.getString(GlobalClass.carrierId, null));
            //                  jsonObject.put("en_route_time", preferences.getString(GlobalClass.orderId, null));

            Object oOO = NULL;
            jsonObject.put("final_delivery_date_time", oOO);

            jsonObject.put("en_route_time", dateTimeActual);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json_status_string", jsonObject.toString());

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));

        Log.d("token_token", preferences.getString(GlobalClass.toKen, null));

        String url = GlobalClass.lastDeliveryStatusChangeTime + "?en_route_or_pod=en_route";

        ViewMoreActivity.dialog.show();
        ApiCallCls apiCallCls = new ApiCallCls() {
            @Override
            public Response.Listener<JSONObject> responseJsonReqSuccess() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("json_status_string", response.toString());

                        ViewMoreActivity.statusDotColor.setImageResource(R.drawable.status_dot);
                        ViewMoreActivity.statusOrder.setText("In-progress");
                        ViewMoreActivity.statusTruck.setImageResource(R.drawable.inprogress);

                        activity.onStart();
                    }
                };
            }
        };

        apiCallCls.jsonPostMethod(context, url, jsonObject, ViewMoreActivity.dialog, headers);
    }


    public void volleyErrorResponse(VolleyError error) {

        Log.d("response_volley_err", error.toString());

        if (error.networkResponse != null) {
            Log.d("response_volley_err2", String.valueOf(error.networkResponse.statusCode));
        } else {
            Toast.makeText(context, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

// Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_volley_err3", obj.toString());
            } catch (UnsupportedEncodingException e1) {
// Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
// returned data is not JSONObject?
                e2.printStackTrace();
            }
        } else {
            Toast.makeText(context, "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }

        new GlobalClass().inValidToken(context, error);
    }






    private void calculateEtaTime(final int position, String latt, String lngg) {

        String url = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&waypoint0=geo!" + GlobalClass.latt + "," + GlobalClass.lngg + "&waypoint1=geo!" + latt + "," + lngg + "&mode=fastest;truck;traffic:disabled";

        Log.d("url_string_eta", "Hi\n"+position+"\n"+list.get(position).getPickUpDrop()+"\n"+url);

        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                EtaTime etaTime = new Gson().fromJson(response.toString(), EtaTime.class);

                long time = etaTime.getResponse().getRoute().get(0).getSummary().getTrafficTime();
                Log.d("url_string_eta", "Hi\n"+time);

                LatLngCls latLngCls = new LatLngCls();
                        /*latLngCls.setIdPickDrop(listEtaTime.get(sa).getIdPickDrop());
                        latLngCls.setPickUpDrop(listEtaTime.get(sa).getPickUpDrop());*/
                latLngCls.setEtaTime(time);
                listEtaTime.set(position-1, latLngCls);

                time = listEtaTime.get(position - 1).getEtaTime();

                int days = (int) time / (24 * 3600);
                long hours = time % (24 * 3600);
                int minuts = (int) (hours % 3600);
                int seconds = minuts % 60;

                int hhh = (int) (hours / 3600);
                int mmm = minuts / 60;
                String etaTimeStr = "" + days + "D " + hhh + "H " + mmm + "M";

                //            etaBlue.setText(etaTimeStr);

                /*    Log.d("fkjfdasfkdf", "" + ijk + "   " + time + "   " + response.toString());*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LatLngCls latLngCls = new LatLngCls();
                        /*latLngCls.setIdPickDrop(listEtaTime.get(sa).getIdPickDrop());
                        latLngCls.setPickUpDrop(listEtaTime.get(sa).getPickUpDrop());*/
                latLngCls.setEtaTime(0);
                listEtaTime.set(position, latLngCls);
                //            etaBlue.setText("ETA can't be calculated");
                Log.d("url_string_eta", error.toString() /*+ "  " + error.networkResponse.statusCode*/);
            }
        });

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }





    private void updateList(String dateTimeActual) {

        DataData itemData = new DataData();

        itemData.setDatetime(list.get(pos).getDatetime());
        itemData.setStatus("2");
        itemData.setActualDateTime(dateTimeActual);
        itemData.setPickUpDrop(list.get(pos).getPickUpDrop());
        itemData.setIdPickUpDrop(list.get(pos).getIdPickUpDrop());
        itemData.setAddress(list.get(pos).getAddress());
        itemData.setQty(list.get(pos).getQty());
        itemData.setSenderReceiver(list.get(pos).getSenderReceiver());
        itemData.setAssetsList(list.get(pos).getAssetsList());
        itemData.setEtaTime(list.get(pos).getEtaTime());
        if (list.get(pos).getPickUpDrop().equals("delivery")) {
            itemData.setPodImg(list.get(pos).getPodImg());
        }

        list.set(pos, itemData);

        if (pos < list.size() - 1) {

            itemData = new DataData();

            itemData.setDatetime(list.get(pos + 1).getDatetime());
            itemData.setStatus("1");
            itemData.setActualDateTime(list.get(pos + 1).getActualDateTime());
            itemData.setPickUpDrop(list.get(pos + 1).getPickUpDrop());
            itemData.setIdPickUpDrop(list.get(pos + 1).getIdPickUpDrop());
            itemData.setAddress(list.get(pos + 1).getAddress());
            itemData.setQty(list.get(pos + 1).getQty());
            itemData.setSenderReceiver(list.get(pos + 1).getSenderReceiver());
            itemData.setAssetsList(list.get(pos + 1).getAssetsList());
            itemData.setEtaTime(list.get(pos + 1).getEtaTime());
            if (list.get(pos + 1).getPickUpDrop().equals("delivery")) {
                itemData.setPodImg(list.get(pos + 1).getPodImg());
            }

            list.set(pos + 1, itemData);
        }

        notifyDataSetChanged();
    }
}

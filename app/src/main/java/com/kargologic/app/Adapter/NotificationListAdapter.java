package com.kargologic.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kargologic.app.R;
import com.kargologic.app.activities.ViewMoreActivity;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.drawable_vector_color_change.VectorChildFinder;
import com.kargologic.app.drawable_vector_color_change.VectorDrawableCompat;
import com.kargologic.app.pojoClasses.notification.Datum;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    private List<Datum> list;
    Context context;

    public NotificationListAdapter(Context context, List<Datum> list) {

        this.context = context;
        this.list = list;

        preferences = context.getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_data_notification, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


        VectorChildFinder vector = new VectorChildFinder(context, R.drawable.ic_bulleticon_svg, holder.imageViewLogoBack);

        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");

        path1.setFillColor(context.getResources().getColor(new GlobalClass().bulletBackColor[list.get(position).getColorLogo()]));

        holder.logoText.setTextColor(context.getResources().getColor(new GlobalClass().bulletFontColor[list.get(position).getColorLogo()]));

        holder.name.setText(list.get(position).getComments());

        String text = holder.name.getText().toString();

        String nameWithProperSpacing = text.replaceAll("\\s+", " ");

        String[] txtArray = nameWithProperSpacing.trim().split(" ");

        String logoTxt;
        if (txtArray.length > 2) {

            logoTxt = String.valueOf(txtArray[0].charAt(0)) + String.valueOf(txtArray[1].charAt(0));
        } else {
            logoTxt = String.valueOf(txtArray[0].charAt(0)) + String.valueOf(txtArray[0].charAt(1));
        }

        String first = logoTxt.toUpperCase();
        holder.logoText.setText(first);


        /*if (list.get(position).getDateTimeOfCreate() != null) {
            try {
                Date x = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(list.get(position).getDateTimeOfCreate());

                holder.date.setText(new SimpleDateFormat("dd-MM-yyyy").format(x));
                holder.time.setText(new SimpleDateFormat("hh:mm aa").format(x));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }*/


        holder.knowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editor.putString(GlobalClass.orderId, ""+list.get(position).getOrderID());
                editor.commit();
                context.startActivity(new Intent(context, ViewMoreActivity.class));
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView logoText, name, date, time;
        ImageView imageViewLogoBack;
        LinearLayout knowMore;

        public ViewHolder(View v) {
            super(v);

            logoText = v.findViewById(R.id.logo_textview);
            name = (TextView) v.findViewById(R.id.name_notify);
            date = (TextView) v.findViewById(R.id.date_notify);
            time = (TextView) v.findViewById(R.id.notify_time);
            imageViewLogoBack = (ImageView) v.findViewById(R.id.logo_text_view_back);

            knowMore = (LinearLayout) v.findViewById(R.id.know_more);



            date.setVisibility(View.GONE);
            time.setVisibility(View.GONE);
  //          knowMore.setVisibility(View.GONE);
        }
    }
}

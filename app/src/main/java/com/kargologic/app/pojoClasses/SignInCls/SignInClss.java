package com.kargologic.app.pojoClasses.SignInCls;

//
// Created by Sandy on 22-May-19 at 8:23 PM.
//


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignInClss {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("license_num")
    @Expose
    private String licenseNum;
    @SerializedName("categ_name")
    @Expose
    private String categName;
    @SerializedName("truck_reg_num")
    @Expose
    private String truckRegNum;
    @SerializedName("carr_id")
    @Expose
    private Integer carrId;
    @SerializedName("carr_com_id")
    @Expose
    private Integer carrComId;
    @SerializedName("message")
    @Expose
    private String message;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLicenseNum() {
        return licenseNum;
    }

    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    public String getCategName() {
        return categName;
    }

    public void setCategName(String categName) {
        this.categName = categName;
    }

    public String getTruckRegNum() {
        return truckRegNum;
    }

    public void setTruckRegNum(String truckRegNum) {
        this.truckRegNum = truckRegNum;
    }

    public Integer getCarrId() {
        return carrId;
    }

    public void setCarrId(Integer carrId) {
        this.carrId = carrId;
    }

    public Integer getCarrComId() {
        return carrComId;
    }

    public void setCarrComId(Integer carrComId) {
        this.carrComId = carrComId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
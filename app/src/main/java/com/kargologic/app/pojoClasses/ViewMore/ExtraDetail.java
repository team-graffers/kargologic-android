package com.kargologic.app.pojoClasses.ViewMore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 06/08/2019 at 12:11 PM.
 */
public class ExtraDetail {

    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("load_carried")
    @Expose
    private String loadCarried;
    @SerializedName("load_type")
    @Expose
    private String loadType;
    @SerializedName("load_weight")
    @Expose
    private String loadWeight;
    @SerializedName("length_load_dimen")
    @Expose
    private String lengthLoadDimen;
    @SerializedName("width_load_dimen")
    @Expose
    private String widthLoadDimen;
    @SerializedName("height_load_dimen")
    @Expose
    private String heightLoadDimen;
    @SerializedName("measure_unit")
    @Expose
    private String measureUnit;
    @SerializedName("units")
    @Expose
    private String units;
    @SerializedName("hazardous")
    @Expose
    private Boolean hazardous;
    @SerializedName("additional_note")
    @Expose
    private String additionalNote;
    @SerializedName("job_no")
    @Expose
    private String jobNo;
    @SerializedName("customer_company_id")
    @Expose
    private Integer customerCompanyId;
    @SerializedName("customer_company_name")
    @Expose
    private String customerCompanyName;
    @SerializedName("customer_company_number")
    @Expose
    private String customerCompanyNumber;
    @SerializedName("carrier_company_id")
    @Expose
    private Integer carrierCompanyId;
    @SerializedName("carrier_company_name")
    @Expose
    private String carrierCompanyName;
    @SerializedName("carrier_company_number")
    @Expose
    private String carrierCompanyNumber;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("vehicle_number")
    @Expose
    private String vehicleNumber;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getLoadCarried() {
        return loadCarried;
    }

    public void setLoadCarried(String loadCarried) {
        this.loadCarried = loadCarried;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getLoadWeight() {
        return loadWeight;
    }

    public void setLoadWeight(String loadWeight) {
        this.loadWeight = loadWeight;
    }

    public String getLengthLoadDimen() {
        return lengthLoadDimen;
    }

    public void setLengthLoadDimen(String lengthLoadDimen) {
        this.lengthLoadDimen = lengthLoadDimen;
    }

    public String getWidthLoadDimen() {
        return widthLoadDimen;
    }

    public void setWidthLoadDimen(String widthLoadDimen) {
        this.widthLoadDimen = widthLoadDimen;
    }

    public String getHeightLoadDimen() {
        return heightLoadDimen;
    }

    public void setHeightLoadDimen(String heightLoadDimen) {
        this.heightLoadDimen = heightLoadDimen;
    }

    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Boolean getHazardous() {
        return hazardous;
    }

    public void setHazardous(Boolean hazardous) {
        this.hazardous = hazardous;
    }

    public String getAdditionalNote() {
        return additionalNote;
    }

    public void setAdditionalNote(String additionalNote) {
        this.additionalNote = additionalNote;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public Integer getCustomerCompanyId() {
        return customerCompanyId;
    }

    public void setCustomerCompanyId(Integer customerCompanyId) {
        this.customerCompanyId = customerCompanyId;
    }

    public String getCustomerCompanyName() {
        return customerCompanyName;
    }

    public void setCustomerCompanyName(String customerCompanyName) {
        this.customerCompanyName = customerCompanyName;
    }

    public String getCustomerCompanyNumber() {
        return customerCompanyNumber;
    }

    public void setCustomerCompanyNumber(String customerCompanyNumber) {
        this.customerCompanyNumber = customerCompanyNumber;
    }

    public Integer getCarrierCompanyId() {
        return carrierCompanyId;
    }

    public void setCarrierCompanyId(Integer carrierCompanyId) {
        this.carrierCompanyId = carrierCompanyId;
    }

    public String getCarrierCompanyName() {
        return carrierCompanyName;
    }

    public void setCarrierCompanyName(String carrierCompanyName) {
        this.carrierCompanyName = carrierCompanyName;
    }

    public String getCarrierCompanyNumber() {
        return carrierCompanyNumber;
    }

    public void setCarrierCompanyNumber(String carrierCompanyNumber) {
        this.carrierCompanyNumber = carrierCompanyNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }
}

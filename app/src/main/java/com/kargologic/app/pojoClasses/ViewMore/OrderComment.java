package com.kargologic.app.pojoClasses.ViewMore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderComment {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("Created_by_CustomerID")
    @Expose
    private CreatedByCustomerID createdByCustomerID;
    @SerializedName("Created_by_CarrierID")
    @Expose
    private CreatedByCarrierID createdByCarrierID;
    @SerializedName("Created_by_DriverID")
    @Expose
    private CreatedByDriverID createdByDriverID;
    @SerializedName("created_DT")
    @Expose
    private String createdDT;
    @SerializedName("updated_DT")
    @Expose
    private String updatedDT;
    @SerializedName("date_time_of_create")
    @Expose
    private String date_timeOfCreate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public CreatedByCustomerID getCreatedByCustomerID() {
        return createdByCustomerID;
    }

    public void setCreatedByCustomerID(CreatedByCustomerID createdByCustomerID) {
        this.createdByCustomerID = createdByCustomerID;
    }

    public CreatedByCarrierID getCreatedByCarrierID() {
        return createdByCarrierID;
    }

    public void setCreatedByCarrierID(CreatedByCarrierID createdByCarrierID) {
        this.createdByCarrierID = createdByCarrierID;
    }

    public CreatedByDriverID getCreatedByDriverID() {
        return createdByDriverID;
    }

    public void setCreatedByDriverID(CreatedByDriverID createdByDriverID) {
        this.createdByDriverID = createdByDriverID;
    }

    public String getCreatedDT() {
        return createdDT;
    }

    public void setCreatedDT(String createdDT) {
        this.createdDT = createdDT;
    }

    public String getUpdatedDT() {
        return updatedDT;
    }

    public void setUpdatedDT(String updatedDT) {
        this.updatedDT = updatedDT;
    }

    public String getDate_timeOfCreate() {
        return date_timeOfCreate;
    }

    public void setDate_timeOfCreate(String date_timeOfCreate) {
        this.date_timeOfCreate = date_timeOfCreate;
    }
}

package com.kargologic.app.pojoClasses.ViewMore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 06/08/2019 at 12:11 PM.
 */
public class Main {

    @SerializedName("extra_detail")
    @Expose
    private ExtraDetail extraDetail;
    @SerializedName("pickup")
    @Expose
    private List<Pickup> pickup = null;
    @SerializedName("delivery")
    @Expose
    private List<Delivery> delivery = null;
    @SerializedName("order_enroute")
    @Expose
    private Object orderEnroute;
    @SerializedName("order_final_deliver")
    @Expose
    private Object orderFinalDeliver;

    public ExtraDetail getExtraDetail() {
        return extraDetail;
    }

    public void setExtraDetail(ExtraDetail extraDetail) {
        this.extraDetail = extraDetail;
    }

    public List<Pickup> getPickup() {
        return pickup;
    }

    public void setPickup(List<Pickup> pickup) {
        this.pickup = pickup;
    }

    public List<Delivery> getDelivery() {
        return delivery;
    }

    public void setDelivery(List<Delivery> delivery) {
        this.delivery = delivery;
    }

    public Object getOrderEnroute() {
        return orderEnroute;
    }

    public void setOrderEnroute(Object orderEnroute) {
        this.orderEnroute = orderEnroute;
    }

    public Object getOrderFinalDeliver() {
        return orderFinalDeliver;
    }

    public void setOrderFinalDeliver(Object orderFinalDeliver) {
        this.orderFinalDeliver = orderFinalDeliver;
    }

}

package com.kargologic.app.pojoClasses.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum {

    @SerializedName("extra_detail")
    @Expose
    private ExtraDetail extraDetail;
    @SerializedName("pickup")
    @Expose
    private List<Pickup> pickup = null;
    @SerializedName("delivery")
    @Expose
    private List<Delivery> delivery = null;

    public ExtraDetail getExtraDetail() {
        return extraDetail;
    }

    public void setExtraDetail(ExtraDetail extraDetail) {
        this.extraDetail = extraDetail;
    }

    public List<Pickup> getPickup() {
        return pickup;
    }

    public void setPickup(List<Pickup> pickup) {
        this.pickup = pickup;
    }

    public List<Delivery> getDelivery() {
        return delivery;
    }

    public void setDelivery(List<Delivery> delivery) {
        this.delivery = delivery;
    }

}

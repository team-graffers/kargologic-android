package com.kargologic.app.pojoClasses.ViewMore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 06/08/2019 at 12:11 PM.
 */
public class AssetList_ {

    @SerializedName("asset_id")
    @Expose
    private String assetId;
    @SerializedName("asset_type")
    @Expose
    private String assetType;
    @SerializedName("registeration_no")
    @Expose
    private String registerationNo;

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getRegisterationNo() {
        return registerationNo;
    }

    public void setRegisterationNo(String registerationNo) {
        this.registerationNo = registerationNo;
    }
}

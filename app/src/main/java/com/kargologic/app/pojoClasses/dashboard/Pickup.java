package com.kargologic.app.pojoClasses.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pickup {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pickup_date_time")
    @Expose
    private String pickupDateTime;
    @SerializedName("pickup_date_time_to")
    @Expose
    private String pickupDateTimeTo;
    @SerializedName("actual_pickup_date_time")
    @Expose
    private Object actualPickupDateTime;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("assigned_quantity")
    @Expose
    private String assignedQuantity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPickupDateTime() {
        return pickupDateTime;
    }

    public void setPickupDateTime(String pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }

    public String getPickupDateTimeTo() {
        return pickupDateTimeTo;
    }

    public void setPickupDateTimeTo(String pickupDateTimeTo) {
        this.pickupDateTimeTo = pickupDateTimeTo;
    }

    public Object getActualPickupDateTime() {
        return actualPickupDateTime;
    }

    public void setActualPickupDateTime(Object actualPickupDateTime) {
        this.actualPickupDateTime = actualPickupDateTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAssignedQuantity() {
        return assignedQuantity;
    }

    public void setAssignedQuantity(String assignedQuantity) {
        this.assignedQuantity = assignedQuantity;
    }
}

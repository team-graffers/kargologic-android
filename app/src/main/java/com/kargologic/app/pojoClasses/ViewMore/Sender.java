package com.kargologic.app.pojoClasses.ViewMore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 06/08/2019 at 12:12 PM.
 */
public class Sender {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("primary_contact")
    @Expose
    private String primaryContact;
    @SerializedName("pri_email_id")
    @Expose
    private String priEmailId;
    @SerializedName("pri_phone")
    @Expose
    private String priPhone;
    @SerializedName("secondary_contact")
    @Expose
    private String secondaryContact;
    @SerializedName("sec_email_id")
    @Expose
    private String secEmailId;
    @SerializedName("sec_phone")
    @Expose
    private String secPhone;
    @SerializedName("business_address")
    @Expose
    private String businessAddress;
    @SerializedName("contract_start_date")
    @Expose
    private String contractStartDate;
    @SerializedName("contract_end_date")
    @Expose
    private String contractEndDate;
    @SerializedName("contract_documents")
    @Expose
    private Object contractDocuments;
    @SerializedName("Customer_code")
    @Expose
    private Object customerCode;
    @SerializedName("is_sender")
    @Expose
    private Boolean isSender;
    @SerializedName("created_DT")
    @Expose
    private String createdDT;
    @SerializedName("updated_DT")
    @Expose
    private String updatedDT;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("is_delete")
    @Expose
    private Boolean isDelete;
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
    }

    public String getPriEmailId() {
        return priEmailId;
    }

    public void setPriEmailId(String priEmailId) {
        this.priEmailId = priEmailId;
    }

    public String getPriPhone() {
        return priPhone;
    }

    public void setPriPhone(String priPhone) {
        this.priPhone = priPhone;
    }

    public String getSecondaryContact() {
        return secondaryContact;
    }

    public void setSecondaryContact(String secondaryContact) {
        this.secondaryContact = secondaryContact;
    }

    public String getSecEmailId() {
        return secEmailId;
    }

    public void setSecEmailId(String secEmailId) {
        this.secEmailId = secEmailId;
    }

    public String getSecPhone() {
        return secPhone;
    }

    public void setSecPhone(String secPhone) {
        this.secPhone = secPhone;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(String contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public String getContractEndDate() {
        return contractEndDate;
    }

    public void setContractEndDate(String contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public Object getContractDocuments() {
        return contractDocuments;
    }

    public void setContractDocuments(Object contractDocuments) {
        this.contractDocuments = contractDocuments;
    }

    public Object getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(Object customerCode) {
        this.customerCode = customerCode;
    }

    public Boolean getIsSender() {
        return isSender;
    }

    public void setIsSender(Boolean isSender) {
        this.isSender = isSender;
    }

    public String getCreatedDT() {
        return createdDT;
    }

    public void setCreatedDT(String createdDT) {
        this.createdDT = createdDT;
    }

    public String getUpdatedDT() {
        return updatedDT;
    }

    public void setUpdatedDT(String updatedDT) {
        this.updatedDT = updatedDT;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

}

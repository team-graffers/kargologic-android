package com.kargologic.app.pojoClasses.eta;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 05/07/2019 at 02:21 PM.
 */
public class Mode {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("transportModes")
    @Expose
    private List<String> transportModes = null;
    @SerializedName("trafficMode")
    @Expose
    private String trafficMode;
    @SerializedName("feature")
    @Expose
    private List<Object> feature = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getTransportModes() {
        return transportModes;
    }

    public void setTransportModes(List<String> transportModes) {
        this.transportModes = transportModes;
    }

    public String getTrafficMode() {
        return trafficMode;
    }

    public void setTrafficMode(String trafficMode) {
        this.trafficMode = trafficMode;
    }

    public List<Object> getFeature() {
        return feature;
    }

    public void setFeature(List<Object> feature) {
        this.feature = feature;
    }

}

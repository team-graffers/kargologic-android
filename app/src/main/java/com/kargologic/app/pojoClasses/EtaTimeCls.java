package com.kargologic.app.pojoClasses;

/**
 * Created by Sandy on 08/07/2019 at 03:20 PM.
 */
public class EtaTimeCls {

    String id, id2, idPickDrop, idPickDrop2;
    long timeEta;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId2() {
        return id2;
    }

    public void setId2(String id2) {
        this.id2 = id2;
    }

    public String getIdPickDrop() {
        return idPickDrop;
    }

    public void setIdPickDrop(String idPickDrop) {
        this.idPickDrop = idPickDrop;
    }

    public String getIdPickDrop2() {
        return idPickDrop2;
    }

    public void setIdPickDrop2(String idPickDrop2) {
        this.idPickDrop2 = idPickDrop2;
    }

    public long getTimeEta() {
        return timeEta;
    }

    public void setTimeEta(long timeEta) {
        this.timeEta = timeEta;
    }
}

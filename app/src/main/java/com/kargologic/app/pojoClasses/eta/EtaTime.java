package com.kargologic.app.pojoClasses.eta;

/**
 * Created by Sandy on 05/07/2019 at 02:20 PM.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EtaTime {

    @SerializedName("response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}
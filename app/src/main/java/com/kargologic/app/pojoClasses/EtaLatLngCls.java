package com.kargologic.app.pojoClasses;

/**
 * Created by Sandy on 06/08/2019 at 11:48 AM.
 */
public class EtaLatLngCls {

    double lat, lng, lat2, lng2;
    String id, id2, idPickDrop, idPickDrop2;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat2() {
        return lat2;
    }

    public void setLat2(double lat2) {
        this.lat2 = lat2;
    }

    public double getLng2() {
        return lng2;
    }

    public void setLng2(double lng2) {
        this.lng2 = lng2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId2() {
        return id2;
    }

    public void setId2(String id2) {
        this.id2 = id2;
    }

    public String getIdPickDrop() {
        return idPickDrop;
    }

    public void setIdPickDrop(String idPickDrop) {
        this.idPickDrop = idPickDrop;
    }

    public String getIdPickDrop2() {
        return idPickDrop2;
    }

    public void setIdPickDrop2(String idPickDrop2) {
        this.idPickDrop2 = idPickDrop2;
    }
}

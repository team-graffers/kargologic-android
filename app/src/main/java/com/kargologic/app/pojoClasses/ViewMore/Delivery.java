package com.kargologic.app.pojoClasses.ViewMore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 06/08/2019 at 12:11 PM.
 */
public class Delivery {

    @SerializedName("data_of")
    @Expose
    private String dataOf;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("asset_list")
    @Expose
    private List<AssetList_> assetList = null;
    @SerializedName("delivery_date_time")
    @Expose
    private String deliveryDateTime;
    @SerializedName("delivery_date_time_to")
    @Expose
    private String deliveryDateTimeTo;
    @SerializedName("actual_delivery_date_time")
    @Expose
    private Object actualDeliveryDateTime;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("assigned_quantity")
    @Expose
    private String assignedQuantity;
    @SerializedName("pod_image")
    @Expose
    private Object podImage;
    @SerializedName("receiver")
    @Expose
    private Receiver receiver;

    public String getDataOf() {
        return dataOf;
    }

    public void setDataOf(String dataOf) {
        this.dataOf = dataOf;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<AssetList_> getAssetList() {
        return assetList;
    }

    public void setAssetList(List<AssetList_> assetList) {
        this.assetList = assetList;
    }

    public String getDeliveryDateTime() {
        return deliveryDateTime;
    }

    public void setDeliveryDateTime(String deliveryDateTime) {
        this.deliveryDateTime = deliveryDateTime;
    }

    public String getDeliveryDateTimeTo() {
        return deliveryDateTimeTo;
    }

    public void setDeliveryDateTimeTo(String deliveryDateTimeTo) {
        this.deliveryDateTimeTo = deliveryDateTimeTo;
    }

    public Object getActualDeliveryDateTime() {
        return actualDeliveryDateTime;
    }

    public void setActualDeliveryDateTime(Object actualDeliveryDateTime) {
        this.actualDeliveryDateTime = actualDeliveryDateTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAssignedQuantity() {
        return assignedQuantity;
    }

    public void setAssignedQuantity(String assignedQuantity) {
        this.assignedQuantity = assignedQuantity;
    }

    public Object getPodImage() {
        return podImage;
    }

    public void setPodImage(Object podImage) {
        this.podImage = podImage;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

}

package com.kargologic.app.pojoClasses.ViewMore;

/**
 * Created by Sandy on 06/08/2019 at 12:52 PM.
 */
public class Assets {

    private String assetId;
    private String assetType;
    private String registerationNo;

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getRegisterationNo() {
        return registerationNo;
    }

    public void setRegisterationNo(String registerationNo) {
        this.registerationNo = registerationNo;
    }
}

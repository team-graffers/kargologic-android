package com.kargologic.app.pojoClasses.ViewMore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 06/08/2019 at 12:12 PM.
 */
public class Receiver {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("primary_contact")
    @Expose
    private Object primaryContact;
    @SerializedName("pri_email_id")
    @Expose
    private Object priEmailId;
    @SerializedName("pri_phone")
    @Expose
    private Object priPhone;
    @SerializedName("secondary_contact")
    @Expose
    private Object secondaryContact;
    @SerializedName("sec_email_id")
    @Expose
    private Object secEmailId;
    @SerializedName("sec_phone")
    @Expose
    private Object secPhone;
    @SerializedName("business_address")
    @Expose
    private Object businessAddress;
    @SerializedName("contract_start_date")
    @Expose
    private Object contractStartDate;
    @SerializedName("contract_end_date")
    @Expose
    private Object contractEndDate;
    @SerializedName("contract_documents")
    @Expose
    private Object contractDocuments;
    @SerializedName("Customer_code")
    @Expose
    private Object customerCode;
    @SerializedName("is_sender")
    @Expose
    private Boolean isSender;
    @SerializedName("created_DT")
    @Expose
    private String createdDT;
    @SerializedName("updated_DT")
    @Expose
    private String updatedDT;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("is_delete")
    @Expose
    private Boolean isDelete;
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(Object primaryContact) {
        this.primaryContact = primaryContact;
    }

    public Object getPriEmailId() {
        return priEmailId;
    }

    public void setPriEmailId(Object priEmailId) {
        this.priEmailId = priEmailId;
    }

    public Object getPriPhone() {
        return priPhone;
    }

    public void setPriPhone(Object priPhone) {
        this.priPhone = priPhone;
    }

    public Object getSecondaryContact() {
        return secondaryContact;
    }

    public void setSecondaryContact(Object secondaryContact) {
        this.secondaryContact = secondaryContact;
    }

    public Object getSecEmailId() {
        return secEmailId;
    }

    public void setSecEmailId(Object secEmailId) {
        this.secEmailId = secEmailId;
    }

    public Object getSecPhone() {
        return secPhone;
    }

    public void setSecPhone(Object secPhone) {
        this.secPhone = secPhone;
    }

    public Object getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(Object businessAddress) {
        this.businessAddress = businessAddress;
    }

    public Object getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Object contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public Object getContractEndDate() {
        return contractEndDate;
    }

    public void setContractEndDate(Object contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public Object getContractDocuments() {
        return contractDocuments;
    }

    public void setContractDocuments(Object contractDocuments) {
        this.contractDocuments = contractDocuments;
    }

    public Object getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(Object customerCode) {
        this.customerCode = customerCode;
    }

    public Boolean getIsSender() {
        return isSender;
    }

    public void setIsSender(Boolean isSender) {
        this.isSender = isSender;
    }

    public String getCreatedDT() {
        return createdDT;
    }

    public void setCreatedDT(String createdDT) {
        this.createdDT = createdDT;
    }

    public String getUpdatedDT() {
        return updatedDT;
    }

    public void setUpdatedDT(String updatedDT) {
        this.updatedDT = updatedDT;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

}

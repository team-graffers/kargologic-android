package com.kargologic.app.pojoClasses.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExtraDetail {

    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("job_no")
    @Expose
    private String jobNo;
    @SerializedName("customer_company_id")
    @Expose
    private Integer customerCompanyId;
    @SerializedName("customer_company_name")
    @Expose
    private String customerCompanyName;
    @SerializedName("customer_company_number")
    @Expose
    private String customerCompanyNumber;
    @SerializedName("carrier_company_id")
    @Expose
    private Integer carrierCompanyId;
    @SerializedName("carrier_company_name")
    @Expose
    private String carrierCompanyName;
    @SerializedName("carrier_company_number")
    @Expose
    private String carrierCompanyNumber;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public Integer getCustomerCompanyId() {
        return customerCompanyId;
    }

    public void setCustomerCompanyId(Integer customerCompanyId) {
        this.customerCompanyId = customerCompanyId;
    }

    public String getCustomerCompanyName() {
        return customerCompanyName;
    }

    public void setCustomerCompanyName(String customerCompanyName) {
        this.customerCompanyName = customerCompanyName;
    }

    public String getCustomerCompanyNumber() {
        return customerCompanyNumber;
    }

    public void setCustomerCompanyNumber(String customerCompanyNumber) {
        this.customerCompanyNumber = customerCompanyNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCarrierCompanyId() {
        return carrierCompanyId;
    }

    public void setCarrierCompanyId(Integer carrierCompanyId) {
        this.carrierCompanyId = carrierCompanyId;
    }

    public String getCarrierCompanyName() {
        return carrierCompanyName;
    }

    public void setCarrierCompanyName(String carrierCompanyName) {
        this.carrierCompanyName = carrierCompanyName;
    }

    public String getCarrierCompanyNumber() {
        return carrierCompanyNumber;
    }

    public void setCarrierCompanyNumber(String carrierCompanyNumber) {
        this.carrierCompanyNumber = carrierCompanyNumber;
    }
}

package com.kargologic.app.pojoClasses.ViewMore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ViewMoreClass {

    @SerializedName("main")
    @Expose
    private List<Main> main = null;
    @SerializedName("order_comment")
    @Expose
    private List<OrderComment> orderComment = null;
    @SerializedName("attachment")
    @Expose
    private List<Attachment> attachment = null;

    public List<Main> getMain() {
        return main;
    }

    public void setMain(List<Main> main) {
        this.main = main;
    }

    public List<OrderComment> getOrderComment() {
        return orderComment;
    }

    public void setOrderComment(List<OrderComment> orderComment) {
        this.orderComment = orderComment;
    }

    public List<Attachment> getAttachment() {
        return attachment;
    }

    public void setAttachment(List<Attachment> attachment) {
        this.attachment = attachment;
    }
}

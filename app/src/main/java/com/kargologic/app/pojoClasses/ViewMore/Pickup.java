package com.kargologic.app.pojoClasses.ViewMore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 06/08/2019 at 12:11 PM.
 */
public class Pickup {

    @SerializedName("data_of")
    @Expose
    private String dataOf;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("asset_list")
    @Expose
    private List<AssetList> assetList = null;
    @SerializedName("pickup_date_time")
    @Expose
    private String pickupDateTime;
    @SerializedName("pickup_date_time_to")
    @Expose
    private String pickupDateTimeTo;
    @SerializedName("actual_pickup_date_time")
    @Expose
    private Object actualPickupDateTime;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("assigned_quantity")
    @Expose
    private String assignedQuantity;
    @SerializedName("sender")
    @Expose
    private Sender sender;

    public String getDataOf() {
        return dataOf;
    }

    public void setDataOf(String dataOf) {
        this.dataOf = dataOf;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<AssetList> getAssetList() {
        return assetList;
    }

    public void setAssetList(List<AssetList> assetList) {
        this.assetList = assetList;
    }

    public String getPickupDateTime() {
        return pickupDateTime;
    }

    public void setPickupDateTime(String pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }

    public String getPickupDateTimeTo() {
        return pickupDateTimeTo;
    }

    public void setPickupDateTimeTo(String pickupDateTimeTo) {
        this.pickupDateTimeTo = pickupDateTimeTo;
    }

    public Object getActualPickupDateTime() {
        return actualPickupDateTime;
    }

    public void setActualPickupDateTime(Object actualPickupDateTime) {
        this.actualPickupDateTime = actualPickupDateTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAssignedQuantity() {
        return assignedQuantity;
    }

    public void setAssignedQuantity(String assignedQuantity) {
        this.assignedQuantity = assignedQuantity;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

}

package com.kargologic.app.pojoClasses.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Delivery {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("delivery_date_time")
    @Expose
    private String deliveryDateTime;
    @SerializedName("delivery_date_time_to")
    @Expose
    private String deliveryDateTimeTo;
    @SerializedName("actual_delivery_date_time")
    @Expose
    private Object actualDeliveryDateTime;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("assigned_quantity")
    @Expose
    private String assignedQuantity;
    @SerializedName("pod_image")
    @Expose
    private Object podImage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeliveryDateTime() {
        return deliveryDateTime;
    }

    public void setDeliveryDateTime(String deliveryDateTime) {
        this.deliveryDateTime = deliveryDateTime;
    }

    public String getDeliveryDateTimeTo() {
        return deliveryDateTimeTo;
    }

    public void setDeliveryDateTimeTo(String deliveryDateTimeTo) {
        this.deliveryDateTimeTo = deliveryDateTimeTo;
    }

    public Object getActualDeliveryDateTime() {
        return actualDeliveryDateTime;
    }

    public void setActualDeliveryDateTime(Object actualDeliveryDateTime) {
        this.actualDeliveryDateTime = actualDeliveryDateTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAssignedQuantity() {
        return assignedQuantity;
    }

    public void setAssignedQuantity(String assignedQuantity) {
        this.assignedQuantity = assignedQuantity;
    }

    public Object getPodImage() {
        return podImage;
    }

    public void setPodImage(Object podImage) {
        this.podImage = podImage;
    }
}

package com.kargologic.app.pojoClasses.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("is_read")
    @Expose
    private Boolean isRead;
    @SerializedName("date_time_of_create")
    @Expose
    private String dateTimeOfCreate;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("OrderID")
    @Expose
    private Integer orderID;
    @SerializedName("created_by_carrier")
    @Expose
    private Object createdByCarrier;
    @SerializedName("created_by_customer")
    @Expose
    private Object createdByCustomer;
    @SerializedName("created_by_driver")
    @Expose
    private Object createdByDriver;
    @SerializedName("created_for_carrier")
    @Expose
    private Object createdForCarrier;
    @SerializedName("created_for_customer")
    @Expose
    private Object createdForCustomer;
    @SerializedName("created_for_driver")
    @Expose
    private Integer createdForDriver;
    @SerializedName("color_logo")
    @Expose
    int colorLogo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public String getDateTimeOfCreate() {
        return dateTimeOfCreate;
    }

    public void setDateTimeOfCreate(String dateTimeOfCreate) {
        this.dateTimeOfCreate = dateTimeOfCreate;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public Object getCreatedByCarrier() {
        return createdByCarrier;
    }

    public void setCreatedByCarrier(Object createdByCarrier) {
        this.createdByCarrier = createdByCarrier;
    }

    public Object getCreatedByCustomer() {
        return createdByCustomer;
    }

    public void setCreatedByCustomer(Object createdByCustomer) {
        this.createdByCustomer = createdByCustomer;
    }

    public Object getCreatedByDriver() {
        return createdByDriver;
    }

    public void setCreatedByDriver(Object createdByDriver) {
        this.createdByDriver = createdByDriver;
    }

    public Object getCreatedForCarrier() {
        return createdForCarrier;
    }

    public void setCreatedForCarrier(Object createdForCarrier) {
        this.createdForCarrier = createdForCarrier;
    }

    public Object getCreatedForCustomer() {
        return createdForCustomer;
    }

    public void setCreatedForCustomer(Object createdForCustomer) {
        this.createdForCustomer = createdForCustomer;
    }

    public Integer getCreatedForDriver() {
        return createdForDriver;
    }

    public void setCreatedForDriver(Integer createdForDriver) {
        this.createdForDriver = createdForDriver;
    }

    public int getColorLogo() {
        return colorLogo;
    }

    public void setColorLogo(int colorLogo) {
        this.colorLogo = colorLogo;
    }
}

package com.kargologic.app.pojoClasses.ViewMore;

import java.util.ArrayList;
import java.util.List;

public class DataData {

    String datetime, status, actualDateTime, pickUpDrop, idPickUpDrop, address, qty, podImg, latt, lngg, orderId, senderReceiver;
    long etaTime;
    List<Assets> assetsList = new ArrayList<>();

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActualDateTime() {
        return actualDateTime;
    }

    public void setActualDateTime(String actualDateTime) {
        this.actualDateTime = actualDateTime;
    }

    public String getPickUpDrop() {
        return pickUpDrop;
    }

    public void setPickUpDrop(String pickUpDrop) {
        this.pickUpDrop = pickUpDrop;
    }

    public String getIdPickUpDrop() {
        return idPickUpDrop;
    }

    public void setIdPickUpDrop(String idPickUpDrop) {
        this.idPickUpDrop = idPickUpDrop;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPodImg() {
        return podImg;
    }

    public void setPodImg(String podImg) {
        this.podImg = podImg;
    }

    public String getLatt() {
        return latt;
    }

    public void setLatt(String latt) {
        this.latt = latt;
    }

    public String getLngg() {
        return lngg;
    }

    public void setLngg(String lngg) {
        this.lngg = lngg;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSenderReceiver() {
        return senderReceiver;
    }

    public void setSenderReceiver(String senderReceiver) {
        this.senderReceiver = senderReceiver;
    }

    public List<Assets> getAssetsList() {
        return assetsList;
    }

    public void setAssetsList(List<Assets> assetsList) {
        this.assetsList = assetsList;
    }

    public long getEtaTime() {
        return etaTime;
    }

    public void setEtaTime(long etaTime) {
        this.etaTime = etaTime;
    }
}

package com.kargologic.app.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kargologic.app.Adapter.InProgressListAdapter;
import com.kargologic.app.classes.CurrentLocation;
import com.kargologic.app.classes.DataListInProgressFrag;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.classes.InternetGpsCheck;
import com.kargologic.app.classes.LatLngCls;
import com.kargologic.app.pojoClasses.EtaLatLngCls;
import com.kargologic.app.pojoClasses.EtaTimeCls;
import com.kargologic.app.pojoClasses.ViewMore.DataData;
import com.kargologic.app.pojoClasses.dashboard.DashboardClass;
import com.kargologic.app.pojoClasses.dashboard.Delivery;
import com.kargologic.app.pojoClasses.dashboard.Pickup;
import com.kargologic.app.pojoClasses.eta.EtaTime;
import com.kargologic.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;

public class InprogressFragment extends Fragment {

    RecyclerView rv;
    List<DataListInProgressFrag> list;
    private Dialog dialog;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    View view;
    SwipeRefreshLayout refreshLayout;
    InProgressListAdapter adapter;
    private Context context;
    LinearLayout ordersNotAvailable;
    TextView ordersNotAvailableTxt;
    private Handler mHandler;
    private DashboardClass dashboardClass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_inprogress, container, false);

        preferences = getActivity().getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        context = getActivity();
        ordersNotAvailable = (LinearLayout) view.findViewById(R.id.no_order_in_list);
        ordersNotAvailableTxt = (TextView) view.findViewById(R.id.no_order_in_list_txt);

        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_progress_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        return view;
    }

    LocationManager locationManager;

    @Override
    public void onResume() {
        super.onResume();

       /* GlobalClass.latt = 0.0;
        GlobalClass.lngg = 0.0;*/

       iii = 0;

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout_swipe);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onResume();
            }
        });

        if (new InternetGpsCheck(context).isGpsEnabled()) {
            new CurrentLocation(context).lattLong();
            new CurrentLocation(context).liveLocation();
        } else {
            Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }
        if (new InternetGpsCheck(context).isNetworkConnected()) {
            dashboardMethod();
        } else {
            Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void dashboardMethod() {

        GlobalClass.listEtaRoute = new ArrayList<>();
        GlobalClass.listEtaNext = new ArrayList<>();
        listHashMap = new HashMap<>();
        listKey = new ArrayList<>();

        if (!refreshLayout.isRefreshing()) {
            dialog.show();
        }

        final HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));

        String url = GlobalClass.base_Url + "driv/driver_dashboard/?status=in process";

        rv = (RecyclerView) view.findViewById(R.id.recyclerview);

        RequestQueue queue = Volley.newRequestQueue(getContext());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                list = new ArrayList<>();
                list.clear();
                adapter = new InProgressListAdapter(getActivity(), list, GlobalClass.listEtaRoute, GlobalClass.listEtaNext);
                rv.setHasFixedSize(true);
                rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                rv.setAdapter(adapter);
                rv.setItemAnimator(new DefaultItemAnimator());

                if (new InternetGpsCheck(context).isGpsEnabled()) {
                    new CurrentLocation(context).lattLong();
                    new CurrentLocation(context).liveLocation();
                } else {
                    Toast.makeText(context, "Please Enable Your GPS Location", Toast.LENGTH_SHORT).show();
                }

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (refreshLayout.isRefreshing()) {
                    refreshLayout.setRefreshing(false);
                }

                Log.d("response_scs_inprogrss", response.toString());

                dashboardClass = new Gson().fromJson(response.toString(), DashboardClass.class);

                if (dashboardClass.getData() != null && dashboardClass.getData().size() > 0) {

                    rv.setVisibility(View.VISIBLE);
                    ordersNotAvailable.setVisibility(View.GONE);

                    timeEta = new ArrayList<>();
                    timeEtaNextList = new ArrayList<>();
                    datalistSet = new ArrayList<>();
                    listFinalLatLng = new ArrayList<>();

                    int l = dashboardClass.getData().size();

                    for (int pos = 0; pos < l; pos++) {

                        datalist = new ArrayList<>();
                        setPickUpData(dashboardClass.getData().get(pos).getPickup(), dashboardClass.getData().get(pos).getExtraDetail().getOrderId());
                        setDeliveryData(dashboardClass.getData().get(pos).getDelivery(), dashboardClass.getData().get(pos).getExtraDetail().getOrderId());
                        sortDataList(datalist);
                        setDataInAdapter(dashboardClass, pos);
                        setLatLongInList();

                        if (listHashLatLng.size()>0) {
                            prepareForCalculateEta(dashboardClass.getData().get(pos).getExtraDetail().getJobNo());
                        }
                    }

                    mHandler = new Handler();
                    mHandler.postDelayed(m_Runnable, 60000);

                } else {
                    rv.setVisibility(View.GONE);
                    ordersNotAvailable.setVisibility(View.VISIBLE);

                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    } else if (refreshLayout.isRefreshing()) {
                        refreshLayout.setRefreshing(false);
                    }
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                rv.setVisibility(View.GONE);
                ordersNotAvailable.setVisibility(View.VISIBLE);
                Log.d("response_e_inprogrss", error.toString());
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (refreshLayout.isRefreshing()) {
                    refreshLayout.setRefreshing(false);
                }
                volleyErrMethod(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    private void prepareForCalculateEta(String jobNo) {

        if (new InternetGpsCheck(context).isGpsEnabled()) {

            String urlLlll;
            String urlLocation;

            String actual = datalist.get(0).getActualDateTime();
            String orderId = datalist.get(0).getOrderId();

            if (actual != null && actual != "null") {

            } else {
                urlLlll = "waypoint0=geo!" + listHashLatLng.get(0).getLat() + "," + listHashLatLng.get(0).getLng() + "&waypoint1=geo!" + datalist.get(0).getLatt() + "," + datalist.get(0).getLngg();

                urlLocation = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&" + urlLlll + "&mode=fastest;truck;traffic:disabled";

                calculateEta(urlLocation, true, orderId);
            }

            urlLlll = "waypoint0=geo!" + listHashLatLng.get(0).getLat() + "," + listHashLatLng.get(0).getLng();
            int xh = 0;
            Log.d("order_crash", "Datalist Size: "+datalist.size());
            for (int mn = 0; mn < datalist.size(); mn++) {

                Log.d("order_crash", "Datalist Size: "+mn);

                actual = datalist.get(mn).getActualDateTime();
                if (actual != null && actual != "null") {

                } else {
                    ++xh;
                    urlLlll += "&waypoint" + xh + "=geo!" + datalist.get(mn).getLatt() + "," + datalist.get(mn).getLngg();
                }
            }

            if (xh > 0) {
                urlLocation = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&" + urlLlll + "&mode=fastest;truck;traffic:disabled";

//                Log.d("eta_job", jobNo+"  "+urlLocation);

                calculateEta(urlLocation, false, orderId);
            }
        }
    }


    private final Runnable m_Runnable = new Runnable() {
        @Override
        public void run() {
            for (int position = 0; position < dashboardClass.getData().size(); position++) {

                datalist = new ArrayList<>();

                setPickUpData(dashboardClass.getData().get(position).getPickup(), dashboardClass.getData().get(position).getExtraDetail().getOrderId());
                setDeliveryData(dashboardClass.getData().get(position).getDelivery(), dashboardClass.getData().get(position).getExtraDetail().getOrderId());
                sortDataList(datalist);

                setLatLongInList();

                if (listHashLatLng.size()>0)
                prepareForCalculateEta(dashboardClass.getData().get(position).getExtraDetail().getJobNo());
            }
            iii += 100;
            mHandler.postDelayed(m_Runnable, 60000);
        }
    };

    private void setLatLongInList() {

        listHashLatLng = new ArrayList<>();

        LatLngCls latLngCls;
        for (int i = 0; i < datalist.size(); i++) {

            String stsrsty = datalist.get(i).getActualDateTime();

            if (stsrsty != null && stsrsty != "null") {
            } else {
                latLngCls = new LatLngCls();

                latLngCls.setLat(Double.parseDouble(datalist.get(i).getLatt()));
                latLngCls.setLng(Double.parseDouble(datalist.get(i).getLngg()));
                latLngCls.setIdPickDrop(datalist.get(i).getIdPickUpDrop());
                latLngCls.setId(datalist.get(i).getOrderId());

                listHashLatLng.add(latLngCls);
            }
        }

        if (listHashLatLng.size() > 0) {

            latLngCls = new LatLngCls();
            latLngCls.setLat(GlobalClass.latt);
            latLngCls.setLng(GlobalClass.lngg);
            latLngCls.setIdPickDrop("0");
            latLngCls.setId(datalist.get(0).getOrderId());

            listHashLatLng.add(0, latLngCls);
        }
    }

    private void setDataInAdapter(DashboardClass dashboardClass, int position) {

        final int ipos = new Random().nextInt(5);

        item = new DataListInProgressFrag();

        item.setColorLogo(ipos);

        item.setCompanyName(dashboardClass.getData().get(position).getExtraDetail().getCustomerCompanyName());
        item.setJobNumber(dashboardClass.getData().get(position).getExtraDetail().getJobNo());

        item.setPickUpAddress(datalist.get(0).getAddress());
        String strPickUpDate[] = datalist.get(0).getDatetime().split("T");
        String pickUpDate = strPickUpDate[0];

        item.setPickUpDate(pickUpDate);

        item.setDeliveryAddress(datalist.get(datalist.size() - 1).getAddress());
        String strDeliveryDate[] = datalist.get(datalist.size() - 1).getDatetime().split("T");

        item.setDeliveryDate(strDeliveryDate[0]);

        Log.d("sfasfjbkjcvbk", "Hi\n" + dashboardClass.getData().get(position).getExtraDetail().getJobNo() + "  " + dashboardClass.getData().get(position).getExtraDetail().getCarrierCompanyNumber());

        String contactCompanyNumber = dashboardClass.getData().get(position).getExtraDetail().getCarrierCompanyNumber();
        if (contactCompanyNumber != null && contactCompanyNumber != "null" && contactCompanyNumber != "") {
            item.setCompanyContact(contactCompanyNumber);
        }

        item.setOrderId(dashboardClass.getData().get(position).getExtraDetail().getOrderId().toString());

        item.setEstimateTimeFirst("0");
        item.setEstimateTimeLast("0");

        item.setActualTime(datalist.get(0).getActualDateTime());

        list.add(item);

        EtaTimeCls etaTimeModelCls = new EtaTimeCls();

        etaTimeModelCls.setTimeEta(0);
        etaTimeModelCls.setId(String.valueOf(dashboardClass.getData().get(position).getExtraDetail().getOrderId()));

        GlobalClass.listEtaRoute.add(etaTimeModelCls);
        GlobalClass.listEtaNext.add(etaTimeModelCls);

        adapter.notifyDataSetChanged();
    }

    private void sortDataList(List<DataData> datalist) {
        for (int i = 0; i < datalist.size() - 1; i++) {
            for (int j = i + 1; j < datalist.size(); j++) {
                if (datalist.get(i).getDatetime().compareTo(datalist.get(j).getDatetime()) > 0) {
                    itemDataData = new DataData();
                    itemDataData = datalist.get(i);
                    datalist.set(i, datalist.get(j));
                    datalist.set(j, itemDataData);
                }
            }
        }
    }

    private void setDeliveryData(List<Delivery> delivery, Integer orderId) {
        String currentDateAndTime;
        for (int i = 0; i < delivery.size(); i++) {

            currentDateAndTime = delivery.get(i).getDeliveryDateTimeTo();
            itemDataData = new DataData();
            itemDataData.setDatetime(currentDateAndTime);
            itemDataData.setLatt(delivery.get(i).getLatitude());
            itemDataData.setLngg(delivery.get(i).getLongitude());
            itemDataData.setAddress(delivery.get(i).getAddress());
            itemDataData.setIdPickUpDrop(String.valueOf(delivery.get(i).getId()));
            itemDataData.setOrderId(String.valueOf(orderId));
            itemDataData.setActualDateTime(String.valueOf(delivery.get(i).getActualDeliveryDateTime()));
            datalist.add(itemDataData);
        }
    }

    private void setPickUpData(List<Pickup> pickup, Integer orderId) {

        String currentDateAndTime;
        for (int i = 0; i < pickup.size(); i++) {

            currentDateAndTime = pickup.get(i).getPickupDateTimeTo();
            itemDataData = new DataData();
            itemDataData.setDatetime(currentDateAndTime);
            itemDataData.setLatt(pickup.get(i).getLatitude());
            itemDataData.setLngg(pickup.get(i).getLongitude());
            itemDataData.setAddress(pickup.get(i).getAddress());
            itemDataData.setIdPickUpDrop(String.valueOf(pickup.get(i).getId()));
            itemDataData.setOrderId(String.valueOf(orderId));
            itemDataData.setActualDateTime(String.valueOf(pickup.get(i).getActualPickupDateTime()));
            datalist.add(itemDataData);
        }
    }


    List<EtaTimeCls> timeEta;
    public static List<EtaTimeCls> timeEtaNextList;
    DataListInProgressFrag item;
    DataData itemDataData;
    List<DataData> datalist, datalistSet;
    HashMap<String, List<LatLngCls>> listHashMap;
    List<LatLngCls> listHashLatLng;
    List<EtaLatLngCls> listFinalLatLng;
    List<String> listKey;
    int iii;

    private void calculateEta(String urlLocation, final boolean b, final String orderId) {

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, urlLocation, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                EtaTime etaTime = new Gson().fromJson(response.toString(), EtaTime.class);

                EtaTimeCls etaTimeCls;
                if (b) {
                    int ik = -1;
                    for (int kk = 0; kk < GlobalClass.listEtaNext.size(); kk++) {
                        if (GlobalClass.listEtaNext.get(kk).getId().equals(orderId)) {
                            ik = kk;
                            break;
                        }
                    }

                    if (ik >= 0) {
                        etaTimeCls = new EtaTimeCls();
                        etaTimeCls.setId(GlobalClass.listEtaNext.get(ik).getId());
                        etaTimeCls.setTimeEta(etaTime.getResponse().getRoute().get(0).getSummary().getTrafficTime()/* + iii*/);
                        GlobalClass.listEtaNext.set(ik, etaTimeCls);
                    }
                } else {
                    int ik = -1;
                    for (int kk = 0; kk < GlobalClass.listEtaRoute.size(); kk++) {
                        if (GlobalClass.listEtaRoute.get(kk).getId().equals(orderId)) {
                            ik = kk;
                            break;
                        }
                    }
                    if (ik >= 0) {
                        etaTimeCls = new EtaTimeCls();
                        etaTimeCls.setId(GlobalClass.listEtaRoute.get(ik).getId());
                        etaTimeCls.setTimeEta(etaTime.getResponse().getRoute().get(0).getSummary().getTrafficTime());
                        GlobalClass.listEtaRoute.set(ik, etaTimeCls);
                    }
                }

                adapter.notifyDataSetChanged();

                Log.d("response_e_in", "OrderId: " + orderId + " id pick drop "/* + idPickDrop2*/);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("response_e_inprogrss76", "OrderId: " + orderId + " id pick drop "/* + idPickDrop2*/ + "  " + String.valueOf(error.networkResponse.statusCode));
                volleyErrMethod(error);
            }
        });

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    private void listSetData(DashboardClass dashboardClass, List<DataData> dataListSet, String idOrderCurrent) {

        int posAdapter = -1;
        for (int x = 0; x < list.size(); x++) {

            if (list.get(x).getOrderId().equals(idOrderCurrent)) {
                posAdapter = x;
                break;
            }
        }

        if (posAdapter >= 0) {

            item = new DataListInProgressFrag();

            item.setCompanyName(list.get(posAdapter).getCompanyName());
            item.setJobNumber(list.get(posAdapter).getJobNumber());
            item.setPickUpAddress(list.get(posAdapter).getPickUpAddress());
            item.setDeliveryAddress(list.get(posAdapter).getDeliveryAddress());
            item.setPickUpDate(list.get(posAdapter).getPickUpDate());
            item.setDeliveryDate(list.get(posAdapter).getDeliveryDate());
            item.setCompanyContact(list.get(posAdapter).getCompanyContact());
            item.setOrderId(list.get(posAdapter).getOrderId());
            item.setActualTime(list.get(posAdapter).getActualTime());
            item.setColorLogo(list.get(posAdapter).getColorLogo());


            long eta = 0;
            long etaN = 0;

            int idOrder = Integer.parseInt(idOrderCurrent);

            for (int x = 0; x < timeEtaNextList.size(); x++) {

                String id = timeEtaNextList.get(x).getId();

                String id2 = timeEtaNextList.get(x).getId2();

                String idPD = timeEtaNextList.get(x).getIdPickDrop();

                if (id.equals("0") && idPD.equals("0") && id2.equals(String.valueOf(idOrder))) {

                    etaN = timeEtaNextList.get(x).getTimeEta();
                    Log.d("eta_n_by_job_N", "JobId" + dashboardClass.getData().get(posAdapter).getExtraDetail().getOrderId() + "  " + etaN);
                    break;
                }
            }

            //         GlobalClass.listEtaNext.set(posAdapter, String.valueOf(etaN));
            for (int x = 0; x < timeEta.size(); x++) {

                if (list.get(posAdapter).getOrderId().equals(timeEta.get(x).getId())) {

                    eta = timeEta.get(x).getTimeEta();

                    Log.d("eta_b_by_job_N", "JobId" + list.get(posAdapter).getOrderId() + "  " + eta);
                    Log.d("eta_by_job", "JobId" + list.get(posAdapter).getOrderId() + "  " + eta);
                    break;
                }
            }
            //          GlobalClass.listEtaRoute.set(posAdapter, String.valueOf(eta));

            item.setEstimateTimeFirst(String.valueOf(etaN));
            item.setEstimateTimeLast(String.valueOf(eta));

            list.set(posAdapter, item);

            adapter.notifyDataSetChanged();
        }

        if (dialog.isShowing()) {
            dialog.dismiss();
        }

        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }

    }

    private void volleyErrMethod(VolleyError error) {

        if (error.networkResponse != null) {
            Log.d("response_e_inprogrss2", String.valueOf(error.networkResponse.statusCode));
            String sttr = error.networkResponse.data.toString();
            Log.d("response_e_inprogrss5", sttr);
        } else {
            //  Toast.makeText(this, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

                // Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_e_inprogrss3", obj.toString());
                Log.d("response_e_inprogrss4", response.toString());
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
                // returned data is not JSONObject?
                e2.printStackTrace();
            }
        } else {
            //         Toast.makeText(getContext(), "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }

        new GlobalClass().inValidToken(getContext(), error);
    }


    private void sortPickUpDropList(final DashboardClass data, double latt, double lngg, double latt2, double lngg2, final String id, final String idPickDrop, final String id2, final String idPickDrop2) {

        String url = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&waypoint0=geo!" + latt + "," + lngg + "&waypoint1=geo!" + latt2 + "," + lngg2 + "&mode=fastest;truck;traffic:disabled";

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                EtaTime etaTime = new Gson().fromJson(response.toString(), EtaTime.class);

                boolean boolBool = false;
                int ik = -1;
                for (int kk = 0; kk < timeEta.size(); kk++) {
                    if (timeEta.get(kk).getId().equals(String.valueOf(id2))) {
                        boolBool = true;
                        ik = kk;
                        break;
                    }
                }
                if (boolBool) {
                    EtaTimeCls etaTimeCls = new EtaTimeCls();
                    etaTimeCls.setId(timeEta.get(ik).getId());
                    etaTimeCls.setTimeEta(timeEta.get(ik).getTimeEta() + etaTime.getResponse().getRoute().get(0).getSummary().getTrafficTime());
                    timeEta.set(ik, etaTimeCls);
                } else {
                    EtaTimeCls etaTimeCls = new EtaTimeCls();
                    etaTimeCls.setId(id2);
                    etaTimeCls.setTimeEta(etaTime.getResponse().getRoute().get(0).getSummary().getTrafficTime());
                    timeEta.add(etaTimeCls);
                }

                EtaTimeCls etaTimeCls = new EtaTimeCls();
                etaTimeCls.setId(id);
                etaTimeCls.setId2(id2);
                etaTimeCls.setIdPickDrop(idPickDrop);
                etaTimeCls.setIdPickDrop2(idPickDrop2);
                etaTimeCls.setTimeEta(etaTime.getResponse().getRoute().get(0).getSummary().getTrafficTime());
                timeEtaNextList.add(etaTimeCls);

                listSetData(data, datalistSet, id2);

                Log.d("response_e_in", "OrderId: " + id2 + " id pick drop " + idPickDrop2);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("response_e_inprogrss76", "OrderId: " + id2 + " id pick drop " + idPickDrop2 + "  " + String.valueOf(error.networkResponse.statusCode));
                volleyErrMethod(error);
            }
        });

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }



    /*EtaLatLngCls etaLatLngCls;

    int ll = listHashMap.size();

    for (int iw = 0; iw < ll; iw++) {
        for (int iq = 1; iq < listHashMap.get(listKey.get(iw)).size(); iq++) {

            etaLatLngCls = new EtaLatLngCls();

            double lat = listHashMap.get(listKey.get(iw)).get(iq - 1).getLat();
            double lng = listHashMap.get(listKey.get(iw)).get(iq - 1).getLng();
            double lat2 = listHashMap.get(listKey.get(iw)).get(iq).getLat();
            double lng2 = listHashMap.get(listKey.get(iw)).get(iq).getLng();

            String id = listHashMap.get(listKey.get(iw)).get(iq - 1).getId();
            String id2 = listHashMap.get(listKey.get(iw)).get(iq).getId();
            String idPickDrop = listHashMap.get(listKey.get(iw)).get(iq - 1).getIdPickDrop();
            String idPickDrop2 = listHashMap.get(listKey.get(iw)).get(iq).getIdPickDrop();

            etaLatLngCls.setLat(lat);
            etaLatLngCls.setLng(lng);
            etaLatLngCls.setId(id);
            etaLatLngCls.setIdPickDrop(idPickDrop);

            etaLatLngCls.setLat2(lat2);
            etaLatLngCls.setLng2(lng2);
            etaLatLngCls.setId2(id2);
            etaLatLngCls.setIdPickDrop2(idPickDrop2);

            listFinalLatLng.add(etaLatLngCls);
        }
    }

    int len = listFinalLatLng.size();*/

    /*if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        boolean vb = true;
        while (vb) {
            if (GlobalClass.latt == 0.0) {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        new CurrentLocation(context).liveLocation();
                    }
                };
                thread.start();
            } else {
                vb = false;


                for (iy = 0; iy < len; iy++) {

                    sortPickUpDropList(dashboardClass, listFinalLatLng.get(iy).getLat(), listFinalLatLng.get(iy).getLng(), listFinalLatLng.get(iy).getLat2(), listFinalLatLng.get(iy).getLng2(), listFinalLatLng.get(iy).getId(), listFinalLatLng.get(iy).getIdPickDrop(), listFinalLatLng.get(iy).getId2(), listFinalLatLng.get(iy).getIdPickDrop2());
                }
            }
        }
    }*/




    /*GlobalClass.listLatLng = new ArrayList<>();
        listHashLatLng = new ArrayList<>();

        LatLngCls latLngCls;
        for (int i = 0; i < datalist.size(); i++) {

            String stsrsty = datalist.get(i).getActualDateTime();

            if (stsrsty != null && stsrsty != "null") {
            } else {
                latLngCls = new LatLngCls();

                latLngCls.setLat(Double.parseDouble(datalist.get(i).getLatt()));
                latLngCls.setLng(Double.parseDouble(datalist.get(i).getLngg()));
                latLngCls.setIdPickDrop(datalist.get(i).getIdPickUpDrop());
                latLngCls.setId(datalist.get(i).getOrderId());

                GlobalClass.listLatLng.add(latLngCls);
                listHashLatLng.add(latLngCls);
            }
        }

        latLngCls = new LatLngCls();
        latLngCls.setLat(GlobalClass.latt);
        latLngCls.setLng(GlobalClass.lngg);
        latLngCls.setIdPickDrop("0");
        latLngCls.setId(datalist.get(0).getOrderId());

        GlobalClass.listLatLng.add(0, latLngCls);
        listHashLatLng.add(0, latLngCls);*/

        /*listKey.add(String.valueOf(dashboardClass.getData().get(pos).getExtraDetail().getOrderId()));
        listHashMap.put(listKey.get(listKey.size() - 1), listHashLatLng);*/


        /*EtaTimeCls etaTimeCls = new EtaTimeCls();
        etaTimeCls.setId(datalist.get(0).getOrderId());
        etaTimeCls.setTimeEta(0);
        timeEta.add(etaTimeCls);
        timeEtaNextList.add(etaTimeCls);*/

        /*if (new InternetGpsCheck(context).isGpsEnabled()) {

            String urlLlll;
            urlLlll = "waypoint0=geo!" + listHashLatLng.get(0).getLat() + "," + listHashLatLng.get(0).getLng();

            String urlLocation;

            String actual = datalist.get(0).getActualDateTime();
            String orderId = datalist.get(0).getOrderId();

            if (actual != null && actual != "null") {

            } else {
                urlLlll += "&waypoint1=geo!" + datalist.get(0).getLatt() + "," + datalist.get(0).getLngg();
                urlLocation = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&" + urlLlll + "&mode=fastest;truck;traffic:disabled";

                calculateEta(dashboardClass, urlLocation, true, orderId);
            }

            urlLlll = "waypoint0=geo!" + listHashLatLng.get(0).getLat() + "," + listHashLatLng.get(0).getLng();
            int xh = 1;
            for (int mn = 0; mn < datalist.size(); mn++) {

                actual = datalist.get(mn).getActualDateTime();
                if (actual != null && actual != "null") {

                } else {
                    urlLlll += "&waypoint" + xh + "=geo!" + datalist.get(mn).getLatt() + "," + datalist.get(mn).getLngg();
                    xh++;
                }
            }
            urlLocation = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&" + urlLlll + "&mode=fastest;truck;traffic:disabled";

            calculateEta(dashboardClass, urlLocation, false, orderId);
        }*/
}
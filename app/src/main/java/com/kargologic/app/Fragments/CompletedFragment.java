package com.kargologic.app.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kargologic.app.Adapter.CompletedListAdapter;
import com.kargologic.app.classes.DataListInProgressFrag;
import com.kargologic.app.classes.GlobalClass;
import com.kargologic.app.classes.InternetGpsCheck;
import com.kargologic.app.classes.LatLngCls;
import com.kargologic.app.pojoClasses.ViewMore.DataData;
import com.kargologic.app.pojoClasses.dashboard.DashboardClass;
import com.kargologic.app.pojoClasses.dashboard.Delivery;
import com.kargologic.app.pojoClasses.dashboard.Pickup;
import com.kargologic.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;

public class CompletedFragment extends Fragment {

    RecyclerView rv;
    List<DataListInProgressFrag> list;
    private Dialog dialog;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    View view;
    SwipeRefreshLayout refreshLayout;
    private static Context context;
    LinearLayout ordersNotAvailable;
    TextView ordersNotAvailableTxt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_completed, container, false);

        preferences = getActivity().getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        context = getActivity();

        ordersNotAvailable = (LinearLayout) view.findViewById(R.id.no_order_in_list);
        ordersNotAvailableTxt = (TextView) view.findViewById(R.id.no_order_in_list_txt);

        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_progress_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout_swipe);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onResume();
            }
        });

        if (new InternetGpsCheck(context).isNetworkConnected()) {
 //           Toast.makeText(context, "Completed", Toast.LENGTH_SHORT).show();
            dashboardMethod();
        }else {
            Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    //    dashboardMethod();
    }

    public void dashboardMethod() {

        if (refreshLayout.isRefreshing()) {

        } else {
            dialog.show();
        }

        rv = (RecyclerView) view.findViewById(R.id.recyclerview);

        final HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));

        String url = GlobalClass.base_Url + "driv/driver_dashboard/?status=delivered";

        RequestQueue queue = Volley.newRequestQueue(getContext());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                list = new ArrayList<>();
                list.clear();

                final CompletedListAdapter adapter = new CompletedListAdapter(getActivity(), list);

                rv.setHasFixedSize(true);
                rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                rv.setAdapter(adapter);
                rv.setItemAnimator(new DefaultItemAnimator());

                Log.d("response_scs_cmplt", response.toString());

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }else if (refreshLayout.isRefreshing()) {
                    refreshLayout.setRefreshing(false);
                }

                DashboardClass dashboardClass = new Gson().fromJson(response.toString(), DashboardClass.class);

                if (dashboardClass.getData() != null && dashboardClass.getData().size() > 0) {

                    rv.setVisibility(View.VISIBLE);
                    ordersNotAvailable.setVisibility(View.GONE);

                    DataListInProgressFrag item;

                    for (int i = 0; i < dashboardClass.getData().size(); i++) {

                        int ipos = new Random().nextInt(5);

                        item = new DataListInProgressFrag();

                        setPickUpData(dashboardClass.getData().get(i).getPickup());
                        setDeliveryData(dashboardClass.getData().get(i).getDelivery());
                        sortPickUpDropList();

                        String estimateTimeFirst, estimateTimeLast;
                        item.setColorLogo(ipos);
                        item.setCompanyName(dashboardClass.getData().get(i).getExtraDetail().getCustomerCompanyName());
                        item.setJobNumber(dashboardClass.getData().get(i).getExtraDetail().getJobNo());

                        String adrs = datalist.get(0).getAddress();
                        item.setPickUpAddress(adrs);

                        String strPickUpDate[] = datalist.get(0).getDatetime().split("T");
                        String pickUpDate = strPickUpDate[0];
                        item.setPickUpDate(pickUpDate);

                        item.setDeliveryAddress(datalist.get(datalist.size() - 1).getAddress());
                        String strDeliveryDate[] = datalist.get(datalist.size() - 1).getDatetime().split("T");
                        item.setDeliveryDate(strDeliveryDate[0]);

                        String contactCompanyNumber = dashboardClass.getData().get(i).getExtraDetail().getCustomerCompanyNumber();
                        if (contactCompanyNumber != null && contactCompanyNumber != "null" && contactCompanyNumber != "") {
                            item.setCompanyContact(contactCompanyNumber);
                        }

                        item.setOrderId(dashboardClass.getData().get(i).getExtraDetail().getOrderId().toString());

                        list.add(item);
                        adapter.notifyDataSetChanged();
                    }

                    /* if (list.size()>0){*/

                    //     }
                }else {
                    rv.setVisibility(View.GONE);
                    ordersNotAvailable.setVisibility(View.VISIBLE);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                rv.setVisibility(View.GONE);
                ordersNotAvailable.setVisibility(View.VISIBLE);
                Log.d("response_e_completed", error.toString());
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (refreshLayout.isRefreshing()) {
                    refreshLayout.setRefreshing(false);
                }
                volleyErrMethod(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }


    DataData itemDataData;
    List<DataData> datalist;

    private void sortPickUpDropList() {

        if (datalist.size() > 0) {
            for (int i = 0; i < datalist.size() - 1; i++) {
                for (int j = i + 1; j < datalist.size(); j++) {

                    if (datalist.get(i).getDatetime().compareTo(datalist.get(j).getDatetime()) > 0) {

                        itemDataData = new DataData();

                        itemDataData = datalist.get(i);
                        datalist.set(i, datalist.get(j));
                        datalist.set(j, itemDataData);
                    }
                }
            }

            GlobalClass.listLatLng = new ArrayList<>();
            LatLngCls latLngCls;
            for (int i = 0; i < datalist.size(); i++) {

                latLngCls = new LatLngCls();
                latLngCls.setLat(Double.parseDouble(datalist.get(i).getLatt()));
                latLngCls.setLng(Double.parseDouble(datalist.get(i).getLngg()));

                GlobalClass.listLatLng.add(latLngCls);
            }
        }
    }

    private void setPickUpData(List<Pickup> pickup) {

        datalist = new ArrayList<>();

        String currentDateAndTime, latt, lngg;

        for (int i = 0; i < pickup.size(); i++) {

            currentDateAndTime = pickup.get(i).getPickupDateTimeTo();

            itemDataData = new DataData();

            itemDataData.setDatetime(currentDateAndTime);
            itemDataData.setLatt(pickup.get(i).getLatitude());
            itemDataData.setLngg(pickup.get(i).getLongitude());
            itemDataData.setAddress(pickup.get(i).getAddress());

            datalist.add(itemDataData);
        }
    }

    private void setDeliveryData(List<Delivery> delivery) {

        String currentDateAndTime, latt, lngg;

        for (int i = 0; i < delivery.size(); i++) {

            currentDateAndTime = delivery.get(i).getDeliveryDateTimeTo();

            itemDataData = new DataData();

            itemDataData.setDatetime(currentDateAndTime);
            itemDataData.setLatt(delivery.get(i).getLatitude());
            itemDataData.setLngg(delivery.get(i).getLongitude());
            itemDataData.setAddress(delivery.get(i).getAddress());

            datalist.add(itemDataData);
        }
    }

    private void volleyErrMethod(VolleyError error) {

        if (error.networkResponse != null) {
            Log.d("response_e_completed2", String.valueOf(error.networkResponse.statusCode));
        } else {
            //  Toast.makeText(this, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

                // Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_e_completed3", obj.toString());
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
                // returned data is not JSONObject?
                e2.printStackTrace();
            }
        } else {
            //         Toast.makeText(getContext(), "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }

        new GlobalClass().inValidToken(getContext(), error);
    }
}
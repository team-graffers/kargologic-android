package com.kargologic.app.classes;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kargologic.app.pojoClasses.dashboard.DashboardClass;
import com.kargologic.app.R;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class DashBoardDataCls {

    private static final String JSON_URL = GlobalClass.dashboard_URL;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;
    private Dialog dialog;

    public void userregister(Context context) {

        this.context = context;

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progress_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        preferences = context.getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                DashboardClass dashboardClass = new Gson().fromJson(response, DashboardClass.class);
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("Error", "Volley Error");
                dialog.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();

                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Token "+preferences.getString(GlobalClass.toKen, null));

                return headers;
            }
        };

        queue.add(stringRequest);
    }
}

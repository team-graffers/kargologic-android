package com.kargologic.app.classes;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Sandy on 24/08/2019 at 10:20 AM.
 */
public class InternetGpsCheck {

    private Context context;
    public InternetGpsCheck(Context context){
        this.context = context;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public boolean isGpsEnabled(){
        LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
}

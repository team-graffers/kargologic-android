package com.kargologic.app.classes;

public class LatLngCls {

    double lat, lng;
    String id, idPickDrop, pickUpDrop;
    long etaTime;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPickDrop() {
        return idPickDrop;
    }

    public void setIdPickDrop(String idPickDrop) {
        this.idPickDrop = idPickDrop;
    }

    public String getPickUpDrop() {
        return pickUpDrop;
    }

    public void setPickUpDrop(String pickUpDrop) {
        this.pickUpDrop = pickUpDrop;
    }

    public long getEtaTime() {
        return etaTime;
    }

    public void setEtaTime(long etaTime) {
        this.etaTime = etaTime;
    }
}

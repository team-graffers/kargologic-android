package com.kargologic.app.classes;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Looper;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class MyBroadcastReceiver extends BroadcastReceiver {

    double lattUpdateLocation, lnggUpdateLocation;
    boolean bool;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    public void onReceive(Context context, Intent intent) {

        preferences = context.getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        Intent intent3 = new Intent(context, MyBroadcastReceiver.class);
        PendingIntent pendingIntent3 = PendingIntent.getBroadcast(context, 123, intent3, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager mgr2 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr2.cancel(pendingIntent3);
        pendingIntent3.cancel();

        Intent intent2 = new Intent(context, MyBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 123, intent2, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 60000, pendingIntent);

        lattUpdateLocation = 0.0;
        bool = true;

        /*new CurrentLocation(context).liveLocation();

        updateBackgroundLocation(context);*/

        if (new InternetGpsCheck(context).isGpsEnabled()) {
            currentLocation(context);
        }
    }

    @SuppressLint("MissingPermission")
    private void currentLocation(final Context context) {

        //      Toast.makeText(context, "Location Updated before Current", Toast.LENGTH_SHORT).show();

        FusedLocationProviderClient fusedLocationProviderClient;
        LocationRequest locationRequest;
        fusedLocationProviderClient = new FusedLocationProviderClient(context);

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(500);
        locationRequest.setInterval(1000);

        fusedLocationProviderClient.requestLocationUpdates(locationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                lattUpdateLocation = locationResult.getLastLocation().getLatitude();
                lnggUpdateLocation = locationResult.getLastLocation().getLongitude();

                GlobalClass.latt = locationResult.getLastLocation().getLatitude();
                GlobalClass.lngg = locationResult.getLastLocation().getLongitude();

                if (bool) {
                    bool = false;
                    updateBackgroundLocation(context);
                }

            }
        }, Looper.getMainLooper());
    }

    private void updateBackgroundLocation(final Context context) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("lat", lattUpdateLocation);
            jsonObject.put("lng", lnggUpdateLocation);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, GlobalClass.updateLocation, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("update_location", response.toString());
  //              Toast.makeText(context, "Location Updated  "+lattUpdateLocation, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("update_location", error.toString());

                try {
                    if(error.networkResponse != null) {

                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject data = new JSONObject(responseBody);
            /*JSONArray errors = data.getJSONArray("errors");
            JSONObject jsonMessage = errors.getJSONObject(0);*/
                        String message = data.getString("detail");

                        if (message.equals("Invalid token.")) {

                            editor.clear();
                            editor.commit();

                            Intent intent3 = new Intent(context, MyBroadcastReceiver.class);
                            PendingIntent pendingIntent3 = PendingIntent.getBroadcast(context, 123, intent3, PendingIntent.FLAG_ONE_SHOT);
                            AlarmManager mgr2 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                            mgr2.cancel(pendingIntent3);
                            pendingIntent3.cancel();
                        }
                        Log.d("response_e_completed61", message);
                    }
                } catch (JSONException e) {
                } catch (UnsupportedEncodingException errorr) {
                }
                //                   Toast.makeText(context, "Location Updated Error", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences preferences = context.getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
                headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }
}

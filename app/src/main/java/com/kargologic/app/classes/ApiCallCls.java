package com.kargologic.app.classes;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kargologic.app.activities.SignInActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class ApiCallCls {

    Context context;
    RequestQueue queue;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public void stringGetMethod(final Context context, String url){

        this.context = context;
        queue = Volley.newRequestQueue(context);

        preferences = context.getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        StringRequest request = new StringRequest(Request.Method.GET, url, responseStringReqSuccess(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errorResponse(context, error);
            }
        });

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    public void jsonGetMethod(final Context context, String url){
        this.context = context;
        queue = Volley.newRequestQueue(context);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, responseJsonReqSuccess(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errorResponse(context, error);
            }
        });

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    public void jsonPostMethod(final Context context, String url, JSONObject jsonObject, final Dialog dialog, final HashMap<String, String> headers){
        this.context = context;
        queue = Volley.newRequestQueue(context);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, responseJsonReqSuccess(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                errorResponse(context, error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    public Response.Listener<JSONObject> responseJsonReqSuccess() {
        return null;
    }

    public Response.Listener<String> responseStringReqSuccess() {
        return null;
    }

    public void errorResponse(Context context, VolleyError error){

        this.context = context;
        Log.d("response_volley_err", error.toString());
   //     Log.d("response_volley_code", error.toString());

        error.printStackTrace();


        if (error instanceof TimeoutError){

        }

        if (error instanceof NetworkError){
            Toast.makeText(context, "Please Check Your Internet Connection.", Toast.LENGTH_LONG).show();
        }


        if (error.networkResponse != null) {
            Log.d("response_volley_err2", String.valueOf(error.networkResponse.statusCode));
            try {
                String responseBody = new String(error.networkResponse.data, "utf-8");
                JSONObject data = new JSONObject(responseBody);
            /*JSONArray errors = data.getJSONArray("errors");
            JSONObject jsonMessage = errors.getJSONObject(0);*/
                String message = data.getString("detail");

                if (message.equals("Invalid token.")){
                    editor.remove(GlobalClass.toKen);
                    editor.commit();
                    context.startActivity(new Intent(context, SignInActivity.class));
                }
                Log.d("response_e_upcoming6", message);
            } catch (JSONException e) {
            } catch (UnsupportedEncodingException errorr) {
            }
        }else{
//            Toast.makeText(this.context, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

// Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_volley_err3", obj.toString());
                String msg = obj.getString("message");
                Toast.makeText(context, ""+msg, Toast.LENGTH_LONG).show();
            } catch (UnsupportedEncodingException e1) {
// Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
// returned data is not JSONObject?
                e2.printStackTrace();
            }
        }else {
 //           Toast.makeText(this.context, "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }
    }
}

package com.kargologic.app.classes;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.VolleyError;
import com.kargologic.app.activities.SignInActivity;
import com.kargologic.app.pojoClasses.EtaTimeCls;
import com.kargologic.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class GlobalClass {

    public static final String preferencesName = "kargoLogic";
    public static final String toKen = "Token";
    public static final String nameUser = "nameUser";
    public static final String contactNoUser = "contactNoUser";
    public static final String emailUser = "emailUser";
    public static final String imgUser = "imageUser";
    public static final String carrierId = "carrierID";
    public static final String carrierCompId = "carrierCompID";
    public static final String enrouteTime = "enrouteTime";

    public static final String truckType = "truckType";
    public static final String truckRego = "truckRego";
    public static final String licenseNo = "licenseNo";
    public static final String vehicleId = "vehicleId";

    public static final String podImage = "podImage";

    public static final String companyContactNo = "companyContactNumber";

    public static final String jobNo = "jobNo";

    public static boolean boolDashBoard = false;
    public static boolean isNotify = false;

    public static final String orderId = "orderID";

    public static double latt  = 0.0;
    public static double lngg = 0.0;

    public static double lattUpdateLocation  = 0.0;
    public static double lnggUpdateLocation = 0.0;

    public static String etaTimeRoute = null;
    public static String etaTimeNext = null;

    public static List<LatLngCls> listLatLng;

    public static List<EtaTimeCls> listEtaRoute;
    public static List<EtaTimeCls> listEtaNext;

/**
 * Base URL
 * **/
 //   public static final String base_Url = "http://3.106.32.156:8000/";
 //   public static final String base_Url = "http://13.54.44.169:8000/";

//    public static final String base_Url = "https://carriers.kargologic.com/";    /*Production*/
    public static final String base_Url = "http://52.62.137.8:8000/";    /*:8000*/     /*Development*/

    public static final String imgBase_Url = "https://s3.amazonaws.com/kargobucket/media/";

    public static final String login_URL = base_Url+"driv/driv_login/";

    public static final String dashboard_URL = base_Url + "driv/driver_dashboard/";

    public static final String updateProfileURL = base_Url+"kargologic.app";
    public static final String uniqueMobileURL = base_Url+"driv/unique_user_num/";
    public static final String uniqueEmailURL = base_Url+"driv/driver_pr_upd_email_chk/";
    public static final String actualPickUpTime = base_Url+"pickup/driver_update_pck_actual_time/";
    public static final String actualDeliveryTime = base_Url+"delivery/driver_update_drp_actual_time/";
    public static final String lastDeliveryStatusChangeTime = base_Url+"orders/driver_chng_ord_stat_/";

    public static final String truck_Cat_List = base_Url+"truck_categ/truck_categ_list/";
    public static final String notification = base_Url+"driv/notify-driv/";

    public static final String sendComments = base_Url+"driv/driver_order_comment/";
    public static final String forgotPswd = base_Url+"driv/driv_forgot_pass/";
    public static final String changePswd = base_Url+"driv/driver_reset_pass/";

    public static final String updateLocation = base_Url+"driv/cor_driver_update/";

    public final int[] bulletBackColor = new int[]{R.color.a1, R.color.a2, R.color.a3, R.color.a4, R.color.a5};
    public final int[] bulletFontColor = new int[]{R.color.b1, R.color.b2, R.color.b3, R.color.b4, R.color.b5};


    public void inValidToken(Context context, VolleyError error){

        SharedPreferences preferences;
        SharedPreferences.Editor editor;
        preferences = context.getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();
        try {
            if(error.networkResponse != null) {

                String responseBody = new String(error.networkResponse.data, "utf-8");
                JSONObject data = new JSONObject(responseBody);
            /*JSONArray errors = data.getJSONArray("errors");
            JSONObject jsonMessage = errors.getJSONObject(0);*/
                String message = data.getString("detail");

                if (message.equals("Invalid token.")) {
                    
                    editor.clear();
                    editor.commit();

                    Intent intent3 = new Intent(context, MyBroadcastReceiver.class);
                    PendingIntent pendingIntent3 = PendingIntent.getBroadcast(context, 123, intent3, PendingIntent.FLAG_ONE_SHOT);
                    AlarmManager mgr2 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    mgr2.cancel(pendingIntent3);
                    pendingIntent3.cancel();

                    Intent intent = new Intent(context, SignInActivity.class);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                }
                Log.d("response_e_completed6", message);
            }
        } catch (JSONException e) {
        } catch (UnsupportedEncodingException errorr) {
        }
    }

    public static String convert(String str) {

        char ch[] = str.toCharArray();

        for (int i = 0; i < str.length(); i++) {

            // If first character of a word is found
            if (i == 0 && ch[i] != ' ' || ch[i] != ' ' && ch[i - 1] == ' ') {

                // If it is in lower-case
                if (ch[i] >= 'a' && ch[i] <= 'z') {

                    // Convert into Upper-case
                    ch[i] = (char)(ch[i] - 'a' + 'A');
                }
            }
            // If apart from first character
            // Any one is in Upper-case
            else if (ch[i] >= 'A' && ch[i] <= 'Z')

                // Convert into Lower-Case
                ch[i] = (char)(ch[i] + 'a' - 'A');
        }

        // Convert the char array to equivalent String
        String st = new String(ch);
        return st;
    }
}
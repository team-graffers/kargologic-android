package com.kargologic.app.classes;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kargologic.app.activities.StatusActivity;
import com.kargologic.app.pojoClasses.notification.NotificationList;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Sandy on 30/09/2019 at 06:48 PM.
 */
public class MyBroadcastReceiverNotification extends BroadcastReceiver {


    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    public void onReceive(final Context context, Intent intent) {

        preferences = context.getSharedPreferences(GlobalClass.preferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, GlobalClass.notification, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_notify_sR", response.toString());

                NotificationList notificationList = new Gson().fromJson(response.toString(), NotificationList.class);

                if (notificationList.getData().size() > 0 && notificationList.getNewMessage() == true){
                    StatusActivity.viewNotifyDot.setVisibility(View.VISIBLE);
                    GlobalClass.isNotify = true;
                }else {
   //                 StatusActivity.viewNotifyDot.setVisibility(View.GONE);
                }

                Intent intent3 = new Intent(context, MyBroadcastReceiverNotification.class);
                PendingIntent pendingIntent3 = PendingIntent.getBroadcast(context, 123, intent3, PendingIntent.FLAG_ONE_SHOT);
                AlarmManager mgr2 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                mgr2.cancel(pendingIntent3);
                pendingIntent3.cancel();

                Intent intent2 = new Intent(context, MyBroadcastReceiverNotification.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 123, intent2, PendingIntent.FLAG_ONE_SHOT);
                AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 10000, pendingIntent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("resp_notify_eR", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + preferences.getString(GlobalClass.toKen, null));
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }
}

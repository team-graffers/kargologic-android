package com.kargologic.app.classes;

public class DataListInProgressFrag {

    String companyName, jobNumber, pickUpAddress, deliveryAddress, pickUpDate, deliveryDate, estimateTimeFirst, estimateTimeLast, companyContact = "", orderId, actualTime;
    int colorLogo;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getEstimateTimeFirst() {
        return estimateTimeFirst;
    }

    public void setEstimateTimeFirst(String estimateTimeFirst) {
        this.estimateTimeFirst = estimateTimeFirst;
    }

    public String getEstimateTimeLast() {
        return estimateTimeLast;
    }

    public void setEstimateTimeLast(String estimateTimeLast) {
        this.estimateTimeLast = estimateTimeLast;
    }

    public String getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(String companyContact) {
        this.companyContact = companyContact;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public Integer getColorLogo() {
        return colorLogo;
    }

    public void setColorLogo(Integer colorLogo) {
        this.colorLogo = colorLogo;
    }
}

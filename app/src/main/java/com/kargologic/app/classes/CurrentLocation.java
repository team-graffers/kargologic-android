package com.kargologic.app.classes;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

/**
 * Created by Sandy on 19-03-2019 at 12:30 PM.
 */
public class CurrentLocation {

    Context context;
    protected LocationManager locationManager;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;

    public CurrentLocation(Context context) {
        this.context = context;
    }

    public void liveLocation(){
        try {

            if (locationManager == null) {

                locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            }


            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location locationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location locationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);


            if (location != null){
                GlobalClass.latt = location.getLatitude();
                GlobalClass.lngg = location.getLongitude();

                /*Log.d("asdah", "lat: "+latitude+"   lng: "+longitude);*/
            }else if (locationNetwork != null){
                GlobalClass.latt = locationNetwork.getLatitude();
                GlobalClass.lngg = locationNetwork.getLongitude();

     //           Log.d("asdah", "lat: "+latitude+"   lng: "+longitude);
            }else if (locationPassive != null){
                GlobalClass.latt = locationPassive.getLatitude();
                GlobalClass.lngg = locationPassive.getLongitude();

     //           Log.d("asdah", "lat: "+latitude+"   lng: "+longitude);
            }

            /*locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 0,
                    mLocationListeners[1]);*/

        } catch (java.lang.SecurityException ex) {

            Log.i("asdah", "fail to request location update, ignore", ex);

        } catch (IllegalArgumentException ex) {

            Log.d("asdah", "network provider does not exist, " + ex.getMessage());
        }
    }

    @SuppressLint("MissingPermission")
    public void lattLong() {

        fusedLocationProviderClient = new FusedLocationProviderClient(context);

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(1000);
        locationRequest.setInterval(1000);

        fusedLocationProviderClient.requestLocationUpdates(locationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                GlobalClass.latt = locationResult.getLastLocation().getLatitude(  );
                GlobalClass.lngg = locationResult.getLastLocation().getLongitude();

                GlobalClass.lattUpdateLocation = locationResult.getLastLocation().getLatitude(  );
                GlobalClass.lnggUpdateLocation = locationResult.getLastLocation().getLongitude();

            }

        }, Looper.getMainLooper());
    }
}
